1. mv cakebasic name_project
2. cd name_project
3. delete .git
4. crear repo project en bitbucket
5. git init
6. git remote add origin ssh://git@bitbucket.org/josetablate/name_project.git
7. edit gitignore : #/vendor/*
					/tmp/*
					/tmp/sessions/*
					/logs/*
					# exception to the rule
					!/tmp/sessions
					!/tmp/sessions/empty
					!/logs/empty 
8. composer install
9. comment BootstrapUI.Flash en /cakebasic/vendor/friendsofcake/bootstrap-ui/src/View/UIViewTrait.php (para que funcione el noty)
10. descargar FPDF http://www.fpdf.org/ y agregarlo al vendor solamente el archivo fpdf.php y la carpeta font
11. require ROOT . DS . 'vendor' . DS . 'fpdf' . DS . 'fpdf.php'; -> agregar en config/boostrap.php
12. git add .
13. git commit -m "primer commit"
14. git push origin master

Pasos para hacer funcionar Cooty Web sin la extension intl

The recommended way to install composer packages is:

composer require hraq/cake-intl dev-master --ignore-platform-reqs
Add:

Plugin::load('CakeIntl', ['bootstrap' => true]);
in "config/bootstrap.php"

Luego en config/app.php
comentar la linea

/*
 *  You can remove this if you are confident you have intl installed.
 */
//if (!extension_loaded('intl')) {
//    trigger_error('You must enable the intl extension to use CakePHP.', E_USER_ERROR);
//}

En vendor/cakephp/cakephp/src/DataBase/Type/DataTimeType.php
comentar

	/**
     * Set the classname to use when building objects.
     *
     * @param string $class The classname to use.
     * @param string $fallback The classname to use when the preferred class does not exist.
     * @return void
     */
    protected function _setClassName($class, $fallback)
    {
        if (!class_exists($class)) {
            $class = $fallback;
        }
        $this->_className = $class;
        //$this->_datetimeInstance = new $this->_className;
    }
    
    /**
     * Convert strings into DateTime instances.
     *
     * @param string $value The value to convert.
     * @param \Cake\Database\Driver $driver The driver instance to convert with.
     * @return \Cake\I18n\Time|\DateTime
     */
    public function toPHP($value, Driver $driver)
    {
        if ($value === null || strpos($value, '0000-00-00') === 0) {
            return null;
        }

        if (strpos($value, '.') !== false) {
            list($value) = explode('.', $value);
        }

        //$instance = clone $this->_datetimeInstance;
        $instance = new \DateTime();

        return $instance->modify($value);
    }


