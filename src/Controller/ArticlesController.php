<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Articles Controller
 *
 * @property \App\Model\Table\ArticlesTable $Articles
 */
class ArticlesController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        if ($this->request->session()->read('type_user') != 'admin' ) {
            if ($this->Auth) {
                $this->Auth->logout();
            }
        }
        $this->viewBuilder()->layout('admin');
    }

    public function isAuthorized($user = null) 
    {
        return parent::isAuthorized($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $articles = $this->Articles->find()->contain(['StoresHasArticles.Stores']);

        $this->set(compact('articles'));
        $this->set('_serialize', ['articles']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $ids = explode(',', $_POST['ids']);
            $style_id = $_POST['style_id'];
            $product_id = $_POST['product_id'];
            $arts = array_chunk($ids, 3);

            foreach ($arts as $art) {
                $article = $this->Articles->get($art[0]);
                $article->size = $art[1];
                $article->name_display = $art[2];
                $article->style_id = intval($style_id);

                $article = $this->Articles->patchEntity($article, $this->request->data);
                if ($this->Articles->save($article)) {
                    $this->Flash->success(__('El articulo se ha modificado correctamente.'));
                } else {
                    $this->Flash->error(__('Error al modificar el articulo. Por favor, intente nuevamente.'));
                }
            }
            return $this->redirect(['controller' => 'products', 'action' => 'edit', $product_id, $style_id]);
        }
    }

    /**
     * Delete method
     *
     *
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);

        $article = $this->Articles->get($_POST['id']);
        $article->style_id = null;
        $article->size = null;

        if ($this->Articles->save($article)) {
            $this->Flash->success(__('El articulo se ha eliminado correctamente.'));
        } else {
            $this->Flash->error(__('Error al eliminar el articulo. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['controller' => 'products', 'action' => 'edit', $_POST['product_id']]);
    }

    public function getArticlesAjax() 
    {
        //Ajax Detection
        if ($this->request->is('Ajax')) {

            $articles = $this->Articles->find()->where(['style_id is' => null]);

            if ($articles->count() > 0) {
                $this->set('articles', $articles);
            } else {
                $this->set('articles', false);
            }
        }
    }

    public function editNameDisplay($id = null)
    {
        $article = $this->Articles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $article = $this->Articles->patchEntity($article, $this->request->data);
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('El articulo se ha modificado correctamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El Articulo no se ha modificado. Por favor, intente nuevamente.'));
        }
        $this->set(compact('article'));
        $this->set('_serialize', ['article']);
    }

    public function addSizeImage()
    {
        $this->request->allowMethod(['post', 'delete']);
        $id = $_POST['article_id'];
        $article = $this->Articles->get($id);

        $file = $this->request->data['file'];

        if ($file) {
            $ext = explode('.', $file['name']);
            $now = new \DateTime();
            $newName = $now->getTimestamp() . '-' . $ext[0] . '.' .  end($ext);
            $from = $file['tmp_name'];
            $to = WWW_ROOT . 'images/'. $newName ;

            if (move_uploaded_file($from,$to)) {
                $article->size_image = $newName;
            }
        }

        if ($this->Articles->save($article)) {
            $this->Flash->success(__('Se ha añadido la imagen de talles correctamente.'));
        } else {
            $this->Flash->error(__('Error al añadir la imagen de talles. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['controller' => 'articles', 'action' => 'editNameDisplay', $id]);
    }

    public function deleteSizeImage($id = null)
    {

        $this->request->allowMethod(['post', 'delete']);
        $id = $_POST['id'];
        $article = $this->Articles->get($id);
        if (unlink(WWW_ROOT . 'images/'. $article->size_image)) {
            $article->size_image = null;

            if ($this->Articles->save($article)) {
                $this->Flash->success(__('Se ha eliminado correctamente la iamgen de talles.'));
            } else {
                $this->Flash->error(__('Error al elimninar la imagen de talles. Por favor, intente nuevamente.'));
            }
        } else {
            $this->Flash->error(__('Error al elimninar la imagen de talles. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['controller' => 'articles', 'action' => 'editNameDisplay', $id]);
    }
}
