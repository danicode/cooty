<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Filesystem\File;

/**
 * Assets Controller
 *
 * @property \App\Model\Table\AssetsTable $Assets
 */
class AssetsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
 
        if ($this->request->session()->read('type_user') != 'admin' ) {
            if ($this->Auth) {
                $this->Auth->logout();
            }
        }
        $this->viewBuilder()->layout('admin');
    }

    public function isAuthorized($user = null)
    {
        return parent::isAuthorized($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Styles']
        ];
        $assets = $this->paginate($this->Assets);

        $this->set(compact('assets'));
        $this->set('_serialize', ['assets']);
    }

    /**
     * View method
     *
     * @param string|null $id Asset id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $asset = $this->Assets->get($id, [
            'contain' => ['Styles', 'Promotions']
        ]);

        $this->set('asset', $asset);
        $this->set('_serialize', ['asset']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($product_id)
    {
        if ($this->request->is('post')) {

            foreach ($this->request->data['files'] as $file) {

                 if ($file['tmp_name']) {

                    $asset = $this->Assets->newEntity();

                    $ext = explode('.', $file['name']);

                    $now = new \DateTime();
                    $newName = $now->getTimestamp() .'-' . $ext[0] . '.' .  end($ext);
                    $from = $file['tmp_name'];
                    $to = WWW_ROOT . 'images/'. $newName ;

                    if (move_uploaded_file($from,$to)) {

                        $this->request->data['url'] = $newName;
                    }
                }

                $asset = $this->Assets->patchEntity($asset, $this->request->data);
                if (!$this->Assets->save($asset)) {
                     $this->Flash->error(__('Las imagnes no se han guardado. Por favor, intente nuevamente.'));
                }
            }

            $this->Flash->success(__('Las imagenes se han guardado correctamente.'));

            return $this->redirect(['controller' => 'products', 'action' => 'edit', $product_id, $asset->style_id]);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Asset id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $asset = $this->Assets->get($id, [
            'contain' => ['Promotions']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $asset = $this->Assets->patchEntity($asset, $this->request->data);
            if ($this->Assets->save($asset)) {
                $this->Flash->success(__('Las imagenes se han modificado correctamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Las imagnes no se han modificado. Por favor, intente nuevamente.'));
        }
        $styles = $this->Assets->Styles->find('list', ['limit' => 200]);
        $promotions = $this->Assets->Promotions->find('list', ['limit' => 200]);
        $this->set(compact('asset', 'styles', 'promotions'));
        $this->set('_serialize', ['asset']);
    }

    /**
     * Delete method
     *
     *
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $asset = $this->Assets->get($_POST['id']);
        $asset->style_id = null;
        // if (unlink(WWW_ROOT . 'images/'. $asset->url)) {

        //     if ($this->Assets->delete($asset)) {
        //         $this->Flash->success(__('Las imagen se ha eliminado correctamente.'));
        //     } else {
        //         $this->Flash->error(__('La imagen no se han eliminado. Por favor, intente nuevamente.'));
        //     }
        // }
        $this->Assets->save($asset);

        return $this->redirect(['controller' => 'products', 'action' => 'edit', $_POST['product_id'], $asset->style_id]);
    }
}
