<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

class CatalogoController extends AppController
{
    
    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->layout('ecommerce');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index', 'temporadas', 'buscador']);
    }
 
    public function index($category_id = null)
    {
        $this->loadModel('Categories');
        $this->loadModel('Seasons');
        $this->loadModel('Products');
        $this->loadModel('Groups');
        $this->loadModel('Promotions');
        $this->loadModel('Styles');
        $order = null;
        $group_selected = null;

        if ($this->request->is('post')) {
            $order = $_POST['order'];
            $group_selected = $_POST['group_selected'];
        }

        $products = null;
        $groups = $this->Groups->find()->contain(['Categories', 'Categories.CategoriesAssets'])->limit(6);
        $promotions = $this->Promotions->find()->contain(['PromotionsAssets']);

        if ($group_selected) {
            $group_name = $this->Groups->get($group_selected)->name;
            if ($order) {
                $products = $this->Products->find()->contain(['Styles.Assets', 'Styles.Articles', 'Categories.Groups'])->where(['news' => 1, 'visible' => 1, 'enable' => 1, 'Groups.id' => $group_selected])->order(['price' => $order]);
            } else  {
                $products = $this->Products->find()->contain(['Styles.Assets', 'Styles.Articles', 'Categories.Groups'])->where(['news' => 1, 'visible' => 1, 'enable' => 1, 'Groups.id' => $group_selected]);
            }
        } else {
            if ($category_id) {
                $category_selected = $this->Categories->get($category_id, ['contain' => ['Groups']]);
                if ($order) {
                    $products = $this->Products->find()->contain(['Styles.Assets', 'Styles.Articles'])->where(['category_id' => $category_id, 'visible' => 1, 'enable' => 1])->order(['price' => $order]);
                } else {
                    $products = $this->Products->find()->contain(['Styles.Assets', 'Styles.Articles'])->where(['category_id' => $category_id, 'visible' => 1, 'enable' => 1]);
                }
            } else {
                $category_id = "false";
                if ($order) {
                    $products = $this->Products->find()->contain(['Styles.Assets', 'Styles.Articles'])->where(['visible' => 1, 'enable' => 1])->order(['price' => $order]);
                } else {
                    $products = $this->Products->find()->contain(['Styles.Assets', 'Styles.Articles'])->where(['visible' => 1, 'enable' => 1]);
                }
            }
        }

        $productx = array();
        foreach ($products as $product) {
            $flag = true;
            $stylex = array();
            $p = null;
            foreach ($product->styles as $style) {
                if (count($style->assets) > 0 && count($style->articles)) {
                    if ($flag) {
                        $s = $this->Styles->newEntity();
                        $p = $this->Products->newEntity();
                        $p->id = $product->id;
                        $p->name = $product->name;
                        $p->price = $product->price;
                        $p->custom_description = $product->custom_description;
                        $p->enable = 1;
                        $p->visible = 1;
                        $p->news = 1;
                        $flag = false;
                    }
                    $s = $style;
                    array_push($stylex, $s);
                }
            }
            if ($p) {
                $p->styles = $stylex;
                array_push($productx, $p);
            }
        }
        $products = $productx;

        $news_products = $this->Products->find()->contain(['Styles.Assets', 'Styles.Articles'])->where(['news' => 1, 'visible' => 1, 'enable' => 1]);
        $productx = array();
        foreach ($news_products as $product) {
            $flag = true;
            $stylex = array();
            $p = null;
            foreach ($product->styles as $style) {
                if (count($style->assets) > 0 && count($style->articles)) {
                    if ($flag) {
                        $s = $this->Styles->newEntity();
                        $p = $this->Products->newEntity();
                        $p->id = $product->id;
                        $p->name = $product->name;
                        $p->price = $product->price;
                        $p->custom_description = $product->custom_description;
                        $p->enable = 1;
                        $p->visible = 1;
                        $p->news = 1;
                        $flag = false;
                    }
                    $s = $style;
                    array_push($stylex, $s);
                }
            }
            if ($p) {
                $p->styles = $stylex;
                array_push($productx, $p);
            }
        }
        $news_products = $productx;

        $session = $this->request->session();
        $shopping_cart = $session->read('shopping_cart');
        $customer = $session->read('customer');

        $categories = $this->Categories->find('list')->contain(['CategoriesAssets'])->limit(6);
        $seasons = $this->Seasons->find();

        $this->set(compact('products', 'customer', 'categories', 'seasons', 'category_selected', 'groups', 'shopping_cart', 'news_products', 'order', 'category_id', 'promotions', 'group_selected', 'group_name'));
        $this->set('_serialize', ['products', 'customer', 'categories', 'seasons', 'category_selected', 'groups', 'shopping_cart', 'news_products', 'order', 'category_id', 'promotions', 'group_selected', 'group_name']);
    }

    public function temporadas($season_id = null)
    {
        $this->loadModel('Categories');
        $this->loadModel('Seasons');
        $this->loadModel('Products');
        $this->loadModel('Groups');
        $this->loadModel('Promotions');
        $this->loadModel('Styles');
        $order = null;
        if ($this->request->is('post')) {
            $order = $_POST['order'];
        }

        $products = null;
        $groups = $this->Groups->find()->contain(['Categories', 'Categories.CategoriesAssets'])->limit(6);
        $promotions = $this->Promotions->find()->contain(['PromotionsAssets']);

        if ($season_id) {
            $season_selected = $this->Seasons->get($season_id);
            if ($order) {
                $products = $this->Products->find()->contain(['Styles.Assets', 'Styles.Articles'])->where(['season_id' => $season_id, 'visible' => 1, 'enable' => 1])->order(['price' => $order]);
            } else {
                $products = $this->Products->find()->contain(['Styles.Assets', 'Styles.Articles'])->where(['season_id' => $season_id, 'visible' => 1, 'enable' => 1]);
            }
        } else {
            $season_id = "false";
            if ($order) {
                $products = $this->Products->find()->contain(['Styles.Assets', 'Styles.Articles'])->where(['visible' => 1, 'enable' => 1])->order(['price' => $order]);
            } else {
                $products = $this->Products->find()->contain(['Styles.Assets', 'Styles.Articles'])->where(['visible' => 1, 'enable' => 1]);
            }
        }
        $productx = array();
        foreach ($products as $product) {
            $flag = true;
            $stylex = array();
            $p = null;
            foreach ($product->styles as $style) {
                if (count($style->assets) > 0 && count($style->articles)) {
                    if ($flag) {
                        $s = $this->Styles->newEntity();
                        $p = $this->Products->newEntity();
                        $p->id = $product->id;
                        $p->name = $product->name;
                        $p->price = $product->price;
                        $p->custom_description = $product->custom_description;
                        $p->enable = 1;
                        $p->visible = 1;
                        $p->news = 1;
                        $flag = false;
                    }
                    $s = $style;
                    array_push($stylex, $s);
                }
            }
            if ($p) {
                $p->styles = $stylex;
                array_push($productx, $p);
            }
        }
        $products = $productx;

        $news_products = $this->Products->find()->contain(['Styles.Assets', 'Styles.Articles'])->where(['news' => 1, 'visible' => 1, 'enable' => 1]);
        $productx = array();
        foreach ($news_products as $product) {
            $flag = true;
            $stylex = array();
            $p = null;
            foreach ($product->styles as $style) {
                if (count($style->assets) > 0 && count($style->articles)) {
                    if ($flag) {
                        $s = $this->Styles->newEntity();
                        $p = $this->Products->newEntity();
                        $p->id = $product->id;
                        $p->name = $product->name;
                        $p->price = $product->price;
                        $p->custom_description = $product->custom_description;
                        $p->enable = 1;
                        $p->visible = 1;
                        $p->news = 1;
                        $flag = false;
                    }
                    $s = $style;
                    array_push($stylex, $s);
                }
            }
            if ($p) {
                $p->styles = $stylex;
                array_push($productx, $p);
            }
        }
        $news_products = $productx;

        $session = $this->request->session();
        $shopping_cart = $session->read('shopping_cart');
        $customer = $session->read('customer');

        $categories = $this->Categories->find('list')->contain(['CategoriesAssets'])->limit(6);
        $seasons = $this->Seasons->find();

        $this->set(compact('products', 'customer', 'categories', 'seasons', 'season_selected', 'groups', 'shopping_cart', 'news_products', 'order', 'season_id', 'promotions'));
        $this->set('_serialize', ['products', 'customer', 'categories', 'seasons', 'season_selected', 'groups', 'shopping_cart', 'news_products', 'order', 'season_id', 'promotions']);
    }
    
    public function buscador($category_id = null)
    {
        if ($this->request->is('post')) {
            $this->loadModel('Categories');
            $this->loadModel('Seasons');
            $this->loadModel('Products');
            $this->loadModel('Groups');
            $this->loadModel('Promotions');
            $this->loadModel('Styles');

            $order = $_POST['order'];
            $search = $_POST['search'];
            $search_word = $_POST['search'];
            $products = null;
            $groups = $this->Groups->find()->contain(['Categories', 'Categories.CategoriesAssets'])->limit(6);
            $promotions = $this->Promotions->find()->contain(['PromotionsAssets']);

            if ($category_id) {
                $category_selected = $this->Categories->get($category_id, ['contain' => ['Groups']]);
                if ($order) {
                    $products = $this->Products->find()->contain(['Styles.Assets', 'Styles.Articles'])->where(['category_id' => $category_id, 'visible' => 1, 'enable' => 1, 'name LIKE' => '%' . $_POST['search'] . '%'])->order(['price' => $order]);
                } else {
                    $products = $this->Products->find()->contain(['Styles.Assets', 'Styles.Articles'])->where(['category_id' => $category_id, 'visible' => 1, 'enable' => 1, 'name LIKE' => '%' . $_POST['search'] . '%']);
                }
            } else {
                $category_id = "false";
                if ($order) {
                    $products = $this->Products->find()->contain(['Styles.Assets', 'Styles.Articles'])->where(['visible' => 1, 'enable' => 1, 'name LIKE' => '%' . $_POST['search'] . '%'])->order(['price' => $order]);
                } else {
                    $products = $this->Products->find()->contain(['Styles.Assets', 'Styles.Articles'])->where(['visible' => 1, 'enable' => 1, 'name LIKE' => '%' . $_POST['search'] . '%']);
                }
            }

            if ($products->count() == 0) {
                $word = 'No se encontro ningun producto relacionado con la palabra ' . '<span style="font-weight: bold; font-family: monospace; font-size: 22px;"><i style="margin-right: 5px;" class="fa fa-quote-left"></i>' . $search . '<i style="margin-left: 5px;" class="fa fa-quote-right"></i></span>';
                $search = $word;
            } else {
                $search = '<span style="font-weight: bold; font-family: monospace; font-size: 22px;"><i style="margin-right: 5px;" class="fa fa-quote-left"></i>' . $search . '<i style="margin-left: 5px;" class="fa fa-quote-right"></i></span>';
                $productx = array();
                foreach ($products as $product) {
                    $flag = true;
                    $stylex = array();
                    $p = null;
                    foreach ($product->styles as $style) {
                        if (count($style->assets) > 0 && count($style->articles)) {
                            if ($flag) {
                                $s = $this->Styles->newEntity();
                                $p = $this->Products->newEntity();
                                $p->id = $product->id;
                                $p->name = $product->name;
                                $p->price = $product->price;
                                $p->custom_description = $product->custom_description;
                                $p->enable = 1;
                                $p->visible = 1;
                                $p->news = 1;
                                $flag = false;
                            }
                            $s = $style;
                            array_push($stylex, $s);
                        }
                    }
                    if ($p) {
                        $p->styles = $stylex;
                        array_push($productx, $p);
                    }
                }
                $products = $productx;
            }

            $news_products = $this->Products->find()->contain(['Styles.Assets', 'Styles.Articles'])->where(['news' => 1, 'visible' => 1, 'enable' => 1]);
            $productx = array();
            foreach ($news_products as $product) {
                $flag = true;
                $stylex = array();
                $p = null;
                foreach ($product->styles as $style) {
                    if (count($style->assets) > 0 && count($style->articles)) {
                        if ($flag) {
                            $s = $this->Styles->newEntity();
                            $p = $this->Products->newEntity();
                            $p->id = $product->id;
                            $p->name = $product->name;
                            $p->price = $product->price;
                            $p->custom_description = $product->custom_description;
                            $p->enable = 1;
                            $p->visible = 1;
                            $p->news = 1;
                            $flag = false;
                        }
                        $s = $style;
                        array_push($stylex, $s);
                    }
                }
                if ($p) {
                    $p->styles = $stylex;
                    array_push($productx, $p);
                }
            }
            $news_products = $productx;

            $session = $this->request->session();
            $shopping_cart = $session->read('shopping_cart');
            $customer = $session->read('customer');

            $categories = $this->Categories->find('list')->contain(['CategoriesAssets'])->limit(6);
            $seasons = $this->Seasons->find();
        }
        $this->set(compact('products', 'customer', 'categories', 'seasons', 'category_selected', 'groups', 'shopping_cart', 'news_products', 'order', 'category_id', 'search', 'promotions', 'search_word'));
        $this->set('_serialize', ['products', 'customer', 'categories', 'seasons', 'category_selected', 'groups', 'shopping_cart', 'news_products', 'order', 'category_id', 'search', 'promotions', 'search_word']);
    }
}
