<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CategoriesAssets Controller
 *
 * @property \App\Model\Table\CategoriesAssetsTable $CategoriesAssets
 */
class CategoriesAssetsController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        if ($this->request->session()->read('type_user') != 'admin' ) {
            if ($this->Auth) {
                $this->Auth->logout();				
            }
        }
        $this->viewBuilder()->layout('admin');
    }

    public function isAuthorized($user = null) 
    {
        return parent::isAuthorized($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Categories']
        ];
        $categoriesAssets = $this->paginate($this->CategoriesAssets);

        $this->set(compact('categoriesAssets'));
        $this->set('_serialize', ['categoriesAssets']);
    }

    /**
     * View method
     *
     * @param string|null $id Categories Asset id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $categoriesAsset = $this->CategoriesAssets->get($id, [
            'contain' => ['Categories']
        ]);

        $this->set('categoriesAsset', $categoriesAsset);
        $this->set('_serialize', ['categoriesAsset']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {

            foreach ($this->request->data['files'] as $file) {
                
                if ($file['tmp_name']) {

                    $categoryAsset = $this->CategoriesAssets->newEntity();

                    $ext = explode('.', $file['name']);
                    $now = new \DateTime();
                    $newName = $now->getTimestamp() .'-' . $ext[0] . '.' .  end($ext);
                    $from = $file['tmp_name'];
                    $to = WWW_ROOT . 'images/'. $newName ;

                    if (move_uploaded_file($from, $to)) {

                        $this->request->data['url'] = $newName;
                    }
                }

                $categoryAsset = $this->CategoriesAssets->patchEntity($categoryAsset, $this->request->data);
                $this->CategoriesAssets->save($categoryAsset);
            }
        }
        $this->Flash->success(__('Se ha guardado correctamente.'));
        return $this->redirect(['controller' => 'categories', 'action' => 'edit', $categoryAsset->category_id]);
    }

    /**
     * Edit method
     *
     * @param string|null $id Categories Asset id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $categoriesAsset = $this->CategoriesAssets->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $categoriesAsset = $this->CategoriesAssets->patchEntity($categoriesAsset, $this->request->data);
            if ($this->CategoriesAssets->save($categoriesAsset)) {
                $this->Flash->success(__('The categories asset has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The categories asset could not be saved. Please, try again.'));
        }
        $categories = $this->CategoriesAssets->Categories->find('list', ['limit' => 200]);
        $this->set(compact('categoriesAsset', 'categories'));
        $this->set('_serialize', ['categoriesAsset']);
    }

    /**
     * Delete method
     *
     *
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $categoryAsset = $this->CategoriesAssets->get($_POST['id']);

        if (unlink(WWW_ROOT . 'images/'. $categoryAsset->url)) {

            if ($this->CategoriesAssets->delete($categoryAsset)) {
                $this->Flash->success(__('The asset has been deleted.'));
            } else {
                $this->Flash->error(__('The asset could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(['controller' => 'categories', 'action' => 'edit', $categoryAsset->category_id]);
    }

    /**
     * Visible method
     *
     *
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function visible()
    {
        $categoryAsset = $this->CategoriesAssets->get($_POST['id']);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $categoryAsset = $this->CategoriesAssets->patchEntity($categoryAsset, $this->request->data);
            $categoryAsset->visible = !$categoryAsset->visible;
            if ($this->CategoriesAssets->save($categoryAsset)) {
                $this->Flash->success(__('The categories asset has been saved.'));
            } else {
                $this->Flash->error(__('The categories asset could not be saved. Please, try again.'));
            }
        }
        return $this->redirect(['controller' => 'categories', 'action' => 'edit', $categoryAsset->category_id]);
    }
}
