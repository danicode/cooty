<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Categories Controller
 *
 * @property \App\Model\Table\CategoriesTable $Categories
 */
class CategoriesController extends AppController
{

    public function initialize()
    {
        parent::initialize();

        if ($this->request->session()->read('type_user') != 'admin' ) {
            if ($this->Auth) {
                $this->Auth->logout();
            }
        }
        $this->viewBuilder()->layout('admin');
    }

    public function isAuthorized($user = null) 
    {
        return parent::isAuthorized($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $categories = $this->Categories->find()->contain(['Groups']);

        $this->set(compact('categories'));
        $this->set('_serialize', ['categories']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $category = $this->Categories->newEntity();
        if ($this->request->is('post')) {
            $category = $this->Categories->patchEntity($category, $this->request->data);
            if ($this->Categories->save($category)) {
                $this->Flash->success(__('La Categoria se ha añadido correctamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('La Categoria no se ha guardado. Por favor, intente nuevamente.'));
        }

        $groups =  $this->Categories->Groups->find('list');
        $this->set(compact('category', 'groups'));
        $this->set('_serialize', ['category', 'groups']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Category id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $category = $this->Categories->get($id, [
            'contain' => ['CategoriesAssets']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $category = $this->Categories->patchEntity($category, $this->request->data);
            if ($this->Categories->save($category)) {
                $this->Flash->success(__('La Categoria se ha modificado correctamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('La Categoria no se ha modificado. Por favor, intente nuevamente.'));
        }

        $groups =  $this->Categories->Groups->find('list');
        $this->set(compact('category', 'groups'));
        $this->set('_serialize', ['category', 'groups']);
    }

    /**
     * Delete method
     *
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $id = $_POST['id'];
        $this->__deleteCategoryAsset($id);
        $this->__deleteProducts($id);
        $category = $this->Categories->get($id);
        if ($this->Categories->delete($category)) {
            $this->Flash->success(__('La Categoria se ha eliminado correctamente.'));
        } else {
            $this->Flash->error(__('La Categoria no se ha eliminado. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Delete method
     *
     * @param string|null $category_id Category id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    private function __deleteCategoryAsset($category_id)
    {
        $this->loadModel('CategoriesAssets');
        $categoriesAssets = $this->CategoriesAssets->find()->where(['category_id' => $category_id]);

        foreach ($categoriesAssets as $ca) {
            if (unlink(WWW_ROOT . 'images/' . $ca->url)) {
                $this->CategoriesAssets->delete($ca);
            }
        }
    }

    /**
     * Delete method
     *
     * @param string|null $category_id Category id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    private function __deleteProducts($category_id)
    {
        $this->loadModel('Products');
        $products = $this->Products->find()->where(['category_id' => $category_id]);

        foreach ($products as $product) {
            $product->visible = false;
            $product->enable = false;
            $this->Products->save($product);
        }
    }
}
