<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
/**
 * StoreSync component
 */
class StoreSyncComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    private $_ctrl;
    private static $STORES = [
        '05' => 'Deposito Online',
        '10' => 'San Martín',
        '20' => 'Boggiani',
        '50' => 'Denis Roa',
        '70' => 'Ciudad del Este',
    ];

    public function initialize(array $config)
    {
        $this->_ctrl = $this->_registry->getController();
    }

    public function executeSync()
    {
        $this->_ctrl->loadModel('Articles');
        $this->_ctrl->loadModel('Stores');
        $this->_ctrl->loadModel('StoresHasArticles');
        $this->_ctrl->loadModel('SyncsLogs');

        $conn = ConnectionManager::get('main_store');

        // $articlesStocks = $conn->execute("SELECT s.UNIDADES AS article_amount,
        //     a.NUMERO AS article_code,
        //     a.DESCRIP AS article_description,
        //     s.DEPOSITO as deposito_id,
        //     s.DEPOSITO AS deposito_name,
        //     s.DEPOSITO AS store_number
        // FROM ARTICULO a 
        // JOIN STOCKS s ON s.ARTICULO = a.NUMERO
        // WHERE a.AUXILIAR2 = 'S'")->fetchAll('obj');

        $articlesStocks = $conn->execute("SELECT s.UNIDADES AS article_amount,
            a.NUMERO AS article_code,
            a.DESCRIP AS article_description,
            s.DEPOSITO as deposito_id,
            s.DEPOSITO AS deposito_name,
            s.DEPOSITO AS store_number
        FROM ARTICULO a 
        JOIN STOCKS s ON s.ARTICULO = a.NUMERO
        WHERE s.DEPOSITO = '05'")->fetchAll('obj');

        foreach ($articlesStocks as $articleStock ) {
            $result = $conn->execute("SELECT PRECIO, LISTA FROM LISTAS WHERE ARTICULO = '" . $articleStock->article_code . "' AND LISTA = '01'")->fetchAll('obj');
            if ($result) {
                $articleStock->article_price = intval($result[0]->PRECIO);
            }
        }

        foreach ($articlesStocks as $row) {

            $code = $row->article_code;

            //tengo que verificar si existe la sucursal que tiene el articulo, porque en nuestra tabla articles tiene un fk_store_id
            if ($this->__verifyStore($row)) {
                //se accede a la tabla intermedia y con Articles.code se relaciona el id del articulo con el codigo
                $article = $this->_ctrl->Articles->find()->contain(['StoresHasArticles'])->where(['code' => $code])->first();

                if ($article) {

                    $storeHasArticle = null;

                    foreach ($article->stores_has_articles as $sha) {
                        if ($sha->store_id == $row->store_number) {
                            $storeHasArticle = $sha;
                        }
                    }

                    if ($storeHasArticle) {
                        //modificar la cantidad del articulo en la sucursal

                        $store_has_article_changes = false;
                        $article_changes = false;

                        if ($row->article_amount != $storeHasArticle->last_sync) {
                            $store_has_article_changes = true;
                            $storeHasArticle->amount = $row->article_amount - $storeHasArticle->last_sync + $storeHasArticle->amount;
                            $storeHasArticle->last_sync = $row->article_amount;
                        }
                        if ($article->description != $row->article_description) {
                            $article->description = $row->article_description;
                            $article_changes = true;
                        }
                        if ($article->price != $row->article_price) {
                            $article->price = $row->article_price;
                            $article_changes = true;
                        }
                        if ($store_has_article_changes) {
                            if (!$this->_ctrl->Articles->StoresHasArticles->save($storeHasArticle)) {
                                $synclog = $this->_ctrl->SyncsLogs->newEntity();
                                $synclog->article_code = $code;
                                $synclog->store_name = $row->store_number;
                                $msg = 'No se pudo modificar la cantidad del articulo código: ' . $code . 'en la sucursal numero: ' . $row->store_number;
                                $synclog->description = $msg;
                                $now = new \DateTime();
                                $synclog->created = $now->format('Y-m-d H:i:s');
                                $this->_ctrl->SyncsLogs->save($synclog);
                            }
                        }
                        if ($article_changes) {
                            if (!$this->_ctrl->Articles->save($article)) {
                                $synclog = $this->_ctrl->SyncsLogs->newEntity();
                                $synclog->article_code = $code;
                                $synclog->store_id = $row->store_number;
                                $synclog->store_name = $row->store_number;
                                $msg = 'No se pudo modificar el articulo código: ' . $code;
                                $synclog->description = $msg;
                                $now = new \DateTime();
                                $synclog->created = $now->format('Y-m-d H:i:s');
                                $this->_ctrl->SyncsLogs->save($synclog);
                            }
                        }
                    } else {

                        $storeHasArticle = $this->_ctrl->Articles->StoresHasArticles->newEntity();

                        $storeHasArticle->store_id = $row->store_number;
                        $storeHasArticle->article_id = $article->id;
                        $storeHasArticle->amount = $row->article_amount - $storeHasArticle->last_sync + $storeHasArticle->amount;
                        $storeHasArticle->last_sync = $row->article_amount;

                        if (!$this->_ctrl->Articles->StoresHasArticles->save($storeHasArticle)) {
                            $synclog = $this->_ctrl->SyncsLogs->newEntity();
                            $synclog->article_code = $code;
                            $synclog->store_id = $row->store_number;
                            $synclog->store_name = $row->store_number;
                            $msg = 'No se pudo asociar el articulo código: ' . $code . 'a la sucursal id: ' . $row->store_number;
                            $synclog->description = $msg;
                            $now = new \DateTime();
                            $synclog->created = $now->format('Y-m-d H:i:s');
                            $this->_ctrl->SyncsLogs->save($synclog);
                        }
                    }
                } else {
                    //articulo nuevo
                    $art = $this->_ctrl->Articles->newEntity();
                    $art->code = $code;
                    $art->description = $row->article_description;
                    if (isset($row->article_price)) {
                        $art->price = $row->article_price;
                        $synclog = $this->_ctrl->SyncsLogs->newEntity();
                        $synclog->article_code = $code;
                        $synclog->store_name = $row->store_number;
                        $msg = 'Se ha cargar un nuevo articulo código: ' . $code . ' - verificar a que producto pertenece';
                        if ($this->_ctrl->Articles->save($art)) {
                            $storesHasArticles = $this->_ctrl->Articles->StoresHasArticles->newEntity();
                            $storesHasArticles->article_id = $art->id;
                            $storesHasArticles->store_id = $row->store_number;
                            $storesHasArticles->amount = $row->article_amount;
                            $storesHasArticles->last_sync = $row->article_amount;
                            if (!$this->_ctrl->Articles->StoresHasArticles->save($storesHasArticles)) {
                                $msg = 'No se pudo cargar el stock articulo código: ' . $code . 'en la sucursal numero: ' . $row->store_number;
                            }
                        } else {
                            $msg = 'No se pudo cargar el nuevo articulo código: ' . $code;
                        }
                        $synclog->description = $msg;
                        $now = new \DateTime();
                        $synclog->created = $now->format('Y-m-d H:i:s');
                        $this->_ctrl->SyncsLogs->save($synclog);
                    } else {
                        $synclog = $this->_ctrl->SyncsLogs->newEntity();
                        $synclog->article_code = $code;
                        $synclog->store_name = $row->store_number;
                        $msg = 'El articulo código: ' . $code . ' no se ha cargado debido a que no tiene precio.';
                        $synclog->description = $msg;
                        $now = new \DateTime();
                        $synclog->created = $now->format('Y-m-d H:i:s');
                        $this->_ctrl->SyncsLogs->save($synclog);
                    }
                }
            }
        }
    }

    /**
     * Verifiva si existe la sucursal del articulo, en caso de no existit la carga.
     * 
     * @param Object $current_store sucursal del articulo ha cargar
     * @return true se cargo o ya existia la sucursal | false error al cargar la sucursal
     */ 
    private function __verifyStore($current_store) 
    {
        $this->_ctrl->loadModel('Stores');
        $this->_ctrl->loadModel('SyncsLogs');
        $store = $this->_ctrl->Stores->find()->where(['number' => $current_store->store_number])->first();

        if (!$store) {
            //carga una nueva sucursal
            $store = $this->_ctrl->Stores->newEntity();
            $store->name = StoreSyncComponent::$STORES[$current_store->store_number];
            $store->number = $current_store->store_number;
            $store->id = $current_store->store_number;

            if (!$this->_ctrl->Stores->save($store)) {
                 $synclog = $this->_ctrl->SyncsLogs->newEntity();
                 $synclog->article_code = $code;
                 $synclog->store_name = $store->number;
                 $msg = 'No se pudo cargar la nueva sucursal numero' . $current_store->store_number;
                 $synclog->description = $msg;
                 $now = new \DateTime();
                 $synclog->created = $now->format('Y-m-d H:i:s');
                 $this->_ctrl->SyncsLogs->save($synclog);
                 $this->_ctrl->Flash->error(__($msg));
                 return false;
            }
        }

        return true;
    }
}
