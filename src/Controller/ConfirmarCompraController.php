<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;


class ConfirmarCompraController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->layout('ecommerce');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index']);
    }

    public function index($error = null)
    {
        $this->loadModel('Categories');
        $this->loadModel('Seasons');
        $this->loadModel('Groups');
        $this->loadModel('CoversPages');

        $groups = $this->Groups->find()->contain(['Categories', 'Categories.CategoriesAssets'])->limit(6);
        $coversPages = $this->CoversPages->find()->where(['visible' => 1]);
        $session = $this->request->session();
        $shopping_cart = $session->read('shopping_cart');
        $type_user = $session->read('type_user');
        $customer = $this->Auth->user();

        $categories = $this->Categories->find('list')->contain(['CategoriesAssets'])->limit(6);
        $seasons = $this->Seasons->find();

        $data = array();
        if ($customer && $type_user == 'customers') {
            $data['customer_name'] = $customer->name;
            $data['address'] = $customer->address;
            $data['email'] = $customer->email;
            $data['phone'] = $customer->phone;
        }

        $this->set(compact('categories', 'seasons', 'customer', 'groups', 'shopping_cart', 'data', 'coversPages'));
        $this->set('_serialize', ['seasons', 'customer', 'categories', 'groups', 'shopping_cart', 'data', 'coversPages']);
    }
}
