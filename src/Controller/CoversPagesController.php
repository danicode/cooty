<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CoversPages Controller
 *
 * @property \App\Model\Table\CoversPagesTable $CoversPages
 */
class CoversPagesController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        if ($this->request->session()->read('type_user') != 'admin' ) {
            if ($this->Auth) {
                $this->Auth->logout();
            }
        }
        $this->viewBuilder()->layout('admin');
    }

    public function isAuthorized($user = null) 
    {
        return parent::isAuthorized($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $coversPages = $this->CoversPages->find();

        $this->set(compact('coversPages'));
        $this->set('_serialize', ['coversPages']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {

            foreach ($this->request->data['files'] as $file) {

                if ($file['tmp_name']) {

                    $coverPage = $this->CoversPages->newEntity();

                    $ext = explode('.', $file['name']);

                    $now = new \DateTime();
                    $newName = $now->getTimestamp() .'-' . $ext[0] . '.' .  end($ext);
                    $from = $file['tmp_name'];
                    $to = WWW_ROOT . 'images/'. $newName ;

                    if (move_uploaded_file($from, $to)) {
                        $this->request->data['url'] = $newName;
                    }
                }

                $coverPage = $this->CoversPages->patchEntity($coverPage, $this->request->data);
                if ($this->request->data['section'] == 'principal') {
                    $coverPage->principal = 1;
                    $coverPage->nosotros = 0;
                }
                if ($this->request->data['section'] == 'nosotros') {
                    $coverPage->nosotros = 1;
                    $coverPage->principal = 0;
                }
                if ($this->CoversPages->save($coverPage)) {
                    $this->Flash->success(__('Se ha guardado correctamente.'));
                } else {
                    $this->Flash->error(__('No se ha guardado la imagen. Por favor, intente nuevamente.'));
                }
            }
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Delete method
     *
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $coverPage = $this->CoversPages->get($_POST['id']);

        if (unlink(WWW_ROOT . 'images/'. $coverPage->url)) {

            if ($this->CoversPages->delete($coverPage)) {
                $this->Flash->success(__('Se ha eliminado correctamente la imagen.'));
            } else {
                $this->Flash->error(__('No se ha eliminado la imagen. Por favor, intente nuevamente.'));
            }
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Visible method
     *
     *
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function visible()
    {
        $coverPage = $this->CoversPages->get($_POST['id']);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $coverPage = $this->CoversPages->patchEntity($coverPage, $this->request->data);
            $coverPage->visible = !$coverPage->visible;
            if ($coverPage->visible) {

                $colum = 'principal';
                if ($coverPage->nosotros) {
                    $colum = "nosotros";
                }
                $covers = $this->CoversPages->find()->where(['visible' => 1, $colum => 1]);
                if ($covers->count() > 0) {
                    foreach ($covers as $cover) {
                        $cover->visible = 0;
                        $this->CoversPages->save($cover);
                    }
                }
            }
            if ($this->CoversPages->save($coverPage)) {
                $this->Flash->success(__('Se ha modificado la visibilidad correctamente.'));
            } else {
                $this->Flash->error(__('No se ha modificado la visibilidad. por favor, intente nuevamente.'));
            }
        }
        return $this->redirect(['action' => 'index']);
    }
}
