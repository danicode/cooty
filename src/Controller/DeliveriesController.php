<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Deliveries Controller
 *
 * @property \App\Model\Table\DeliveriesTable $Deliveries
 */
class DeliveriesController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        if ($this->request->session()->read('type_user') != 'admin') {
            if ($this->Auth) {
                $this->Auth->logout();
            }
        } 
        $this->viewBuilder()->layout('admin');
    }

    public function isAuthorized($user = null) 
    {
        return parent::isAuthorized($user['id']);
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $deliveries = $this->paginate($this->Deliveries);

        $this->set(compact('deliveries'));
        $this->set('_serialize', ['deliveries']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $delivery = $this->Deliveries->newEntity();
        if ($this->request->is('post')) {
            $delivery = $this->Deliveries->patchEntity($delivery, $this->request->data);
            if ($this->Deliveries->save($delivery)) {
                $this->Flash->success(__('El Repartidor se ha guardado correctamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El Repartidor no se ha guardado. Por favor, intente nuevamente.'));
        }
        $this->set(compact('delivery'));
        $this->set('_serialize', ['delivery']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Delivery id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $delivery = $this->Deliveries->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $delivery = $this->Deliveries->patchEntity($delivery, $this->request->data);
            if ($this->Deliveries->save($delivery)) {
                $this->Flash->success(__('El Repartido se ha modificado correctamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El Repartidor no se ha modificado. Por favor, intente nuevamente.'));
        }
        $this->set(compact('delivery'));
        $this->set('_serialize', ['delivery']);
    }

    /**
     * Delete method
     *
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $id = $_POST['id'];
        $delivery = $this->Deliveries->get($id);
        if ($this->Deliveries->delete($delivery)) {
            $this->Flash->success(__('El Repartidor se ha eliminado correctamente.'));
        } else {
            $this->Flash->error(__('El Repartido no se ha eliminado. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
