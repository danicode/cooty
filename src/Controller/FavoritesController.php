<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Favorites Controller
 *
 * @property \App\Model\Table\FavoritesTable $Favorites
 */
class FavoritesController extends AppController
{

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $favorite = $this->Favorites->newEntity();
        if ($this->request->is('post')) {
            $customer = $this->Auth->user();
            $session = $this->request->session();
            $type_user = $session->read('type_user');
            if ($customer && $type_user == 'customers') {
                $favorite->customer_id = $customer['id'];
                $favorite->article_id = $_POST['id'];
                if ($this->Favorites->save($favorite)) {
                    $this->loadModel('Articles');
                    $article = $this->Articles->get($favorite->article_id, [
                        'contain' => ['Styles.Assets']
                    ]);
                    $favorite->article = $article;
                    array_push($customer['favorites'], $favorite);
                    $session->write('customer', $customer);
                    $type_notification = __('success');
                    $response = __('El articulo se ha añadido a tu lista de favoritos.');
                } else {
                    $type_notification = "warning";
                    $response = __('Error, al añadir el articulo a la lista de favoritos. Por favor, intenta nuevamente.');
                    $errors = $favorite->errors();

                    if ($favorite->errors()) {
                        $error_msg = [];
                        foreach($favorite->errors() as $errors) {
                            if (is_array($errors)) {
                                foreach($errors as $error) {
                                    $error_msg[] = '<br>' . $error;
                                }
                            } else {
                                $error_msg[] = $errors;
                            }
                        }

                        if (!empty($error_msg)) {
                            $response = __("Mensaje: " . implode("\n \r", $error_msg));
                        }
                    }
                }
            } else {
                $type_notification = __('error');
                $response = __('Debe tener una cuenta para poder agregar articulos a la lista de favoritos.');
            }
        }
        $this->set(compact('customer', 'type_notification', 'response'));
        $this->set('_serialize', ['customer', 'type_notification', 'response']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Favorite id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $favorite = $this->Favorites->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $favorite = $this->Favorites->patchEntity($favorite, $this->request->data);
            if ($this->Favorites->save($favorite)) {
                $this->Flash->success(__('The favorite has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The favorite could not be saved. Please, try again.'));
        }
        $this->set(compact('favorite'));
        $this->set('_serialize', ['favorite']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Favorite id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $customer = $this->Auth->user();
        $session = $this->request->session();
        $type_user = $session->read('type_user');
        $article_id = $_POST['id'];
        if ($customer && $type_user == 'customers') {
            $favorite = $this->Favorites->find()->where(['customer_id' => $customer['id'], 'article_id' => $article_id])->first();
            if ($this->Favorites->delete($favorite)) {
                $this->loadModel('Customers');
                $i;
                foreach ($customer['favorites'] as $index => $favorite) {
                    if ($favorite->article->id == $article_id) {
                        $i = $index;
                        break;
                    }
                }
                unset($customer['favorites'][$i]);
                $this->Auth->setUser($customer);
                $this->Flash->success(__('El articulo se ha eliminado correctamente de la lista de favoritos.'));
            } else {
                $this->Flash->error(__('Error al eliminar el articulo de la lista de favoritos. Por favor, intente nuevamente.'));
            }
        } else {
            $this->Flash->error(__('Debe tener una cuenta para poder agregar articulos a la lista de favoritos.'));
        }

        return $this->redirect(['controller' => 'mi-cuenta', 'action' => 'favoritos']);
    }
}
