<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;

/**
 * Home Controller
 *
 * @property \App\Model\Table\HomeTable $Home
 */
class HomeController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->layout('ecommerce');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['index', 'nosotros', 'contactos', 'locales', 'newsletter']);
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->layout('ecommerce');
        //$this->loadModel('CoversPages');
        $this->loadModel('Seasons');
        $this->loadModel('Categories');
        $this->loadModel('Groups');
        $this->loadModel('Products');
        $this->loadModel('Promotions');
        $this->loadModel('Styles');

        $groups = $this->Groups->find()->contain(['Categories', 'Categories.CategoriesAssets'])->limit(6);
        $products = $this->Products->find()
            ->contain([
                'Styles.Assets',
                'Styles.Articles'
                ])
            ->where([
                'Products.news' => 1, 
                'Products.visible' => 1,
                'Products.enable' => 1
        ]);

        $productx = array();
        foreach ($products as $product) {
            $flag = true;
            $stylex = array();
            $p = null;
            foreach ($product->styles as $style) {
                if (count($style->assets) > 0 && count($style->articles)) {
                    if ($flag) {
                        $s = $this->Styles->newEntity();
                        $p = $this->Products->newEntity();
                        $p->id = $product->id;
                        $p->name = $product->name;
                        $p->price = $product->price;
                        $p->custom_description = $product->custom_description;
                        $p->enable = 1;
                        $p->visible = 1;
                        $p->news = 1;
                        $flag = false;
                    }
                    $s = $style;
                    array_push($stylex, $s);
                }
            }
            if ($p) {
                $p->styles = $stylex;
                array_push($productx, $p);
            }
        }
        $products = $productx;

        $promotions = $this->Promotions->find()->contain(['PromotionsAssets'])->where(['enable' => 1, 'deleted' => 0]);

        $session = $this->request->session();
        $shopping_cart = $session->read('shopping_cart');
        $customer = $this->Auth->user();

        //$coversPages = $this->CoversPages->find()->where(['visible' => 1]);
        $seasons = $this->Seasons->find()->limit(3);
        foreach ($seasons as $season) {
            $productx = array();
            $seasons_products = $this->Products->find()->contain(['Styles.Assets', 'Styles.Articles'])->where(['season_id' => $season->id, 'visible' => 1, 'enable' => 1])->limit(3);
            foreach ($seasons_products as $product) {
                $flag = true;
                $stylex = array();
                $p = null;
                foreach ($product->styles as $style) {
                    if (count($style->assets) > 0 && count($style->articles)) {
                        if ($flag) {
                            $s = $this->Styles->newEntity();
                            $p = $this->Products->newEntity();
                            $p->id = $product->id;
                            $p->name = $product->name;
                            $p->price = $product->price;
                            $p->custom_description = $product->custom_description;
                            $p->enable = 1;
                            $p->visible = 1;
                            $p->news = 1;
                            $flag = false;
                        }
                        $s = $style;
                        array_push($stylex, $s);
                    }
                }
                if ($p) {
                    $p->styles = $stylex;
                    array_push($productx, $p);
                }
            }
            $season->products = $productx;
        }

        $this->set(compact('coversPages', 'seasons', 'customer', 'groups', 'shopping_cart', 'products', 'promotions'));
        $this->set('_serialize', ['coversPages', 'seasons', 'customer', 'groups', 'shopping_cart', 'products', 'promotions']);
    }

    public function nosotros() 
    {
        $this->loadModel('CoversPages');
        $this->loadModel('Seasons');
        $this->loadModel('Categories');
        $this->loadModel('Groups');
        $this->loadModel('Promotions');

        $groups = $this->Groups->find()->contain(['Categories', 'Categories.CategoriesAssets'])->limit(6);
        $promotions = $this->Promotions->find()->contain(['PromotionsAssets'])->where(['enable' => 1, 'deleted' => 0]);

        $session = $this->request->session();
        $shopping_cart = $session->read('shopping_cart');
        $customer = $this->Auth->user();

        $coversPages = $this->CoversPages->find()->where(['visible' => 1]);
        $seasons = $this->Seasons->find();

        $this->set(compact('coversPages', 'seasons', 'customer', 'groups', 'shopping_cart', 'promotions'));
        $this->set('_serialize', ['coversPages', 'seasons', 'customer', 'groups', 'shopping_cart', 'promotions']);
    }

    public function contactos() 
    {
        //$this->loadModel('CoversPages');
        $this->loadModel('Seasons');
        $this->loadModel('Categories');
        $this->loadModel('Groups');
        $this->loadModel('Promotions');

        $groups = $this->Groups->find()->contain(['Categories', 'Categories.CategoriesAssets'])->limit(6);
        $promotions = $this->Promotions->find()->contain(['PromotionsAssets'])->where(['enable' => 1, 'deleted' => 0]);

        $session = $this->request->session();
        $shopping_cart = $session->read('shopping_cart');
        $customer = $this->Auth->user();

        //$coversPages = $this->CoversPages->find()->where(['visible' => 1]);
        $seasons = $this->Seasons->find();

        $this->set(compact('coversPages', 'seasons', 'customer', 'groups', 'shopping_cart', 'promotions'));
        $this->set('_serialize', ['coversPages', 'seasons', 'customer', 'groups', 'shopping_cart', 'promotions']);
    }
    
    public function locales() 
    {
        //$this->loadModel('CoversPages');
        $this->loadModel('Seasons');
        $this->loadModel('Categories');
        $this->loadModel('Groups');
        $this->loadModel('Sucursales');
        $this->loadModel('Promotions');

        $groups = $this->Groups->find()->contain(['Categories', 'Categories.CategoriesAssets'])->limit(6);
        $promotions = $this->Promotions->find()->contain(['PromotionsAssets'])->where(['enable' => 1, 'deleted' => 0]);
        $sucursales = $this->Sucursales->find()->where(['enable' => true]);

        $session = $this->request->session();
        $shopping_cart = $session->read('shopping_cart');
        $customer = $this->Auth->user();

        //$coversPages = $this->CoversPages->find()->where(['visible' => 1]);
        $seasons = $this->Seasons->find();

        $this->set(compact('coversPages', 'seasons', 'customer', 'groups', 'shopping_cart', 'sucursales', 'promotions'));
        $this->set('_serialize', ['coversPages', 'seasons', 'customer', 'groups', 'shopping_cart', 'sucursales', 'promotions']);
    }

    public function newsletter()
    {
        if ($this->request->is('post')) {
            $email = $this->request->data['email'];
            $email_enterprise = "info@cootchycoo.com";

            $titulo = 'Cootchy Coo - Newsletter';

        	$html = "
        	<html>
        		<head>
        			<title>" . $titulo . "</title>
        		</head>
        		<body>
        			<p>Nuevo correo para agendar en el NewsLetter - Correo: " . $email . "</p>
        		</body>
        	</html>";

        	$headers = "MIME-Version: 1.0" . "\r\n";
        	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    
        	// More headers
        	$headers .= 'From: <contacto@thevestidor.com>' . "\r\n";
        	//$headers .= 'Cc: myboss@example.com' . "\r\n";
    
        	// $headers = 'From: contacto@vialeros.com' . "\r\n" .
    	    // 'Reply-To: webmaster@example.com' . "\r\n" .
    	    // 'X-Mailer: PHP/' . phpversion();

            return	mail($email_enterprise, $titulo, $html, $headers);
        }
    }
}