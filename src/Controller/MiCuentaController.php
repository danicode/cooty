<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;

class MiCuentaController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->Auth->config('loginAction', [
            'controller' => 'MiCuenta',
            'action' => 'login'
        ]);

        $this->viewBuilder()->layout('ecommerce');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['login', 'add', 'edit', 'favoritos', 'recuperarClave']);
    }

    public function add()
    {
        if ($this->request->is('Ajax')) {

            $this->loadModel('Customers');
            $customer = $this->Customers->newEntity();

            $data = [];

            $customer = $this->Customers->patchEntity($customer, $this->request->data);
            if ($this->Customers->save($customer)) {
                $customer = $this->Customers->get($customer->id, [
                    'contain' => ['Favorites.Articles.Styles.Assets']
                ]);
                $this->Auth->setUser($customer);
                $this->request->session()->write('type_user', 'customers');

                $data['customer'] = $customer;
                $data['type_notification'] = "success";
                $data['response'] = "Se ha registrado correctamente.";
                $this->sendMailWelcome($customer, $customer->email);
            } else {
                $data['type_notification'] = "error";
                $data['response'] = "No se pudo registrar.";
                $data['errors'] = $customer->errors();

                if ($customer->errors()) {
                    $error_msg = [];
                    foreach( $customer->errors() as $errors) {
                        if (is_array($errors)) {
                            foreach($errors as $error) {
                                if ($error != 'The provided value is invalid') {
                                    $error_msg[] = '<br>' . $error;
                                }
                            }
                        } else {
                            $error_msg[] = $errors;
                        }
                    }

                    if (!empty($error_msg)) {
                        $data['response'] =__("Error: " . implode("\n \r", $error_msg));
                    }
                }
            }

            $this->set(compact('data'));
            $this->set('_serialize', ['data']);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Delivery id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->loadModel('Customers');
        $customer = $this->Customers->get($this->Auth->user()->id, [
            'contain' => ['Favorites.Articles.Styles.Assets']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $customer = $this->Customers->patchEntity($customer, $this->request->data);
            if ($this->Customers->save($customer)) {
                $this->Flash->success(__('Se ha modificado correctamente su perfil.'));

                return $this->redirect(['controller' => 'mi-cuenta', 'action' => 'edit', $customer->id]);
            }
            $this->Flash->error(__('Error al modificar su perfil. Por favor, intente nuevamente.'));
        }

        $this->loadModel('CoversPages');
        $this->loadModel('Seasons');
        $this->loadModel('Categories');
        $this->loadModel('Groups');
        $this->loadModel('Orders');
        
        $status = [
            OrdersController::PENDING_STATUS => 'PENDIENTE',
            OrdersController::ASSIGNED_STATUS => 'RECIBIDO',
            OrdersController::DISPACHED_STATUS => 'DESPACHADO',
            OrdersController::COMPLETED_STATUS => 'COMPLETADO',
            OrdersController::CANCELED_STATUS => 'CANCELADO',
        ];

        $orders = $this->Orders->find()->contain(['OrdersHasArticles'])->where(['customer_id' => $customer->id]);
        $groups = $this->Groups->find()->contain(['Categories', 'Categories.CategoriesAssets'])->limit(6);

        $session = $this->request->session();
        $shopping_cart = $session->read('shopping_cart');

        $coversPages = $this->CoversPages->find();
        $seasons = $this->Seasons->find();

        $this->set(compact('coversPages', 'seasons', 'customer', 'groups', 'shopping_cart', 'orders', 'status'));
        $this->set('_serialize', ['coversPages', 'seasons', 'customer', 'groups', 'shopping_cart', 'orders', 'status']);
    }

    public function login() 
    {
        //Ajax Detection
        if ($this->request->is('Ajax')) {

            $email = $this->request->input('json_decode')->email;
            $password = $this->request->input('json_decode')->password;

            $data = [];

            $this->loadModel('Customers');
            $customer = $this->Customers->find()->contain(['Favorites.Articles.Styles.Assets'])->where(['email' => $email, 'deleted' => false])->first();

            if ($customer) {
                if ((new DefaultPasswordHasher)->check($password, $customer->password)) {

                    $this->Auth->setUser($customer);

                    $this->request->session()->write('type_user', 'customers');

                    $data['customer'] = $customer;
                    $data['type_notification'] = "success";
                    $data['response'] = "Hola";
                } else {
                    $data['type_notification'] = "warning";
                    $data['response'] = "El correo o la clave son incorrectas.";
                }
            } else {
                $data['type_notification'] = "warning";
                $data['response'] = "El correo o la clave son incorrectas.";
            }

            $this->set(compact('data'));
            $this->set('_serialize', ['data']);
        }
    }

    public function logout() 
    {
        //Ajax Detection
        if ($this->request->is('Ajax')) {
            parent::activityRecord('Logout customers.', 'Logout');
            $this->Auth->logout();

            $data['type_notification'] = "success";
            $data['response'] = "La sesión se cerro correctamente";

            $this->set(compact('data'));
            $this->set('_serialize', ['data']);
        }
    }

    public function favoritos() 
    {
        $this->loadModel('Customers');
        $customer = $this->Customers->get($this->Auth->user()['id'], [
            'contain' => ['Favorites.Articles.Styles.Assets']
        ]);

        $session = $this->request->session();
        $type_user = $session->read('type_user');
        $shopping_cart = $session->read('shopping_cart');
        $groups;
        $seasons;

        if ($customer && $type_user == 'customers') {
            $this->loadModel('Groups');
            $this->loadModel('Seasons');
            $groups = $this->Groups->find()->contain(['Categories', 'Categories.CategoriesAssets'])->limit(6);
            $seasons = $this->Seasons->find()->limit(6);
        } else {
            $this->Flash->warning(__('Debe tener una cuenta para poder tener una lista de favoritos.'));
            return $this->redirect(['controller' => 'home', 'action' => 'index']);
        }

        $this->set(compact('customer', 'shopping_cart', 'groups', 'seasons'));
        $this->set('_serialize', ['customer', 'shopping_cart', 'groups']);
    }

    public function recuperarClave() 
    {
        if ($this->request->is('post')) {
            $this->loadModel('Customers');
            $email_passed = $_POST['email'];
            $controller = $_POST['controller'];
            $action = $_POST['action'];
            $customer = $this->Customers->find()->contain(['Favorites.Articles.Styles.Assets'])->where(['email' => $email_passed, 'deleted' => false])->first();

            if ($customer) {
                $password = $this->_generateRandomString();
                $customer->password = $password;

                if ($this->Customers->save($customer)) {
                    // $email = new Email();
                    // $email->transport('gmail');
                    // $email
                    //     ->template('forget_password')
                    //     ->emailFormat('html')
                    //     ->subject('Cootchy Coo - Nueva Clave restablecida')
                    //     ->to($customer->email)
                    //     ->from(['info@cootchycoo.com' => 'www.cootchycoo.com'])
                    //     ->viewVars(['psw' => $password])
                    //     ->send();
                    $this->sendMailResetPassword($customer, $customer->email);
                    $this->Flash->success(__('Se ha asignado una nueva Clave, por favor revise su correo.'));
                } else {
                    $this->Flash->warning(__('Ocurrió un error al restablecer su Clave, Por favor intente de nuevo.'));
                }
            } else {
                $this->Flash->warning(__('El correo electrónico no existe. Intente de nuevo'));
            }
        }
        return $this->redirect(['controller' => $controller, 'action' => $action]);
    }

    private function _generateRandomString($length = 4) 
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private function sendMailResetPassword($customer, $email) {

    	$titulo = 'Cootchy Coo';

    	$html = "
    	<html>
    		<head>
    			<title>" . $titulo . "</title>
    		</head>
    		<body>
    			<p>Contraseña restablecida!.<br> Usa esta contraseña para ingresar y luego cambiala por seguridad.</p>
    			<br>
    			<br>
    			<p>Tu nueva contraseña: <b>" . $customer->password . "</b></p>
    		</body>
    	</html>";

    	$headers = "MIME-Version: 1.0" . "\r\n";
    	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

    	// More headers
    	$headers .= 'From: <contacto@thevestidor.com>' . "\r\n";
    	//$headers .= 'Cc: myboss@example.com' . "\r\n";

    	// $headers = 'From: contacto@vialeros.com' . "\r\n" .
	    // 'Reply-To: webmaster@example.com' . "\r\n" .
	    // 'X-Mailer: PHP/' . phpversion();
	    $return = false;
	    try {
	        $return = mail($email, $titulo, $html, $headers);
	    } catch(Exception $e) {
	        $return = false;
	    }

        return	$return;
    }

    private function sendMailWelcome($customer, $email) {

    	$titulo = 'Cootchy Coo';

    	$html = "
    	<html>
    		<head>
    			<title>" . $titulo . "</title>
    		</head>
    		<body>
    			<p>Te damos la bienvenida " . $customer->name . " gracias por registrarte.</p>
    		</body>
    	</html>";

    	$headers = "MIME-Version: 1.0" . "\r\n";
    	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

    	// More headers
    	$headers .= 'From: <contacto@thevestidor.com>' . "\r\n";
    	//$headers .= 'Cc: myboss@example.com' . "\r\n";

    	// $headers = 'From: contacto@vialeros.com' . "\r\n" .
	    // 'Reply-To: webmaster@example.com' . "\r\n" .
	    // 'X-Mailer: PHP/' . phpversion();
	    
        $return = false;
	    try {
	        $return = mail($email, $titulo, $html, $headers);
	    } catch(Exception $e) {
	        $return = false;
	    }

        return	$return;
    }
}
