<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 */
class OrdersController extends AppController
{

    /**
     * Estado cuando recien se crea el pedido el administrador debe asignar sucursal, delivery
     */
    const PENDING_STATUS = 0;

    /**
     * Estado cuando administrador asigna sucursal y delivery
     */
    const ASSIGNED_STATUS = 1;

    /**
     * Estado cuando el usuario de la sucursal entrega el pedido al delivery para que realice la entrega
     */
    const DISPACHED_STATUS = 2;

    /**
     * Estado cuando el delivery entrega la plata a la sucursal, entonces usuario de la sucursal debe asignar el estado de completado al pedido.
     */
    const COMPLETED_STATUS = 3;

    /**
     * Estado cuando ocurre algun inconveniente y no se concreta la compra y el delivery devuelve el o los productos a la sucursal, entonces el usuario de sucursal asigna el estado de cancelado al pedido.
     */
    const CANCELED_STATUS = 4;

    public function initialize()
    {
        parent::initialize();
        if (($this->request->session()->read('type_user') != 'admin') 
        && !(($this->request->session()->read('type_user') == 'customers')) && ($this->request->action == 'add')) {
            if ($this->Auth) {
                $this->Auth->logout();
            }
        }
        $this->viewBuilder()->layout('admin');
    }

    public function isAuthorized($user = null)
    {
        return parent::isAuthorized($user['id']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['add']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->loadModel('UsersHasStores');
        $user = $this->Auth->user();
        $orders;
        if (array_key_exists('admin', $user) && $user['admin']) {
            $orders = $this->Orders->find()->contain(['OrdersHasArticles', 'Customers', 'Deliveries', 'Stores']);
        } else {
            $stores_available = $this->UsersHasStores->find()->where(['user_id' => $user['id']]);
            $stores_available->select(['store_id']);
            $orders = $this->Orders->find()->contain(['OrdersHasArticles', 'Customers', 'Deliveries', 'Stores'])->where(['store_id IN' => $stores_available]);
        }

        $status = [
            OrdersController::PENDING_STATUS => 'PENDIENTE',
            OrdersController::ASSIGNED_STATUS => 'ASIGNADO',
            OrdersController::DISPACHED_STATUS => 'DESPACHADO',
            OrdersController::COMPLETED_STATUS => 'COMPLETADO',
            OrdersController::CANCELED_STATUS => 'CANCELADO',
        ];

        $this->set(compact('orders', 'status'));
        $this->set('_serialize', ['orders', 'status']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $this->loadModel('OrdersHasArticles');
            $order = $this->Orders->newEntity();
            $session = $this->request->session();
            $shopping_cart = $session->read('shopping_cart');
            $customer = $this->Auth->user();
            $type_user = $session->read('type_user');

            $total = 0;
            if ($shopping_cart) {
                foreach ($shopping_cart as $article) {
                    $total += $article->price * $article->amount;
                }
            }
            $order->total = $total;
            $order->status = OrdersController::PENDING_STATUS;

            if ($customer && $type_user == 'customers') {
                $order->customer_id = $customer->id;
            }

            $order->customer_name = $this->request->data['customer_name'];
            $order->address = $this->request->data['address'];
            $order->email = $this->request->data['email'];
            $order->phone = $this->request->data['phone'];

            $idx = $this->request->data['payment_methods'];
            $payment_methods = array(
                0 => __('Contado Efectivo'),
                1 => __('Tarjeta')
            );
            $order->payment_methods = $payment_methods[$idx];

            if ($this->Orders->save($order)) {

                $this->loadModel('OrdersStatus');
                $ordersStatus = $this->OrdersStatus->newEntity();
                $ordersStatus->order_id = $order->id;
                $ordersStatus->status = OrdersController::PENDING_STATUS;
                $this->OrdersStatus->save($ordersStatus);

                foreach ($shopping_cart as $article) {
                    $orderHasArticle = $this->OrdersHasArticles->newEntity();
                    $orderHasArticle->order_id = $order->id;
                    $orderHasArticle->article_id = $article->id;
                    $orderHasArticle->amount = $article->amount;
                    $orderHasArticle->code = $article->code;
                    $orderHasArticle->price = $article->price;
                    $orderHasArticle->url = $article->style->assets[0]->url;
                    $orderHasArticle->name_display = ($article->name_display) ? $article->name_display : $article->description;
                    $this->OrdersHasArticles->save($orderHasArticle);
                }
                $this->Flash->success(__('Se realizo con éxito la compra'));
            } else {
                $this->Flash->error(__('Error, no se efectuo la compra.'));
            }

            $shopping_cart = array();
            $session->write('shopping_cart', $shopping_cart);
        }

        
        return $this->redirect(['controller' => 'home', 'action' => 'index']);
    }

    public function assign()
    {
        $user = $this->Auth->user();
        if (array_key_exists('admin', $user) && $user['admin']) {
            //Ajax Detection
            if ($this->request->is('Ajax')) {
                $id = $_POST['id'];
                $this->loadModel('OrdersHasArticles');

                $orders_details = $this->OrdersHasArticles->find()->where(['order_id' => $id]);

                $this->loadModel('Stores');
                $stores = $this->Stores->find()->contain(['StoresHasArticles']);

                $stores_availables = array();

                foreach ($stores as $store) {
                    $prev = array();
                    foreach ($store->stores_has_articles as $sha) {
                        foreach ($orders_details as $oha) {
                            if ($sha->article_id == $oha->article_id && $sha->amount >= $oha->amount) {
                                array_push($prev, $sha->article_id);
                            }
                        }
                    }
                    if (count($prev) == $orders_details->count()) {
                        array_push($stores_availables, $store);
                    }
                }
                $data['stores_availables'] = $stores_availables;

                $this->loadModel('Deliveries');
                $deliveries = $this->Deliveries->find()->where(['enable' => 1]);
                $orders = $this->Orders->find();

                foreach ($deliveries as $delivery) {
                    $delivery->assigned_order_amount = 0;
                    foreach ($orders as $order) {
                        if ($order->delivery_id == $delivery->id && ($order->status != OrdersController::CANCELED_STATUS && $order->status != OrdersController::COMPLETED_STATUS)) {
                            $delivery->assigned_order_amount++;
                        }
                    }
                }
                $data['deliveries'] = $deliveries;
            }
        } else {
            $this->Flash->error(__('Error, Únicamente el administrador puede asignar al pedido sucursal y repartidor.'));
        }

        $this->set(compact('data'));
        $this->set('_serialize', 'data');
    }

    public function assignment()
    {
        if ($this->request->is('post')) {
            $store_id = $_POST['store_id'];
            $delivery_id = $_POST['delivery_id'];
            $order_id = $_POST['order_id'];
            $order = $this->Orders->get($order_id);
            $order->store_id = $store_id;
            $order->delivery_id = $delivery_id;
            $order->status = OrdersController::ASSIGNED_STATUS;

            if ($this->Orders->save($order)) {

                $this->loadModel('OrdersStatus');
                $user = $this->Auth->user();
                $ordersStatus = $this->OrdersStatus->newEntity();
                $ordersStatus->order_id = $order->id;
                $ordersStatus->user_id = $user['id'];
                $ordersStatus->status = OrdersController::ASSIGNED_STATUS;
                $this->OrdersStatus->save($ordersStatus);

                $this->Flash->success(__('Se han asignado al pedido, el repartidor y la sucursal.'));
            } else {
                $this->Flash->error(__('Error al asignar al pedido, el repartidor y la sucursal.'));
            }
            return $this->redirect(['controller' => 'orders', 'action' => 'index']);
        }

        $this->set(compact('data'));
        $this->set('_serialize', 'data');
    }

    public function changeStatus() 
    {
        if ($this->request->is('post')) {
            $status_info = [
                OrdersController::PENDING_STATUS => 'PENDIENTE',
                OrdersController::ASSIGNED_STATUS => 'ASIGNADO',
                OrdersController::DISPACHED_STATUS => 'DESPACHADO',
                OrdersController::COMPLETED_STATUS => 'COMPLETADO',
                OrdersController::CANCELED_STATUS => 'CANCELADO',
            ];
            $id = $_POST['id'];
            $status = intval($_POST['status']);
            $order = $this->Orders->get($id, [
                'contain' => ['OrdersHasArticles']
            ]);
            $prev_status = $order->status;
            $order->status = $status;
            $store_id = $order->store_id;

            if ($status == OrdersController::CANCELED_STATUS) {
                $order->delivery_id = null;
                $order->store_id = null;
            }

            if ($this->Orders->save($order)) {

                $this->loadModel('OrdersStatus');
                $user = $this->Auth->user();
                $ordersStatus = $this->OrdersStatus->newEntity();
                $ordersStatus->order_id = $order->id;
                $ordersStatus->user_id = $user['id'];
                $ordersStatus->status = $status;
                $now = new \DateTime();
                $ordersStatus->created = $now->format('Y-m-d H:i:s');
                $this->OrdersStatus->save($ordersStatus);

                if ($status == OrdersController::CANCELED_STATUS || $status == OrdersController::DISPACHED_STATUS) {
                    $this->loadModel('StoresHasArticles');
                    foreach ($order->orders_has_articles as $oha) {
                        $storeHasArticle = $this->StoresHasArticles->find()->where(['store_id' => $store_id, 'article_id' => $oha->article_id])->first();

                        if ($status == OrdersController::CANCELED_STATUS && ($prev_status == OrdersController::DISPACHED_STATUS)) {
                            //tiene que sumar al stock de la sucursal la cantidad de articulos del pedido
                            $storeHasArticle->amount += $oha->amount;
                        } else if ($status == OrdersController::DISPACHED_STATUS) {
                            //tiene que restar al stock de la sucursal la cantidad de articulos del pedido
                            $storeHasArticle->amount -= $oha->amount;
                        }
                        $this->StoresHasArticles->save($storeHasArticle);
                    }
                }

                $this->Flash->success(__('El estado del pedido: ' . $status_info[$status]));
            } else {
                $this->Flash->error(__('Error al cambiar el estado del pedido.'));
            }
            return $this->redirect(['controller' => 'orders', 'action' => 'index']);
        }
        $this->set(compact('data'));
        $this->set('_serialize', 'data');
    }

    public function history() 
    {
        //Ajax Detection
        if ($this->request->is('Ajax')) {
            $id = $_POST['id'];
            $status = [
                OrdersController::PENDING_STATUS => 'PENDIENTE',
                OrdersController::ASSIGNED_STATUS => 'ASIGNADO',
                OrdersController::DISPACHED_STATUS => 'DESPACHADO',
                OrdersController::COMPLETED_STATUS => 'COMPLETADO',
                OrdersController::CANCELED_STATUS => 'CANCELADO',
            ];
            $this->loadModel('OrdersStatus');
            $orders_status = $this->OrdersStatus->find()->contain(['Users'])->where(['order_id' => $id]);
            $data['id'] = $id;
            $data['orders_status'] = $orders_status;
            $data['status'] = $status;
        }
        $this->set(compact('data'));
        $this->set('_serialize', 'data');
    }
}
