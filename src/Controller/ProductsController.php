<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 */
class ProductsController extends AppController
{
    
    public function initialize()
    {
        parent::initialize();
        if ($this->request->session()->read('type_user') != 'admin' ) {
            if ($this->Auth) {
                $this->Auth->logout();
            }
        }
        $this->viewBuilder()->layout('admin');
    }

    public function isAuthorized($user = null) 
    {
        return parent::isAuthorized($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $products = $this->Products->find();

        $this->set(compact('products'));
        $this->set('_serialize', ['products']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $product = $this->Products->newEntity();
        if ($this->request->is('post')) {
            $product = $this->Products->patchEntity($product, $this->request->data);
            if ($this->Products->save($product)) {
                $this->Flash->success(__('El Producto se ha guardado correctamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Error al guardar el Producto. Por favor, intente nuevamente.'));
        }
        $categories = $this->Products->Categories->find('list', ['limit' => 200]);
        $seasons = $this->Products->Seasons->find('list', ['limit' => 200]);
        $this->set(compact('product', 'categories', 'seasons'));
        $this->set('_serialize', ['product']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null, $style_id_selected = null)
    {
        $product = $this->Products->get($id, [
            'contain' => ['Styles.Assets', 'Styles.Articles']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $product = $this->Products->patchEntity($product, $this->request->data);
            if ($this->Products->save($product)) {
                $this->Flash->success(__('Se ha modificado correctamente el Producto.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Error al modificar el Producto. Por favor, intente nuevamente.'));
            }
        }

        $this->loadModel('Categories');
        $this->loadModel('Seasons');
        $this->loadModel('Styles');

        $categories = $this->Categories->find('list', ['limit' => 200]);
        $seasons = $this->Seasons->find('list', ['limit' => 200]);
        $styles = $this->Styles->find()->order(['name' => 'ASC']); 

        if ($style_id_selected) {
            $product->style_selected = $style_id_selected;
        }

        $this->set(compact('product', 'categories', 'seasons', 'styles'));
        $this->set('_serialize', ['product']);
    }

    /**
     * Delete method
     *
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $id = $_POST['id'];
        $product = $this->Products->get($id);

        $this->__deleteStyles($id);

        if ($this->Products->delete($product)) {
            $this->Flash->success(__('El Producto se ha eliminado correctamente.'));
        } else {
            $this->Flash->error(__('Error al eliminar el Producto. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Product id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    private function __deleteStyles($product_id)
    {
        $this->loadModel('Styles');
        $styles = $this->Styles->find()->where(['product_id' => $product_id]);

        foreach ($styles as $style) {
            $this->__deleteAssets($style->id);
            $this->__deleteArticles($style->id);

            //unlink(WWW_ROOT . 'images/'. $style->img_url);
            $style->product_id = null;

            $this->Styles->save($style);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Asset id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    private function __deleteArticles($style_id)
    {
        $this->loadModel('Articles');
        $articles = $this->Articles->find()->where(['style_id' => $style_id]);

        foreach ($articles as $article) {
            $article->style_id = null;
            $article->size = null;
            $this->Articles->save($article);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Asset id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    private function __deleteAssets($style_id)
    {
        $this->loadModel('Assets');
        $assets = $this->Assets->find()->where(['style_id' => $style_id]);

        foreach ($assets as $asset) {
            $asset->style_id = null;
            $this->Assets->save($asset);
            // if (unlink(WWW_ROOT . 'images/' . $asset->url)) {
            //     $this->Assets->delete($asset);
            // }
        }
    }
}
