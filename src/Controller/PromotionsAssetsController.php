<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PromotionsAssets Controller
 *
 * @property \App\Model\Table\PromotionsAssetsTable $PromotionsAssets
 */
class PromotionsAssetsController extends AppController
{
    
    public function initialize()
    {
        parent::initialize();

        if ($this->request->session()->read('type_user') != 'admin' ) {
            if ($this->Auth) {
                $this->Auth->logout();
            }
        }
        $this->viewBuilder()->layout('admin');
    }

    public function isAuthorized($user = null) 
    {
        return parent::isAuthorized($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $promotionsAssets = $this->paginate($this->PromotionsAssets);

        $this->set(compact('promotionsAssets'));
        $this->set('_serialize', ['promotionsAssets']);
    }

    /**
     * View method
     *
     * @param string|null $id Promotions Asset id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $promotionsAsset = $this->PromotionsAssets->get($id, [
            'contain' => []
        ]);

        $this->set('promotionsAsset', $promotionsAsset);
        $this->set('_serialize', ['promotionsAsset']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {

            foreach ($this->request->data['files'] as $file) {
                
                if ($file['tmp_name']) {

                    $promotionAsset = $this->PromotionsAssets->newEntity();

                    $ext = explode('.', $file['name']);
                    $now = new \DateTime();
                    $newName = $now->getTimestamp() .'-' . $ext[0] . '.' .  end($ext);
                    $from = $file['tmp_name'];
                    $to = WWW_ROOT . 'images/'. $newName ;

                    if (move_uploaded_file($from, $to)) {

                        $this->request->data['url'] = $newName;
                    }
                }

                $promotionAsset = $this->PromotionsAssets->patchEntity($promotionAsset, $this->request->data);
                if ($this->PromotionsAssets->save($promotionAsset)) {
                    $this->Flash->success(__('Se han guardado las imagenes correctamente.'));
                } else {
                    $this->Flash->error(__('No se han guardado las imagenes. Por favor, intente nuevamente.'));
                }
            }
        }
        return $this->redirect(['controller' => 'promotions', 'action' => 'edit', $promotionAsset->promotion_id]);
    }

    /**
     * Edit method
     *
     * @param string|null $id Promotions Asset id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $promotionsAsset = $this->PromotionsAssets->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $promotionsAsset = $this->PromotionsAssets->patchEntity($promotionsAsset, $this->request->data);
            if ($this->PromotionsAssets->save($promotionsAsset)) {
                $this->Flash->success(__('The promotions asset has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The promotions asset could not be saved. Please, try again.'));
        }
        $this->set(compact('promotionsAsset'));
        $this->set('_serialize', ['promotionsAsset']);
    }

    /**
     * Delete method
     *
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $promotionAsset = $this->PromotionsAssets->get($_POST['id']);

        if (unlink(WWW_ROOT . 'images/'. $promotionAsset->url)) {

            if ($this->PromotionsAssets->delete($promotionAsset)) {
                $this->Flash->success(__('The asset has been deleted.'));
            } else {
                $this->Flash->error(__('The asset could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(['controller' => 'promotions', 'action' => 'edit', $promotionAsset->promotion_id]);
    }
}
