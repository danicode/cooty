<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * ShoppingCarts Controller
 */
class ShoppingCartsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->layout('ecommerce');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index', 'add', 'emptyCart', 'update', 'delete']);
    }

    public function add() 
    {
        //Ajax Detection
        if ($this->request->is('Ajax')) {
            $this->loadModel('Articles');

            $id = $_POST['id'];
            $amount = $_POST['amount'];

            $session = $this->request->session();
            $shopping_cart = $session->read('shopping_cart');

            if ($shopping_cart) {
                if (array_key_exists($id, $shopping_cart)) {
                    $shopping_cart[$id]->amount += $amount;
                } else {
                    $article = $this->Articles->get($id, [
                    'contain' => ['Styles.Assets']
                    ]);
                    $article->amount = intval($amount);
                    $shopping_cart[$id] = $article;
                }
            } else {
                $shopping_cart = array();
                $article = $this->Articles->get($id, [
                    'contain' => ['Styles.Assets']
                ]);
                $article->amount = intval($amount);
                $shopping_cart[$id] = $article;
            }

            $session->write('shopping_cart', $shopping_cart);
            $data['response'] = "Se guardo el carrito";
            $data['shopping_cart'] = $shopping_cart;
        } 

        else {
            $data['response'] = "Error: no se cargo al carrito";
        }

        $this->set(compact('data'));
        $this->set('_serialize', 'data');
    }

    public function emptyCart() 
    {
        //Ajax Detection
        if ($this->request->is('Ajax')) {

            $session = $this->request->session();
            $shopping_cart = $session->read('shopping_cart');
            $shopping_cart= array();

            $session->write('shopping_cart', $shopping_cart);
            $data['response'] = "Carrito vacio";
            $data['shopping_cart'] = $shopping_cart;
        } else {
            $data['response'] = "Error: al vaciar el carrito";
        }

        $this->set(compact('data'));
        $this->set('_serialize', 'data');
    }

    public function index() 
    {
        $this->viewBuilder()->layout('ecommerce');
        $this->loadModel('CoversPages');
        $this->loadModel('Seasons');
        $this->loadModel('Categories');
        $this->loadModel('Groups');
        $this->loadModel('Products');
        $this->loadModel('Styles');

        $groups = $this->Groups->find()->contain(['Categories', 'Categories.CategoriesAssets'])->limit(6);
        $news_products = $this->Products->find()->contain(['Styles.Assets', 'Styles.Articles', 'Categories.Groups'])->where(['news' => 1, 'visible' => 1, 'enable' => 1, 'Groups.name' => 'Accesorios']);
        $productx = array();
        foreach ($news_products as $product) {
            $flag = true;
            $stylex = array();
            $p = null;
            foreach ($product->styles as $style) {
                if (count($style->assets) > 0 && count($style->articles)) {
                    if ($flag) {
                        $s = $this->Styles->newEntity();
                        $p = $this->Products->newEntity();
                        $p->id = $product->id;
                        $p->name = $product->name;
                        $p->price = $product->price;
                        $p->enable = 1;
                        $p->visible = 1;
                        $p->news = 1;
                        $flag = false;
                    }
                    $s = $style;
                    array_push($stylex, $s);
                }
            }
            if ($p) {
                $p->styles = $stylex;
                array_push($productx, $p);
            }
        }
        $products = $news_products;

        $session = $this->request->session();
        $shopping_cart = $session->read('shopping_cart');
        $customer = $this->Auth->user();

        $coversPages = $this->CoversPages->find()->where(['visible' => 1]);;
        $seasons = $this->Seasons->find();

        $this->set(compact('coversPages', 'seasons', 'customer', 'groups', 'shopping_cart', 'news_products'));
        $this->set('_serialize', ['coversPages', 'seasons', 'customer', 'groups', 'shopping_cart', 'news_products']);
    }

    public function delete() 
    {
        //Ajax Detection
        if ($this->request->is('Ajax')) {
            $id = $_POST['id'];
            $session = $this->request->session();
            $shopping_cart = $session->read('shopping_cart');
            unset($shopping_cart[$id]);
            $session->write('shopping_cart', $shopping_cart);
            $data['shopping_cart'] = $shopping_cart;
        }

        $this->set(compact('data'));
        $this->set('_serialize', 'data');
    }

    public function update() 
    {
        //Ajax Detection
        if ($this->request->is('Ajax')) {
            $data['response'] = "Se guardo el carrito";

            $id = $this->request->input('json_decode')->id;
            $increment = $this->request->input('json_decode')->increment;
 
            $session = $this->request->session();
            $shopping_cart = $session->read('shopping_cart');

             if ($shopping_cart) {
                if (array_key_exists($id, $shopping_cart)) {
                    if ($increment) {
                        $shopping_cart[$id]->amount++;
                    } else {
                        $shopping_cart[$id]->amount--;
                    }
                }
            }

            $session->write('shopping_cart', $shopping_cart);
            $data['response'] = "Se guardo el carrito";
            $data['shopping_cart'] = $shopping_cart;
        } else {
            $data['response'] = "Error: no se cargo al carrito";
        }
        $this->set(compact('data'));
        $this->set('_serialize', 'data');
    }
}
