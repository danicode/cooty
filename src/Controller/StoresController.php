<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Stores Controller
 *
 * @property \App\Model\Table\StoresTable $Stores
 */
class StoresController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        if ($this->request->session()->read('type_user') != 'admin' ) {
            if ($this->Auth) {
                $this->Auth->logout();
            }
        }
        $this->viewBuilder()->layout('admin');
    }

    public function isAuthorized($user = null) 
    {
        return parent::isAuthorized($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $stores = $this->Stores->find();
        $user = $this->Auth->user();

        $this->set(compact('stores', 'user'));
        $this->set('_serialize', ['stores', 'user']);
    }

    /**
     * View method
     *
     * @param string|null $id Store id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $store = $this->Stores->get($id, [
            'contain' => ['StoresHasArticles.Articles']
        ]);

        $this->set('store', $store);
        $this->set('_serialize', ['store']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Store id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Auth->user();
        if ($user['admin']) {
             $store = $this->Stores->get($id, [
                'contain' => ['StoresAssets']
            ]);
            if ($this->request->is(['patch', 'post', 'put'])) {
    
                $store = $this->Stores->patchEntity($store, $this->request->data);
                if ($this->Stores->save($store)) {
                    $this->Flash->success(__('La sucursal se ha modificado correctametne.'));
    
                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('Error al modificar la sucursal. Por favor, intente nuevamente.'));
            }
        } else {
            $this->Flash->error(__('Error, no posee los privilegios para editar una Sucursal.'));
            return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
        }
        $this->set(compact('store'));
        $this->set('_serialize', ['store']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Store id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $store = $this->Stores->get($id);
        $store->enable = 0;
        if ($this->Stores->save($store)) {
            $this->Flash->success(__('Se ha deshabilitado correctamente la sucursal.'));
        } else {
            $this->Flash->error(__('Error al deshabilitar la sucursal. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function addImage()
    {
        $this->request->allowMethod(['post', 'delete']);
        $id = $_POST['store_id'];
        $store = $this->Stores->get($id);

        $file = $this->request->data['file'];

        if ($file) {
            $ext = explode('.', $file['name']);
            $now = new \DateTime();
            $newName = $now->getTimestamp() . '-' . $ext[0] . '.' .  end($ext);
            $from = $file['tmp_name'];
            $to = WWW_ROOT . 'images/'. $newName ;

            if (move_uploaded_file($from,$to)) {
                $store->url = $newName;
            }
        }

        if ($this->Stores->save($store)) {
            $this->Flash->success(__('Se ha añadido la imagen correctamente.'));
        } else {
            $this->Flash->error(__('Error al añadir la imagen. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['controller' => 'stores', 'action' => 'edit', $id]);
    }

    public function addGallery()
    {
        if ($this->request->is('post')) {

            foreach ($this->request->data['files'] as $file) {
                
                $this->loadModel('StoresAssets');

                if ($file['tmp_name']) {

                    $storesAssets = $this->StoresAssets->newEntity();

                    $ext = explode('.', $file['name']);
                    $now = Time::now();
                    $newName = $now->toUnixString() .'-' . $ext[0] . '.' .  end($ext);
                    $from = $file['tmp_name'];
                    $to = WWW_ROOT . 'images/'. $newName ;

                    if (move_uploaded_file($from,$to)) {

                        $this->request->data['url'] = $newName;
                    }
                }

                $storesAssets = $this->StoresAssets->patchEntity($storesAssets, $this->request->data);
                if (!$this->StoresAssets->save($storesAssets)) {
                     $this->Flash->error(__('Las imagnes no se han guardado. Por favor, intente nuevamente.'));
                }
            }

            $this->Flash->success(__('Las imagenes se han guardado correctamente.'));

            return $this->redirect(['controller' => 'stores', 'action' => 'edit', $storesAssets->store_id]);
        }
    }

    public function deleteImage($id = null)
    {

        $this->request->allowMethod(['post', 'delete']);
        $id = $_POST['id'];
        $store = $this->Stores->get($id);
        if (unlink(WWW_ROOT . 'images/'. $store->url)) {
            $store->url = null;

            if ($this->Stores->save($store)) {
                $this->Flash->success(__('Se ha eliminado correctamente la iamgen.'));
            } else {
                $this->Flash->error(__('Error al elimninar la imagen. Por favor, intente nuevamente.'));
            }
        } else {
            $this->Flash->error(__('Error al elimninar la imagen. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['controller' => 'stores', 'action' => 'edit', $id]);
    }
    
    public function deleteGallery()
    {
        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('StoresAssets');
        $storesAssets = $this->StoresAssets->get($_POST['id']);

        if (unlink(WWW_ROOT . 'images/'. $storesAssets->url)) {

            if ($this->StoresAssets->delete($storesAssets)) {
                $this->Flash->success(__('La imagen se ha eliminado correctamente.'));
            } else {
                $this->Flash->error(__('La imagen no se ha eliminado. Por favor, intente nuevamente.'));
            }
        }

        return $this->redirect(['controller' => 'stores', 'action' => 'edit', $storesAssets->store_id]);
    }
}
