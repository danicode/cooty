<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Styles Controller
 *
 * @property \App\Model\Table\StylesTable $Styles
 */
class StylesController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        if ($this->request->session()->read('type_user') != 'admin' ) {
            if ($this->Auth) {
                $this->Auth->logout();
            }
        }
        $this->viewBuilder()->layout('admin');
    }

    public function isAuthorized($user = null) 
    {
        return parent::isAuthorized($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Products']
        ];
        $styles = $this->paginate($this->Styles);

        $this->set(compact('styles'));
        $this->set('_serialize', ['styles']);
    }

    /**
     * View method
     *
     * @param string|null $id Style id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $style = $this->Styles->get($id, [
            'contain' => ['Products', 'Articles', 'Assets']
        ]);

        $this->set('style', $style);
        $this->set('_serialize', ['style']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $file = $this->request->data['file'];

            $ext = explode('.', $file['name']);
            $now = new \DateTime();
            $newName = $now->getTimestamp() .'-' . $ext[0] . '.' .  end($ext);
            $from = $file['tmp_name'];
            $to = WWW_ROOT . 'images/'. $newName ;

            if (move_uploaded_file($from,$to)) {
                $this->request->data['img_url'] = $newName;
            }

            $style = $this->Styles->newEntity();
            $style = $this->Styles->patchEntity($style, $this->request->data);

            if ($this->Styles->save($style)) {
                $this->Flash->success(__('El Estilo se ha guardado correctamente.'));
                return $this->redirect(['controller' => 'products', 'action' => 'edit', $style->product_id, $style->id]);
            }
            $this->Flash->error(__('El Estilo no se ha guardado. Por favor, intente nuevamente.'));
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Style id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $style = $this->Styles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $style = $this->Styles->patchEntity($style, $this->request->data);
            if ($this->Styles->save($style)) {
                $this->Flash->success(__('El Estilo se ha modificado correctamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El Estilo no se ha modificado. Por favor, intente nuevamente.'));
        }
        $products = $this->Styles->Products->find('list', ['limit' => 200]);
        $this->set(compact('style', 'products'));
        $this->set('_serialize', ['style']);
    }

    /**
     * Delete method
     *
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $style = $this->Styles->get($_POST['id']);

        $this->__deleteAssets($style->id);
        $this->__deleteArticles($style->id);

        if (unlink(WWW_ROOT . 'images/'. $style->img_url)) {
        }

        if ($this->Styles->delete($style)) {
            $this->Flash->success(__('El Estilo se ha eliminado correctamente.'));
        } else {
            $this->Flash->error(__('El Estilo no se ha eliminado. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['controller' => 'products', 'action' => 'edit', $_POST['product_id']]);
    }
    
    /**
     * Delete method
     *
     * @param string|null $id Asset id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    private function __deleteArticles($style_id)
    {
        $this->loadModel('Articles');
        $articles = $this->Articles->find()->where(['style_id' => $style_id]);

        foreach ($articles as $article) {
            $article->style_id = null;
            $article->size = null;
            $this->Articles->save($article);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Asset id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    private function __deleteAssets($style_id)
    {
        $this->loadModel('Assets');
        $assets = $this->Assets->find()->where(['style_id' => $style_id]);

        foreach ($assets as $asset) {
            if (unlink(WWW_ROOT . 'images/' . $asset->url)) {
                $this->Assets->delete($asset);
            }
        }
    }
}
