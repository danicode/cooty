<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Locations Controller
 *
 * @property \App\Model\Table\LocationsTable $Locations
 */
class SucursalesController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        if ($this->request->session()->read('type_user') != 'admin' ) {
            if ($this->Auth) {
                $this->Auth->logout();
            }
        }
        $this->viewBuilder()->layout('admin');
    }

    public function isAuthorized($user = null) 
    {
        return parent::isAuthorized($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $sucursales = $this->Sucursales->find();
        $user = $this->Auth->user();

        $this->set(compact('sucursales', 'user'));
        $this->set('_serialize', ['sucursales', 'user']);
    }

    /**
     * View method
     *
     * @param string|null $id Sucursal id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sucursal = $this->Sucursales->get($id, [
            'contain' => []
        ]);

        $this->set('sucursal', $sucursal);
        $this->set('_serialize', ['location']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Auth->user();
        if ($user['admin']) {
            $sucursal = $this->Sucursales->newEntity();
            if ($this->request->is('post')) {
                $sucursal = $this->Sucursales->patchEntity($sucursal, $this->request->data);
                if ($this->Sucursales->save($sucursal)) {
                    $this->Flash->success(__('Se ha guardado correctamente la Sucursal.'));
    
                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('No se ha podido guardar la Sucursal. Por favor, intente nuevamente.'));
            }
        } else {
            $this->Flash->error(__('Error, no posee los privilegios para agregar una Sucursal.'));
            return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
        }
        $this->set(compact('sucursal', 'user'));
        $this->set('_serialize', ['sucursal', 'user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sucursal id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Auth->user();
        $sucursal = $this->Sucursales->get($id, [
            'contain' => []
        ]);
        if ($user['admin']) {
            if ($this->request->is(['patch', 'post', 'put'])) {
                $sucursal = $this->Sucursales->patchEntity($sucursal, $this->request->data);
                if ($this->Sucursales->save($sucursal)) {
                    $this->Flash->success(__('La Sucursal se ha modificado correctamente.'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('No se ha podido modificar la Sucursal. Por favor, intente nuevamente.'));
            }
        } else {
            $this->Flash->error(__('Error, no posee los privilegios para editar una Sucursal.'));
            return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
        }

        $this->set(compact('sucursal'));
        $this->set('_serialize', ['sucursal']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Location id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Auth->user();
        if ($user['admin']) {
            $id = $this->request->data['id'];
            $sucursal = $this->Sucursales->get($id);
            $sucursal->enable = false;
            if ($this->Sucursales->save($sucursal)) {
                $this->Flash->success(__('Se ha eliminado correctamente la Sucursal.'));
            } else {
                $this->Flash->error(__('No se ha podido eliminar la Sucursal. Por favor, intente nuevamente.'));
            }
    
            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error(__('Error, no posee los privilegios para eliminar una Sucursal.'));
            return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
        }
    }

    public function addImage()
    {
        $this->request->allowMethod(['post', 'delete']);
        $id = $_POST['sucursal_id'];
        $sucursal = $this->Sucursales->get($id);

        $file = $this->request->data['file'];

        if ($file) {
            $ext = explode('.', $file['name']);
            $now = new \DateTime();
            $newName = $now->getTimestamp() . '-' . $ext[0] . '.' .  end($ext);
            $from = $file['tmp_name'];
            $to = WWW_ROOT . 'images/'. $newName ;

            if (move_uploaded_file($from,$to)) {
                $sucursal->url = $newName;
            }
        }

        if ($this->Sucursales->save($sucursal)) {
            $this->Flash->success(__('Se ha añadido la imagen correctamente.'));
        } else {
            $this->Flash->error(__('Error al añadir la imagen. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['controller' => 'sucursales', 'action' => 'edit', $id]);
    }

    public function deleteImage($id = null)
    {

        $this->request->allowMethod(['post', 'delete']);
        $id = $_POST['id'];
        $sucursal = $this->Sucursales->get($id);
        if (unlink(WWW_ROOT . 'images/'. $sucursal->url)) {
            $sucursal->url = null;

            if ($this->Sucursales->save($sucursal)) {
                $this->Flash->success(__('Se ha eliminado correctamente la iamgen.'));
            } else {
                $this->Flash->error(__('Error al elimninar la imagen. Por favor, intente nuevamente.'));
            }
        } else {
            $this->Flash->error(__('Error al elimninar la imagen. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['controller' => 'sucursales', 'action' => 'edit', $id]);
    }
}
