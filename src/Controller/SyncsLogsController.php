<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SyncsLogs Controller
 *
 * @property \App\Model\Table\SyncsLogsTable $SyncsLogs
 */
class SyncsLogsController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        if ($this->request->session()->read('type_user') != 'admin' ) {
            if ($this->Auth) {
                $this->Auth->logout();
            }
        }
        $this->viewBuilder()->layout('admin');
    }

    public function isAuthorized($user = null) 
    {
        return parent::isAuthorized($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Stores']
        ];
        $syncsLogs = $this->paginate($this->SyncsLogs);

        $this->set(compact('syncsLogs'));
        $this->set('_serialize', ['syncsLogs']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Syncs Log id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $syncsLog = $this->SyncsLogs->get($id);
        if ($this->SyncsLogs->delete($syncsLog)) {
            $this->Flash->success(__('The syncs log has been deleted.'));
        } else {
            $this->Flash->error(__('The syncs log could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
