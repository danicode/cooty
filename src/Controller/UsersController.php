<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->Auth->config('loginAction', [
            'controller' => 'Users',
            'action' => 'login'
        ]);

        if ($this->request->session()->read('type_user') != 'admin') {
            if ($this->Auth) {
                $this->Auth->logout();
            }
        }

        $this->viewBuilder()->layout('admin');

        // importando o nosso component para ficar acessível ao nosso controller.
        $this->loadComponent('StoreSync');
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->action == 'login') {
            return true;
        }

        if ($this->request->action == 'logout') {
            return true;
        }

        return parent::isAuthorized($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $user = $this->Auth->user();
        if ($user['admin']) {
            $users = $this->Users->find()->contain(['UsersHasStores', 'UsersHasStores.Stores']);
            $this->set(compact('users'));
            $this->set('_serialize', ['users']);
        } else {
            $this->Flash->error(__('El usuario no tiene permiso de acceso.'));
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {

            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->loadModel('Stores');
                $this->loadModel('UsersHasStores');
                $stores = $this->Stores->find()->where(['enable' => 1]);
                if ($user->admin) {
                    if ($stores->count() > 0) {
                        foreach ($stores as $store) {
                            $usersHasStores = $this->UsersHasStores->newEntity();
                            $usersHasStores->user_id = $user->id;
                            $usersHasStores->store_id = $store->id;
                            $this->UsersHasStores->save($usersHasStores);
                        }
                    }
                } else {
                    if ($user->stores) {
                        foreach ($user->stores as $store) {
                            $usersHasStores = $this->UsersHasStores->newEntity();
                            $usersHasStores->user_id = $user->id;
                            $usersHasStores->store_id = $store;
                            $this->UsersHasStores->save($usersHasStores);
                        }
                    }
                }
                $this->Flash->success(__('El usuario se ha registrado con éxito'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El usuario no pude registrarse. Por favor, intente nuevamente.'));
        }
        $this->loadModel('Stores');
        $stores = $this->Stores->find();
        $this->set(compact('user', 'stores'));
        $this->set('_serialize', ['user', 'stores']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['UsersHasStores']
        ]);
        $this->loadModel('Stores');
        $stores = $this->Stores->find()->where(['enable' => 1]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            $this->loadModel('UsersHasStores');
            $this->UsersHasStores->deleteAll(['user_id' => $user->id]);
            if ($user->admin) {
                if ($stores->count() > 0) {
                    foreach ($stores as $store) {
                        $usersHasStores = $this->UsersHasStores->newEntity();
                        $usersHasStores->user_id = $user->id;
                        $usersHasStores->store_id = $store->id;
                        $this->UsersHasStores->save($usersHasStores);
                    }
                }
            } else {
                if ($user->stores) {
                    foreach ($user->stores as $store) {
                        $usersHasStores = $this->UsersHasStores->newEntity();
                        $usersHasStores->user_id = $user->id;
                        $usersHasStores->store_id = $store;
                        $this->UsersHasStores->save($usersHasStores);
                    }
                }
            }

            if ($this->Users->save($user)) {
                $this->Flash->success(__('El usuario se ha modificado correctamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El usuario no pudo modificarse. Por favor, intente nuevamente.'));
        }
        $this->set(compact('user', 'stores'));
        $this->set('_serialize', ['user', 'stores']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $id = $_POST['id'];

        $this->request->allowMethod(['post', 'delete']);

        $user = $this->Users->get($id);
        $user->enable = false;
        //$this->loadModel('UsersHasStores');
        //$this->UsersHasStores->deleteAll(['user_id' => $user->id]);

        if ($this->Users->save($user)) {
            $this->Flash->success(__('El usuario ha sido eliminado.'));
        } else {
            $this->Flash->error(__('El usuario no ha sido eliminado. Por favor, intente nuevamente.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function login()
    {
        if ($this->request->session()->read('Auth.User')) {
            return $this->redirect(['controller' => 'Users', 'action' => 'index']); 
        }

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                if ($user['enable']) {
                    //$this->StoreSync->executeSync();
                    $this->Auth->setUser($user);

                    $this->request->session()->write('type_user', 'admin');
                    parent::activityRecord('Login usuario.', 'Login');
                    $this->Flash->default(__('Hola! ' . $this->Auth->user()['username']));
                    return $this->redirect(['controller' => 'Users', 'action' => 'dashboard']);
                } else {
                    $this->Flash->error(__('El usuario no está habilitado.'));
                }
            }
            else {
                $this->Flash->error(__('El usuario o la clave son incorrectas.'));
            }
        }
    }

    public function logout()
    {
        parent::activityRecord('Logout usuario.', 'Logout');
        $this->Auth->logout();
        $this->Flash->default(__('Cerro sesión correctamente.'));
        return $this->redirect(['controller' => 'Users','action' => 'login']);
    }

    public function dashboard() 
    {
        $user = $this->Auth->user();
        $session = $this->request->session();
        $this->loadModel('Orders');
        $this->loadModel('Stores');
        $this->loadModel('Articles');
        $this->loadModel('UsersHasStores');

        if ($user['admin'] && $session->read('type_user') == 'admin') {
            $orders = $this->Orders->find()->where(['status' => 0]);
            $stores = $this->Stores->find()->where(['enable' => 1]);
            $users = $this->Users->find()->where(['enable' => 1]);
            $articles = $this->Articles->find()->where(['style_id IS' => null]);
        } else if ($user['enable'] && $session->read('type_user') == 'admin') {
            $orders = $this->UsersHasStores->find()->where(['user_id' => $user['id']]);
            $orders_assigned = $orders;
            $orders_assigned->matching('Stores.Orders', function ($q) {
                return $q->where(['status' => 1]);
            });
            $orders_dispatched = $orders;
            $orders_dispatched->matching('Stores.Orders', function ($q) {
                return $q->where(['status' => 2]);
            });
            $orders_completed = $orders;
            $orders_completed->matching('Stores.Orders', function ($q) {
                return $q->where(['status' => 3]);
            });
            $orders_cancelled = $orders;
            $orders_cancelled->matching('Stores.Orders', function ($q) {
                return $q->where(['status' => 4]);
            });
        }

        $this->set(compact('user', 'orders', 'users', 'stores', 'articles', 'orders_assigned', 'orders_dispatched', 'orders_completed', 'orders_cancelled'));
        $this->set('_serialize', ['user', 'orders', 'users', 'stores', 'articles', 'orders_assigned', 'orders_dispatched', 'orders_completed', 'orders_cancelled']);
    }
}
