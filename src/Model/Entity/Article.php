<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Article Entity
 *
 * @property int $id
 * @property string $full_code
 * @property string $code_1
 * @property string $code_2
 * @property string $code_3
 * @property string $code_4
 * @property string $full_desc
 * @property string $desc_1
 * @property string $desc_2
 * @property string $desc_3
 * @property string $desc_4
 * @property \Cake\I18n\Time $created
 * @property bool $enable
 * @property \Cake\I18n\Time $modified
 * @property float $price
 * @property int $amount
 * @property int $last_sync
 * @property int $store_id
 *
 * @property \App\Model\Entity\Store $store
 * @property \App\Model\Entity\Asset[] $assets
 * @property \App\Model\Entity\Favorite[] $favorites
 * @property \App\Model\Entity\OrdersHasArticle[] $orders_has_articles
 */
class Article extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
