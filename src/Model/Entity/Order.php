<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Order Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property bool $status
 * @property float $total
 * @property int $customer_id
 * @property int $delivery_id
 * @property int $store_id
 * @property string $email
 * @property string $address
 * @property string $phone
 *
 * @property \App\Model\Entity\Customer $customer
 * @property \App\Model\Entity\Delivery $delivery
 * @property \App\Model\Entity\Store $store
 * @property \App\Model\Entity\OrdersHasArticle[] $orders_has_articles
 * @property \App\Model\Entity\OrdersStatus[] $orders_status
 */
class Order extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
