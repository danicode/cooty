<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Store Entity
 *
 * @property int $id
 * @property string $name
 * @property int $number
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property string $address
 * @property bool $enable
 *
 * @property \App\Model\Entity\Article[] $articles
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\SyncsLog[] $syncs_logs
 * @property \App\Model\Entity\UsersHasStore[] $users_has_stores
 */
class Store extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
