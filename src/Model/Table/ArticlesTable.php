<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Articles Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Stores
 * @property \Cake\ORM\Association\HasMany $Assets
 * @property \Cake\ORM\Association\HasMany $Favorites
 * @property \Cake\ORM\Association\HasMany $OrdersHasArticles
 *
 * @method \App\Model\Entity\Article get($primaryKey, $options = [])
 * @method \App\Model\Entity\Article newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Article[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Article|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Article patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Article[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Article findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ArticlesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('articles');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Favorites', [
            'foreignKey' => 'article_id'
        ]);
        $this->hasMany('OrdersHasArticles', [
            'foreignKey' => 'article_id'
        ]);
        $this->hasMany('StoresHasArticles', [
            'foreignKey' => 'article_id',
            'joinType' => 'left',
        ]);
        $this->belongsToMany('Stores', [
            'targetForeignKey' => 'store_id',
            'foreignKey' => 'article_id',
            'joinTable' => 'stores_has_articles',
        ]);
        $this->belongsTo('Styles', [
            'foreignKey' => 'style_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('code', 'create')
            ->notEmpty('code', 'Este campo no puede estar vacio')
            ->add('code', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => 'El código ya existe']);

        $validator
            ->requirePresence('description', 'create')
            ->notEmpty('description', 'Este campo no puede estar vacio');

        $validator
            ->boolean('enable')
            ->requirePresence('enable', 'create')
            ->notEmpty('enable', 'Este campo no puede estar vacio');

        $validator
            ->decimal('price')
            ->requirePresence('price', 'create')
            ->notEmpty('price', 'Este campo no puede estar vacio');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['code'], 'El código ya existe.'));

        return $rules;
    }
}
