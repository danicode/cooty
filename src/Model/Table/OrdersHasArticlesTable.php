<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrdersHasArticles Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Orders
 * @property \Cake\ORM\Association\BelongsTo $Articles
 *
 * @method \App\Model\Entity\OrdersHasArticle get($primaryKey, $options = [])
 * @method \App\Model\Entity\OrdersHasArticle newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OrdersHasArticle[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OrdersHasArticle|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrdersHasArticle patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OrdersHasArticle[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OrdersHasArticle findOrCreate($search, callable $callback = null, $options = [])
 */
class OrdersHasArticlesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('orders_has_articles');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Articles', [
            'foreignKey' => 'article_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('amount')
            ->requirePresence('amount', 'create')
            ->notEmpty('amount', 'Este campo no puede estar vacio');

        $validator
            ->requirePresence('code', 'create')
            ->notEmpty('code', 'Este campo no puede estar vacio');

        $validator
            ->requirePresence('name_display', 'create')
            ->notEmpty('name_display', 'Este campo no puede estar vacio');

        $validator
            ->decimal('price')
            ->requirePresence('price', 'create')
            ->notEmpty('price', 'Este campo no puede estar vacio');

        $validator
            ->requirePresence('url', 'create')
            ->notEmpty('url', 'Este campo no puede estar vacio');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['order_id'], 'Orders'));
        $rules->add($rules->existsIn(['article_id'], 'Articles'));

        return $rules;
    }
}
