<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Stores Model
 *
 * @property \Cake\ORM\Association\HasMany $Articles
 * @property \Cake\ORM\Association\HasMany $Orders
 * @property \Cake\ORM\Association\HasMany $SyncsLogs
 * @property \Cake\ORM\Association\HasMany $UsersHasStores
 *
 * @method \App\Model\Entity\Store get($primaryKey, $options = [])
 * @method \App\Model\Entity\Store newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Store[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Store|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Store patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Store[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Store findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class StoresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('stores');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('StoresHasArticles', [
            'foreignKey' => 'store_id'
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'store_id'
        ]);
        $this->hasMany('SyncsLogs', [
            'foreignKey' => 'store_id'
        ]);
        $this->hasMany('UsersHasStores', [
            'foreignKey' => 'store_id'
        ]);
        $this->belongsToMany('Articles', [
            'targetForeignKey' => 'article_id',
            'foreignKey' => 'store_id',
            'joinTable' => 'stores_has_articles',
        ]);
        $this->hasMany('StoresAssets', [
            'foreignKey' => 'store_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => 'El nombre de sucursal ya existe.']);

        $validator
            ->integer('number')
            ->allowEmpty('number');

        $validator
            ->allowEmpty('address');
            
        $validator
            ->allowEmpty('phone');
            
        $validator
            ->allowEmpty('url');

        $validator
            ->boolean('enable')
            ->requirePresence('enable', 'create')
            ->notEmpty('enable', 'Este campo no puede estar vacio');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['name'], 'El nombre de sucursal ya existe.'));

        return $rules;
    }
}
