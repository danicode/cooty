<div class="row">
     <div class="col-md-5">
        <div class="pull-right">
             <a class="btn btn-default" title="Ir a la lista de Articulos." href="<?=$this->Url->build(["controller" => "articles", "action" => "index"])?>" role="button">
                <span class="glyphicon icon-undo2" aria-hidden="true"></span>
            </a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-5">
        <legend><?= __('Articulo') ?></legend>
        <?= $this->Form->create($article) ?>
        <fieldset>
            <?php
                echo $this->Form->input('code', ['label' => __('Código'), 'readonly' => 'readonly']);
                echo $this->Form->input('description', ['label' => __('Descripción'), 'readonly' => 'readonly']);
                echo $this->Form->input('name_display', ['label' => __('Nombre a Mostrar')]);
            ?>
        </fieldset>
        <?= $this->Form->button(__('Guardar')) ?>
        <?= $this->Form->end() ?>
    </div>
    
    <div class="col-md-5">
        <div class="row">
            <div class="col-md-11">
                <legend><?= __('Imagen de Talles') ?></legend>
            </div>
            <div class="col-md-1">
                <div class="pull-right">
                    <a class="btn btn-default" title="<?= __('Imagen de Talles') ?>" href="#" role="button" id="btn-add-asset" >
                        <span class="glyphicon icon-plus" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div id="main-image" class="row">
                    <?php if ($article->size_image): ?>
                        <div class='col-md-4'>
                            <img style="max-width: 100%; height: auto;" src="/app/images/<?= $article->size_image ?>"></img>
                            <a class='btn-delete-asset' data-id="<?= $article->id ?>" href='javascript:void(0)'><?= __('Quitar') ?></a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="styles-popup" class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?= __('Añadir Imagen de Talles') ?></h4>
      </div>
      <div class="modal-body">
        <?= $this->Form->create(null, ['url' => ['controller' => 'articles' , 'action' => 'addSizeImage'], 'enctype' => "multipart/form-data" ]) ?>
            <fieldset>
                <?php
                    echo $this->Form->input('article_id', ['type' => 'hidden', 'value' => $article->id]);
                    echo $this->Form->file('file', ['required' => true, 'type'=>'file']);
                ?>
            </fieldset>
            <br>

            <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Cancelar') ?></button>
            <?= $this->Form->button(__('Aceptar')) ?>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>

<script>

    var article = <?= json_encode($article); ?>;

    $('a#btn-add-asset').click(function() {
        $('#styles-popup.modal-actions').modal('show');
    });

	$('a#btn-add-asset').click(function() {
        $('#styles-popup.modal-actions').modal('show');
    });

    $(document).on("click", "a.btn-delete-asset", function(e) {

        var id  = $(this).data('id');
        var text = 'Esta Seguro que desea eliminar la Imagen de Talles ? ';
        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/app/articles/deleteSizeImage/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id})))
                .find('#replacer').submit();
            }
        });
    });
</script>
