<style type="text/css">

    #table-articles {
        width: 100% !important;
    }

    #table-articles td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 5px 0px 5px !important;
        vertical-align: middle;
    }

    .modal-actions a.btn {
        width: 100%;
        margin-bottom: 5px;
    }

     tr.selected {
        background-color: #8eea99;
        color: #333 !important;
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }

</style>

<div id="btns-tools">
    <div class="pull-right btns-tools margin-bottom-5">
        <button type="button" class="btn btn-default btn-export" title="Exportar en formato excel el contenido de la tabla" >
            <span class="glyphicon icon-file-excel"  aria-hidden="true" ></span>
        </button>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-articles" >
        <thead>
            <tr>
                <th><?= __('#') ?></th>
                <th><?= __('Código') ?></th>
                <th><?= __('Descripción') ?></th>
                <th><?= __('Nombre a Mostrar') ?></th>
                <th><?= __('Imagen de Talles') ?></th>
                <th><?= __('Creado') ?></th>
                <th><?= __('Habilitado') ?></th>
                <th><?= __('Modificado') ?></th>
                <th><?= __('Precio') ?></th>
                <th><?= __('Cantidad') ?></th>
                <th><?= __('última sincro') ?></th>
                <th><?= __('Sucursal') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 1 ?>
            <?php foreach ($articles as $article): ?>
                <?php foreach ($article->stores_has_articles as $sha): ?>
                    <tr data-id="<?= $article->id ?>">
                        <td><?= $i ?></td>
                        <td><?= h($article->code) ?></td>
                        <td><?= h($article->description) ?></td>
                        <td><?= h($article->name_display) ?></td>
                        <?php if ($article->size_image): ?>
                            <td><img style="width: 113px; height: auto; position: relative;display: inline-block;" src="/app/images/<?= $article->size_image ?>" alt="" class="img-responsive"/></td>
                        <?php else: ?>
                            <td>Sin Imagen disponible</td>
                        <?php endif; ?>
                        <td><?= date('d/m/Y H:i', strtotime($article->created->format('Y-m-d H:i:s'))) ?></td>
                        <td><?= h($article->enable) ?></td>
                        <td><?= date('d/m/Y H:i', strtotime($article->modified->format('Y-m-d H:i:s'))) ?></td>
                        <td>Gs.<?= $article->price ?></td>
                        <td><?= $sha->amount ?></td>
                        <td><?= $sha->last_sync ?></td>
                        <td><?= $sha->has('store') ? $this->Html->link($sha->store->name, ['controller' => 'Stores', 'action' => 'view', $sha->store->id]) : '' ?></td>
                    </tr>
                    <?php $i++ ?>
                <?php endforeach; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?= __('Acciones') ?></h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a class="btn btn-default" id="btn-edit" href="#" role="button">
                        <span class="glyphicon glyphicon-edit"  aria-hidden="true"></span>
                        <?= __('Editar') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>
                </center>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

   var table_articles = null;

   $(document).ready(function () {

        $('#table-articles').removeClass('display');

		table_articles = $('#table-articles').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "columnDefs": [
                {
                    "targets": [ 5 ],
                    "visible": false,
                    "searchable": true
                },
                {
                    "targets": [ 6 ],
                    "visible": false,
                    "searchable": true
                },
                {
                    "targets": [ 10 ],
                    "visible": false,
                    "searchable": true
                },
                {
                    "targets": [ 11 ],
                    "visible": false,
                    "searchable": true
                }
            ],
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cangando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguinte",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "Todas"]],
            /*"dom": '<"toolbar">frtip'*/
		});

		$('#table-articles_filter').closest('div').before($('#btns-tools').contents());

    });

    $('#table-articles tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_articles.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            $('.modal-actions').modal('show');
        }
    });

    $('a#btn-edit').click(function(){
       var action = '/app/articles/edit-name-display/' + table_articles.$('tr.selected').data('id');
       window.open(action, '_self');
    });

    $(".btn-export").click(function() {
        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-articles').tableExport({tableName: 'Articles', type:'excel', escape:'false'});
            }
        });
    });

</script>
