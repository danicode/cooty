<style type="text/css">

	.add_wishlist img {
	    width: 18px;
	    height: 18px;
	    opacity: .7;
	    cursor: pointer;
	}

	.add_wishlist img:hover {
	    opacity: 1;
	}

	.add_wishlist {
	    display: block;
	    position: relative;
		margin-left: 23px;
		margin-bottom: 15px;
	}

	img .iWish {
	    border: 0;
	    width: 100%;
	    vertical-align: middle;
	}

	.lbl-add-favorite {
		color: #8c8c8c;
		cursor: pointer;
		font-size: 14px;
	}

	.item {
		max-height: 400px;
	}

	.simpleCart_shelfItem p span {
	    text-decoration: none;
	}

</style>

<!-- breadcrumbs -->
	<div class="breadcrumb_dress">
		<div class="container">
			<ul>
				<li><a href="/app/home/index"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> <?= __('Inicio') ?></a> <i>/</i></li>
				<li><?= __('Catalogo') ?></li>
			</ul>
		</div>
	</div>
<!-- //breadcrumbs -->

<div class="container">
	<h2 class="text-center"><?= __('Busqueda: ') ?><span style="font-size: 18px;"><span><?= __($search) ?></span></span> <i style="font-size: 20px;" class="glyphicon glyphicon-search"></i></h2>
</div>

<!-- dresses -->
	<div class="dresses">
		<div class="container">
			<div class="w3ls_dresses_grids">
				<div class="col-md-4 w3ls_dresses_grid_left">
					<div class="w3ls_dresses_grid_left_grid">
						<h3><?= __('Categorias') ?></h3>
						<div class="w3ls_dresses_grid_left_grid_sub">
							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						      <?php $collapse = false; ?>
							  <?php foreach ($groups as $group): ?>
							      <?php if (isset($category_selected)): ?>
							      	<?php $collapse = ($category_selected->group->id == $group->id);  ?>
							      <?php endif; ?>
								  <div class="panel panel-default">
								  	<div class="panel-heading" role="tab" id="heading<?= $group->id ?>">
							  		  <h4 class="panel-title asd">
										<a class="pa_italic <?= $collapse ? '' : 'collapsed' ?>" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$group->id?>" aria-expanded="<?= $collapse ? 'true' : 'false' ?>" aria-controls="collapse<?= $group->id ?>">
										  <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><i class="glyphicon glyphicon-minus" aria-hidden="true"></i><?= __($group->name) ?>
										</a><span style="cursor: pointer;" class="group-name glyphicon glyphicon-chevron-right" data-id="<?= $group->id ?>"></span>
									  </h4>
							  		</div>
							  		<div id="collapse<?= $group->id ?>" class="panel-collapse collapse<?= ($collapse) ? ' in' : '' ?>" role="tabpanel" aria-labelledby="heading<?=$group->id?>">
						  				<div class="panel-body panel_text">
											<ul>
												<?php $category_class_selected = ''; ?>
												<?php if (isset($category_selected)): ?>
													<?php $category_class_selected = 'category-selected'; ?>
												<?php endif; ?>
												<?php foreach ($group->categories as $category): ?>
													<?php $category_class_selected_id = ''; ?>
													<?php if (isset($category_selected)): ?>
														<?php $category_class_selected_id = ($category_selected->id == $category->id) ? $category_class_selected : ''; ?>
													<?php endif; ?>
													<li class="<?= $category_class_selected_id ?>"><?= $this->Html->link(__($category->name), ['controller' => 'catalogo', 'action' => 'index', $category->id]) ?></li>
												<?php endforeach; ?>
											</ul>
										</div>
						  			</div>
								  </div>
								  <?php $collapse = false; ?>
							  <?php endforeach; ?>
						    </div>
						</div>
					</div>
				</div>
				<div class="col-md-8 w3ls_dresses_grid_right">

					<div style="display:none;" class="w3ls_dresses_grid_right_grid2">
						<div class="w3ls_dresses_grid_right_grid2_right">
							<select id="most-relevant" name="select_item" class="select_item">
								<option <?= ($order) ? "" : "selected" ?>><?= __('Ordenar') ?></option>
								<option <?= ($order == 'ASC') ? "selected" : "" ?>><?= __('Menor precio') ?></option>
								<option <?= ($order == 'DESC') ? "selected" : "" ?>><?= __('Mayor precio') ?></option>
							</select>
						</div>
						<div class="clearfix"> </div>
					</div>

					<?php $i = 0; ?>
					<?php foreach ($products as $product): ?>
						<div class="w3ls_dresses_grid_right_grid3">
							<div style="margin-bottom: 30px;" class="col-md-4 agileinfo_new_products_grid agileinfo_new_products_grid_dresses">
								<div class="agile_ecommerce_tab_left dresses_grid">
									<div class="hs-wrapper hs-wrapper2">
										<?php $display = "block"; ?>
										<?php foreach ($product->styles as $style): ?>
										 	<div class="section-images-styles" data-product-id="<?= $product->id; ?>" data-id="<?= $style->id; ?>" style="display: <?= $display; ?>">
												<?php $images = array(); $j = 0; $img = ''?>
												<?php foreach ($style->assets as $asset): ?>
													<?php if ($j == 0): ?>
													<?php $img = $asset->url; ?>
													<?php endif; $j++; ?>
												    <!-- imagen debe tener un width: 300 px - height: 400 px -->
													<?php array_push($images, '/app/images/' . $asset->url); ?>
												<?php endforeach; ?>
												<div class="images-rotation" data-images='["<?= implode("\", \"", $images); ?>"]'>
											    	<img style="cursor: pointer;" data-tytpe='comun' data-id="<?= $product->id ?>" src="/app/images/<?= $img ?>" alt="" class="btn-view-product img-responsive">
											  	</div>
												<?php $display = "none"; ?>
											</div>
										<?php endforeach; ?>
									</div>
									<h5><a href="javascript:void(0)"><?= __($product->name) ?></a></h5>
								    <div class="simpleCart_shelfItem">
										<p><i class="item_price">Gs.<?= $product->price ?></i></p>
									</div>
									<div class="row">
									  <?php $product_selected = 'product-selected' ?>
							          <?php foreach ($product->styles as $style): ?>
								          <div class="col-sm-2">
						            			<a class='img-stylex <?= $product_selected ?>' data-product-id="<?= $product->id ?>" data-id="<?= $style->id ?>" href="javascript:void(0)"><img src="/app/images/<?= $style->img_url ?>" class'img-responsive' style='border-radius: 30px; height: 30px; width: 30px;' alt="<?= $style->name ?>"></a>
								          </div>
								          <?php $product_selected = '' ?>
							          <?php endforeach; ?>
						       		</div>
								</div>
							</div>
							<?php $i++; ?>
							<?php if ($i == 3): ?>
								<?php echo '<div class="clearfix"></div>' ?>
								<?php $i = 0; ?>
							<?php endif; ?>
						</div>
					<?php endforeach; ?>

				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<div class="w3l_related_products">
		<div class="container">
			<h3 class="prod"><?= __('Productos Nuevos') ?></h3>
			<ul id="flexiselDemo2">
				<?php foreach ($news_products as $product): ?>
				<li>
					<div class="w3l_related_products_grid">
						<div class="agile_ecommerce_tab_left dresses_grid">
							<div class="hs-wrapper hs-wrapper3">
								<?php $display = "block"; ?>
								<?php foreach ($product->styles as $style): ?>
								 	<div class="section-images-stylesx" data-product-id="<?= $product->id; ?>" data-id="<?= $style->id; ?>" style="display: <?= $display; ?>">
										<?php $images = array(); $i = 0; $img = ''?>
										<?php foreach ($style->assets as $asset): ?>
											<?php if ($i == 0): ?>
											<?php $img = $asset->url; ?>
											<?php endif; $i++; ?>
										    <!-- imagen debe tener un width: 300 px - height: 400 px -->
											<?php array_push($images, '/app/images/' . $asset->url); ?>
										<?php endforeach; ?>
										<div class="images-rotation" data-images='["<?= implode("\", \"", $images); ?>"]'>
									    	<img style="cursor: pointer;" data-type='news' data-id="<?= $product->id ?>" src="/app/images/<?= $img ?>" alt="" class="btn-view-productx img-responsive">
									  	</div>
										<?php $display = "none"; ?>
									</div>
								<?php endforeach; ?>
							</div>
							<h5><a href="javascript:void(0)"><?= __($product->name) ?></a></h5>
							<div class="simpleCart_shelfItem">
								<p><i class="item_price">Gs.<?= $product->price ?></i></p>
							</div>
							<div class="row">
							  <?php $product_selected = 'product-selected' ?>
					          <?php foreach ($product->styles as $style): ?>
						          <div class="col-sm-2">
				            			<a class='img-stylex <?= $product_selected ?>' data-product-id="<?= $product->id ?>" data-id="<?= $style->id ?>" href="javascript:void(0)"><img src="/app/images/<?= $style->img_url ?>" class'img-responsive' style='border-radius: 30px; height: 30px; width: 30px;' alt="<?= $style->name ?>"></a>
						          </div>
						          <?php $product_selected = '' ?>
					          <?php endforeach; ?>
				       		</div>
						</div>
					</div>
				</li>
				<?php endforeach; ?>
			</ul>
			<script type="text/javascript">
				$(window).load(function() {
					$("#flexiselDemo2").flexisel({
						visibleItems:4,
						animationSpeed: 1000,
						autoPlay: true,
						autoPlaySpeed: 3000,    		
						pauseOnHover: true,
						enableResponsiveBreakpoints: true,
						responsiveBreakpoints: { 
							portrait: { 
								changePoint:480,
								visibleItems: 1
							}, 
							landscape: { 
								changePoint:640,
								visibleItems:2
							},
							tablet: { 
								changePoint:768,
								visibleItems: 3
							}
						}
					});

					$('.images-rotation').imagesRotation();

					$('.btn-view-productx').click(function() {
						var id = $(this).data('id');
						$.each (news_products, function(i, product) {
							if (id == product.id) {
								product_selected = product;
							}
						});

						var style_id;

						$(".section-images-stylesx[data-product-id='" + product_selected.id + "']").each(function(index, value) {
						    if ($(this).css("display") == "block") {
								style_id = $(this).data('id');
						    }
						});

						$.each (product_selected.styles, function(i, style) {
							if (style_id == style.id) {
								style_selected = style;
							}
						});

						stylesLoad();
						styleChanged(style_selected);
						
						$('#custom-description').html("");
						if (product_selected.custom_description) {
							$('#custom-description').append("<legend style='font-family: Glegoo, serif;font-size: 18px;'>Descripción</legend>");
							$('#custom-description').append(product_selected.custom_description);
						}

						//agrega el id del articulo al boton de favoritos
						$('.btn-favorite').data('id', article_selected.id);
						$('.btn-favorite img').attr('src', '/app/img/icon_wishlist.png');
						if (customer) {
							if (customer.hasOwnProperty("favorites")) {
								$.each (customer.favorites, function(i, favorite) {
									if (favorite.article.id == article_selected.id) {
										$('.btn-favorite img').attr('src', '/app/img/icon_wishlist_added.png');
									}
								});
							}
						}

						$('#article_amount').text("1");
						$('#quick-view-popup').modal('show');
					});

					//seleccion del estilo
					$('div').on("click", "a.img-stylex", function() {

						var style_id = $(this).data('id');
						var product_id = $(this).data('product-id');

						$(".section-images-stylesx[data-product-id='" + product_id +"']").each(function(index, value) {
						    var id = $(this).data('id');

						    if (id == style_id) {
						    	$("a.img-stylex[data-id='" + id +"']").addClass('product-selected');
								$(this).css('display', '');
						    } else {
						    	$("a.img-stylex[data-id='" + id +"']").removeClass('product-selected');
								$(this).css('display', 'none');
						    }
						});
					});
				});
			</script>
			<script type="text/javascript" src="/app/js/ecommerce/jquery.flexisel.js"></script>
		</div>
	</div>
<!-- //dresses -->

<!-- quick-view-popup -->
<div data-backdrop="true" data-keyboard="true" id="quick-view-popup" class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="dialog">
        <div class="modal-content">
            <div class="modal-body">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <br>
                  <br>
                  <div class="row">
                      <div class="section-assets-options col-md-1">
                      </div>
                      <div class="section-main-assets col-md-5">
                      </div>
                      <div class="simpleCart_shelfItem col-md-6">
                      	  <h4 style="font-size: 20px; color: #222; font-weight: bold; font-family: Arial,Helvetica,sans-serif; text-transform:capitalize;" id='lbl-title' id="gridSystemModalLabel"></h4>
                          <p>Gs.<i id="lbl-price" class="item_price"></i></p>

                          <h4><?= __('Color') ?></h4>
                          <div class="row">
                              <div class="section-styles">
                              </div>
                          </div>
                          <div style="position: relative;display: inline;">
                          	<h4 style="position: relative;display: inline;"><?= __('Talle') ?></h4>  <span data-id="" class="size-guide" style="position: relative;display: inline;border-bottom: 1px solid #ded5d5;cursor: pointer;color: #908d8d;"><?= __('Guia de Talles') ?></span>
                          </div>
                          <div class="row">
                              <div class="section-size col-md-12">
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-5">
                                  <h4><?= __('Cantidad') ?></h4>
                                  <div class="quantity"> 
										<div class="quantity-select">                           
											<div class="entry value-minus">&nbsp;</div>
											<div id ="article_amount" class="item_Quantity entry value"><span>1</span></div>
											<div class="entry value-plus active">&nbsp;</div>
										</div>
									</div>
                              </div>
                          	  <div style="margin-top: 20px" class="col-md-12">
								<p><a id="btn-add-to-cart" class="item_add" href="javascript:void(0)"><?= __('Agregar al carrito') ?></a></p>
							  </div>
							  <div data-id="" class="btn-favorite add_wishlist pull-left">
								<img class="iWish" src="/app/img/icon_wishlist.png"> <lable class="lbl-add-favorite"><?= __('Añadir lista de favoritos') ?></label>
							  </div>
							  <div style="display: block;padding: 15px;top: 20px;" id="custom-description">
							  </div>
                          </div>
                      </div>
                  </div>
            </div>
        </div>
    </div>
</div>

<div id="size-image-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 style="padding-left: 12px;" class="modal-title">Guia de Talles</h4>
      </div>
      <div class="modal-body">
  		<img id="img-size-image" style="width:100%; height: auto;" src="" class="img-responsive" alt="">
  		<label id="lbl-size-image">Sin Imagen de Talles disponible</label>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>

<!-- //quick-view-popup -->
<script type="text/javascript">

	var products;
	var news_products;
	var shopping_cart;
	var product_selected;
	var style_selected;
	var main_asset;
	var article_selected;
	var customer;
	var category_selected;
	var search;

	//carga la imagen de la izquierda por defecto el primer Style
	var assetsOptionsLoad = function(style) {
	$('.section-assets-options').html('');
		$.each (style.assets, function(i, asset) {
			var img = "<a class='asset-option' data-id='"+asset.id+"' href='javascript:void(0)'><img src='/app/images/" + asset.url + "' class'img-responsive' style='width: 50px;' alt='Image'></a>";
			$('.section-assets-options').append(img).append('<br>').append('<br>');
		});
	}

	//cargar imagen principal
	var mainAssetLoad = function(asset) {
	$('.section-main-assets').html('');
		var img_main_asset = "<img style='max-width:100%; height: auto; display: block;' src='/app/images/" + asset.url + "' class='img-responsive' alt='Image'>"
		$('.section-main-assets').append(img_main_asset);
	}

	//carga de precio
	var articlePriceLoad = function(article) {
		$('#lbl-price').text(article.price);
	}

	//carga de estilos
	var stylesLoad = function() {
		$('.section-styles').html('');
		var id = style_selected.id;
		$.each (product_selected.styles, function (i, style) {
			var sel = (id == style.id) ? 'product-selected' : ''
			var img = "<div class='col-sm-1'><a class='" + sel + " img-style' data-id='" + style.id + "' href='javascript:void(0)'><img src='/app/images/" + style.img_url + "' class'img-responsive' style='border-radius: 30px; height: 30px; width: 30px;' alt='Image'></a></div>";
			$('.section-styles').append(img);
		});
	}

	//carga de talles
	var articleSizeLoad = function(style) {
		$('.section-size').html('');

		$.each (style.articles, function(i, article) {
			var btnSelection = 'btn-sizes';
			if (i == 0) {
				btnSelection = 'btn-sizes-selected';
				var article_name = (article_selected.name_display) ? article_selected.name_display : article_selected.description;
				$('#lbl-title').text(article_name);
			}
			var size = "<a data-id='" + article.id + "' style='margin-right: 4px;' type='button' class='btn-size " + btnSelection + "'>" + article.size + "</a>";
			$('.section-size').append(size);
		});
	}

	//funcion para cambiar el estilo
	var styleChanged = function (style) {
		assetsOptionsLoad(style);

		main_asset  = style.assets[0];
		mainAssetLoad(main_asset);

		article_selected = style.articles[0];
		articlePriceLoad(article_selected);

		articleSizeLoad(style);
	}

	//cambiar el talle y seleccionar articulo
	var sizeChanged = function(id) {
		$.each (style_selected.articles, function (i, article) {
			if (article.id == id) {
				article_selected = article;
				var article_name = (article_selected.name_display) ? article_selected.name_display : article_selected.description;
				$('#lbl-title').text(article_name);
			}
		});
	}

	$('.btn-favorite').click(function() {
		var img = $(this).find('img');
		var id = $(this).data('id');
		addToFavorites(id, img);
	});

	$(document).ready(function() {
		$('.images-rotation').imagesRotation();
		products = <?= json_encode($products); ?>;
		news_products = <?= json_encode($news_products); ?>;
		shopping_cart = <?= json_encode($shopping_cart) ?>;
		customer = <?= json_encode($customer) ?>;
		category_selected = <?= json_encode($category_id) ?>;
		search = <?= json_encode($search_word) ?>;
		var total = 0;
		var amount = 0;
		if (shopping_cart) {

			$.each(shopping_cart, function (i, article) {
				total += parseFloat(article.price) * parseInt(article.amount);
				amount += parseInt(article.amount);
			});
		}

		$("#shopping-cart-quantity").text(amount);
		$("#shopping-cart-total").text('Gs.' + parseFloat(total));

		$('.btn-view-product').click(function() {
			var type = $(this).data('type');
			var id = $(this).data('id');

			$.each (products, function(i, product) {
				if (id == product.id) {
					product_selected = product;
				}
			});

			var style_id;

			$(".section-images-styles[data-product-id='" + product_selected.id + "']").each(function(index, value) {
			    if ($(this).css("display") == "block") {
					style_id = $(this).data('id');
			    }
			});

			$.each (product_selected.styles, function(i, style) {
				if (style_id == style.id) {
					style_selected = style;
				}
			});

			stylesLoad();
			styleChanged(style_selected);

			$('#custom-description').html("");
			if (product_selected.custom_description) {
				$('#custom-description').append("<legend style='font-family: Glegoo, serif;font-size: 18px;'>Descripción</legend>");
				$('#custom-description').append(product_selected.custom_description);
			}

			// agrega el id del articulo al boton de favoritos
			$('.btn-favorite').data('id', article_selected.id);
			$('.btn-favorite img').attr('src', '/app/img/icon_wishlist.png');
			if (customer) {
				$.each (customer.favorites, function(i, favorite) {
					if (favorite.article.id == article_selected.id) {
						$('.btn-favorite img').attr('src', '/app/img/icon_wishlist_added.png');
					}
				});
			}

			$('#article_amount').text("1");
			$('#quick-view-popup').modal('show');
		});

		//seleccion del estilo
		$('div').on("click", "a.img-stylex", function() {

			var style_id = $(this).data('id');
			var product_id = $(this).data('product-id');

			$(".section-images-styles[data-product-id='" + product_id +"']").each(function(index, value) {
			    var id = $(this).data('id');

			    if (id == style_id) {
			    	$("a.img-stylex[data-id='" + id +"']").addClass('product-selected');
					$(this).css('display', '');
			    } else {
			    	$("a.img-stylex[data-id='" + id +"']").removeClass('product-selected');
					$(this).css('display', 'none');
			    }
			});
		});

		//seleccion de las imagnes del costado si se hace click aparece como imagen principal
	    $('div').on("click", "a.asset-option", function() {
			var id = $(this).data('id');
			var asset;
			$.each (style_selected.assets, function(i, asst) {
				if (asst.id == id) {
					asset = asst;
				}
			});
			mainAssetLoad(asset);
		});

		//seleccion del estilo
	    $('div').on("click", "a.img-style", function() {
			var id = $(this).data('id');

			$.each (product_selected.styles, function(i, style) {
				if (style.id == id) {
				    $("a.img-style[data-id='" + style.id +"']").addClass('product-selected');
					style_selected = style;
					styleChanged(style_selected);
				} else {
					$("a.img-style[data-id='" + style.id +"']").removeClass('product-selected');
				}
			});
	    });

	    //cambia el estilo de la seleccion del boton de talles
	    $('div').on("click", "a.btn-size", function() {
			var id = $(this).data('id');
			$('.section-size a').each (function(index) {
				$(this).removeClass("btn-sizes-selected").addClass("btn-sizes");
				if (id == $(this).data('id')) {
					$(this).addClass("btn-sizes-selected");
					sizeChanged(id);
				}
			});
	    });

	    //quantity
		$('.value-plus').on('click', function() {
			var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) + 1;
			divUpd.text(newVal);
		});

		$('.value-minus').on('click', function() {
			var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) - 1;
			if (newVal >= 1) divUpd.text(newVal);
		});

		//agrega un articulo al carrito
		$('#btn-add-to-cart').click(function() {

			var amount = parseInt($("#article_amount").text());
			var id = article_selected.id;

			addToCart(id, amount);
		});

		$("#most-relevant").change(function () {
		    var most_relevant_selected = $("#most-relevant").find(':selected').text();
			var action = "/app/catalogo/buscador";
			if (category_selected && category_selected != 'false') {
				action += "/" + category_selected;
			}
			if (most_relevant_selected == 'Menor precio') {
				// reorndernar version jquery
				/*function sortByMinorPrice(a, b){
					var aPrice = a.price;
					var bPrice = b.price; 
					return ((aPrice < bPrice) ? -1 : ((aPrice > bPrice) ? 1 : 0));
				}

				products.sort(sortByMinorPrice);*/
				order = 'ASC';
			} else if (most_relevant_selected == 'Mayor precio') {
					// reorndernar version jquery
				/*function sortByMajorPrice(a, b){
					var aPrice = a.price;
					var bPrice = b.price; 
					return ((aPrice > bPrice) ? -1 : ((aPrice < bPrice) ? 1 : 0));
				}

				products.sort(sortByMajorPrice);*/
				order = 'DESC';
			} else {
				order = "";
			}
			$('body')
		        .append( $('<form/>').attr({'action': action, 'method': 'post', 'id': 'replacer'})
		        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'group_selected', 'value': null}))
		        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'search', 'value': search}))
		        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'order', 'value': order})))
		        .find('#replacer').submit();
		});

		$('.group-name').click(function() {
          var id = $(this).data('id');
          $('body')
            .append( $('<form/>').attr({'action': '/app/catalogo/index/', 'method': 'post', 'id': 'replacer'})
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'group_selected', 'value': id}))
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'order', 'value': null})))
            .find('#replacer').submit();
        });

        $('.size-guide').click(function() {
			if ( article_selected.size_image) {
				$('#lbl-size-image').hide();
				$("#img-size-image").show();
				$("#img-size-image").attr("src", "/app/images/" + article_selected.size_image);
			} else {
				$("#img-size-image").hide();
				$('#lbl-size-image').show();
			}

			$('#size-image-modal').modal('show');
		});
	});
</script>
