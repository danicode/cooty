<div class="row">
     <div class="col-md-2">
        <div class="pull-right">
             <a class="btn btn-default" title="Ir a la lista de categorias." href="<?=$this->Url->build(["action" => "index"])?>" role="button">
                <span class="glyphicon icon-undo2" aria-hidden="true"></span>
            </a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        <legend><?= __('Categoria') ?></legend>
        <?= $this->Form->create($category) ?>
        <fieldset>
            <?php
                echo $this->Form->input('name', ['label' => __('nombre')]);
                echo $this->Form->input('group_id', ['label' => __('Grupo'), 'options' => $groups]);
            ?>
        </fieldset>
        <?= $this->Form->button(__('Guardar')) ?>
        <?= $this->Form->end() ?>
    </div>

    <div class="col-md-5">

        <div class="row">
            <div class="col-md-11">
                <legend><?= __('Galeria') ?></legend>
            </div>
            <div class="col-md-1">
                <div class="pull-right">
                    <a class="btn btn-default" title="Nueva Imagen" href="#" role="button" id="btn-add-asset" >
                        <span class="glyphicon icon-plus" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div id="gallery" class="row">
                    <?php foreach ($category->categories_assets as $ca): ?>
                    <div class='col-md-6'>
                        <div class='row'><div class='col-md-12'><img class="img-responsive" style="width:100%" src="/app/images/<?= $ca->url ?>"></img></div></div><div class='row'><div class='col-md-12'><a class='btn-delete-asset' data-id="<?= $ca->id ?>" href='javascript:void(0)'><?= __('Quitar') ?></a></div></div>
                        <div class="checkbox chck-cover-page" data-id="<?= $ca->id ?>">
                          <label>
                               <?php if ($ca->visible): ?>
                                   <input type="checkbox" checked value="1">
                               <?php else: ?>
                                   <input type="checkbox" value="0">
                               <?php endif; ?>
                            <?= __('Portada') ?>
                          </label>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="assets-popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?= __('Añadir Imagen') ?></h4>
      </div>
      <div class="modal-body">
           <?= $this->Form->create(null, ['url' => ['controller' => 'CategoriesAssets' , 'action' => 'add'], 'enctype' => "multipart/form-data" ]) ?>
            <fieldset>
                <?php
                    echo $this->Form->file('files[]', ['required' => true, 'multiple'=>'multiple']);
                    echo $this->Form->input('category_id', ['type' => 'hidden', 'value' => $category->id]);
                ?>
            </fieldset>
            <br>

            <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Cancelar') ?></button>
            <?= $this->Form->button(__('Agregar')) ?>
            <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

    var category = <?= json_encode($category); ?>;

    $(document).ready(function () {
       
    });

    $('a#btn-add-asset').click(function() {
        $('#assets-popup').modal('show');
    });

    $(document).on("click", "a.btn-delete-asset", function(e) {

        var id  = $(this).data('id');
        var text = 'Esta Seguro que desea eliminar la imagen ? ';
        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/app/categories-assets/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id})))
                .find('#replacer').submit();
            }
        });
    });

    $(document).on("click", "div.chck-cover-page", function(e) {

        var id  = $(this).data('id');
        $('body')
        .append( $('<form/>').attr({'action': '/app/categories-assets/visible/', 'method': 'post', 'id': 'replacer'})
        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id})))
        .find('#replacer').submit();
    });
</script>