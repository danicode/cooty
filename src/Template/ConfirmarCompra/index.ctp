<style type="text/css">

    checkbox-inline, .radio-inline {
	    font-size: 14px;
	}

	.cc-selector input{
	    margin:0;padding:0;
	    -webkit-appearance:none;
	       -moz-appearance:none;
	            appearance:none;
	}
	.visa{background-image:url(/app/img/money.png);}
	.mastercard{background-image:url(/app/img/pos.png);}
	
	.cc-selector input:active +.drinkcard-cc{opacity: .9;}
	.cc-selector input:checked +.drinkcard-cc{
	    -webkit-filter: none;
	       -moz-filter: none;
	            filter: none;
	}
	.drinkcard-cc{
	    cursor:pointer;
	    background-size:contain;
	    background-repeat:no-repeat;
	    display:inline-block;
	    width:100px;
	    /*height:70px;*/
	    height:100px;
	    -webkit-transition: all 100ms ease-in;
	       -moz-transition: all 100ms ease-in;
	            transition: all 100ms ease-in;
	    -webkit-filter: brightness(1.8) grayscale(1) opacity(.7);
	       -moz-filter: brightness(1.8) grayscale(1) opacity(.7);
	            filter: brightness(1.8) grayscale(1) opacity(.7);
	}
	.drinkcard-cc:hover{
	    -webkit-filter: brightness(1.2) grayscale(.5) opacity(.9);
	       -moz-filter: brightness(1.2) grayscale(.5) opacity(.9);
	       		filter: brightness(1.2) grayscale(.5) opacity(.9);
	}

	/* Extras */
	a:visited{color:#888}
	a{color:#444;text-decoration:none;}
	p{margin-bottom:.3em;}
</style>

<!-- banner -->
<!--<?php foreach ($coversPages as $coversPage): ?>-->
<!--	<?php if ($coversPage->principal): ?>-->
<!--	<div style="background:url(/app/images/<?= $coversPage->url ?>) no-repeat 0px 0px;" class="banner10" id="home1">-->
<!--	<?php endif; ?>-->
<!--<?php endforeach; ?>-->
		<div class="container text-center">
			<h2><?= __('Confirmar Compra') ?></h2>
		</div>
	</div>
<!-- //banner -->

<!-- breadcrumbs -->
	<div class="breadcrumb_dress">
		<div class="container">
			<ul>
				<li><a href="/app/home/index"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> <?= __('Inicio') ?></a> <i>/</i></li>
				<li><?= __('Confirmar Compra') ?></li>
			</ul>
		</div>
	</div>
<!-- //breadcrumbs -->

<!-- confirmar compra -->
	<div class="row">
		<div style="margin-top: 25px; margin-bottom: 25px;" class="container">
			<div class="col-md-3">
			    <?= $this->Form->create(null, ['url' => ['controller' => 'orders', 'action' => 'add']]) ?>
			    <fieldset>
			    	<legend><?= __('Datos de Envio') ?></legend>
			        <?php
			            echo $this->Form->input('customer_name', ['label' =>  __('Nombre'), 'required' => true, 'value' => ($data) ? $data['customer_name'] : '']);
			            echo $this->Form->input('email', ['label' => __('Correo'), 'value' => ($data) ? $data['email'] : '']);
			            echo $this->Form->input('phone', ['label' => __('Teléfono'), 'value' => ($data) ? $data['phone'] : '']);
			            echo $this->Form->input('address', ['label' => __('Domicilio'), 'required' => true, 'value' => ($data) ? $data['address'] : '']);
					?>
					<label style="border-bottom: 1px solid #e3dfdf;width: 100%;" class="control-label"><?= __('Formas de pago') ?></label>
					<!--<div style="padding: 10px;">-->
					<!--	<label class="radio-inline"><input type="radio" name="payment_methods" value="0" checked><?= __('Contado efectivo') ?> <i class="fa fa-money" aria-hidden="true"></i></label> -->
					<!--	<label class="radio-inline"><input type="radio" name="payment_methods" value="1"><?= __('POS') ?> <i class="fa fa-credit-card-alt" aria-hidden="true"></i></label>-->
					<!--</div>-->
					

			    	<div class="row cc-selector">
			    		<div class="col-md-6 text-center">
			    			<input id="visa" type="radio" name="payment_methods" value="0" checked/>
				        	<label class="drinkcard-cc visa" for="visa"></label>
			    		</div>
				        <div class="col-md-6 text-center">
					        <input id="mastercard" type="radio" name="payment_methods" value="1" />
					        <label class="drinkcard-cc mastercard"for="mastercard"></label>
				        </div>
			        </div>
			    </fieldset>
			    <?= $this->Form->button(__('Confirmar'), ['class' => 'btn btn-success btn-block']) ?>
			    <?= $this->Form->end() ?>
			</div>
			<div class="col-md-9">
			    <div>
			    	<?php foreach ($coversPages as $coversPage): ?>
						<?php if ($coversPage->nosotros): ?>
							<img style="margin-top: 8px; margin-left: 100px;" src="/app/images/<?= $coversPage->url ?>" alt=" " class="img-responsive" />
						<?php endif; ?>
					<?php endforeach; ?>
			    </div>
			</div>
		</div>
	</div>
<!-- //confirmar compra -->

<script type="text/javascript">
	var shopping_cart;
	var customer;

	$(document).ready(function() {

		customer = <?= json_encode($customer) ?>;
		shopping_cart = <?= json_encode($shopping_cart) ?>;
		var total = 0;
		var amount = 0;
		if (shopping_cart) {

			$.each(shopping_cart, function (i, article) {
				total += parseFloat(article.price) * parseInt(article.amount);
				amount += parseInt(article.amount);
			});
		}

		$("#shopping-cart-quantity").text(amount);
		$("#shopping-cart-total").text('Gs.' + parseFloat(total));
	});
</script>
