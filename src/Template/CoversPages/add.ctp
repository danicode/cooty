<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Covers Pages'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="coversPages form large-9 medium-8 columns content">
    <?= $this->Form->create($coversPage) ?>
    <fieldset>
        <legend><?= __('Add Covers Page') ?></legend>
        <?php
            echo $this->Form->input('url');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
