<div class="row">
    <div class="col-md-12">

        <div class="row">
            <div class="col-md-12">
                <div id="gallery" class="row">
                    <div class='col-md-4'>
                        <div class="row">
                            <div class="col-md-11">
                                <legend><?= __('Imagen Página Principal') ?></legend>
                            </div>
                            <div class="col-md-1">
                                <div class="pull-right">
                                    <a class="btn btn-default" title="<?= __('Nueva Imagen') ?>" href="#" role="button" id="btn-add-asset-principal" >
                                        <span class="glyphicon icon-plus" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <?php foreach ($coversPages as $coverPage): ?>
                            <?php if ($coverPage->principal): ?>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <img style="max-width: 400px; height: auto; display: block;" src="/app/images/<?= $coverPage->url ?>"></img>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <a class='btn-delete-asset' data-id="<?= $coverPage->id ?>" href='javascript:void(0)'><?= __('Quitar') ?></a>
                                        <div class="checkbox chck-cover-page" data-id="<?= $coverPage->id ?>">
                                          <label>
                                               <?php if ($coverPage->visible): ?>
                                                   <input type="checkbox" checked value="1">
                                               <?php else: ?>
                                                   <input type="checkbox" value="0">
                                               <?php endif; ?>
                                               <?= __('Visible') ?>
                                          </label>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    
                    <div class='col-md-4'>
                        <div class="row">
                            <div class="col-md-11">
                                <legend><?= __('Imagen Página Nosotros') ?></legend>
                            </div>
                            <div class="col-md-1">
                                <div class="pull-right">
                                    <a class="btn btn-default" title="<?= __('Nueva Imagen') ?>" href="#" role="button" id="btn-add-asset-nosotros" >
                                        <span class="glyphicon icon-plus" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <?php foreach ($coversPages as $coverPage): ?>
                            <?php if ($coverPage->nosotros): ?>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <img style="max-width: 400px; height: auto; display: block;" src="/app/images/<?= $coverPage->url ?>"></img>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <a class='btn-delete-asset' data-id="<?= $coverPage->id ?>" href='javascript:void(0)'><?= __('Quitar') ?></a>
                                        <div class="checkbox chck-cover-page" data-id="<?= $coverPage->id ?>">
                                          <label>
                                               <?php if ($coverPage->visible): ?>
                                                   <input type="checkbox" checked value="1">
                                               <?php else: ?>
                                                   <input type="checkbox" value="0">
                                               <?php endif; ?>
                                               <?= __('Visible') ?>
                                          </label>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<div id="assets-popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?= __('Añadir Imagen') ?></h4>
      </div>
      <div class="modal-body">
           <?= $this->Form->create(null, ['url' => ['controller' => 'CoversPages' , 'action' => 'add'], 'enctype' => "multipart/form-data" ]) ?>
            <fieldset>
                <?php
                    echo $this->Form->file('files[]', ['required' => true, 'multiple'=>'multiple']);
                ?>
            </fieldset>
            <br>
            <input id="cover-page-section" type="hidden" name="section" value="">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Cancelar') ?></button>
            <?= $this->Form->button(__('Agregar')) ?>
            <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

    $('a#btn-add-asset-principal').click(function() {
        console.log("dasdasd");
        $('#cover-page-section').val("principal");
        $('#assets-popup').modal('show');
    });

    $('a#btn-add-asset-nosotros').click(function() {
        $('#cover-page-section').val("nosotros");
        $('#assets-popup').modal('show');
    });

    $(document).on("click", "a.btn-delete-asset", function(e) {

        var id  = $(this).data('id');
        var text = 'Esta Seguro que desea eliminar la imagen de portada ? ';
        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/app/covers-pages/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id})))
                .find('#replacer').submit();
            }
        });
    });

    $(document).on("click", "div.chck-cover-page", function(e) {

        var id  = $(this).data('id');
        $('body')
        .append( $('<form/>').attr({'action': '/app/covers-pages/visible/', 'method': 'post', 'id': 'replacer'})
        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id})))
        .find('#replacer').submit();
    });

</script>
