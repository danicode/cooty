<div class="row">
    <div class="col-md-2">
        <?= $this->Form->create($customer) ?>
        <fieldset>
            <legend><?= __('Agregar Cliente') ?></legend>
            <?php
                echo $this->Form->input('name', ['label' => 'Nombre']);
                echo $this->Form->input('email', ['label' => 'Correo']);
                echo $this->Form->input('address', ['label' => 'Dirección']);
                echo $this->Form->input('phone', ['label' => 'Teléfono']);
                echo $this->Form->input('password', ['label' => 'Clave']);
            ?>
        </fieldset>
        <?= $this->Form->button(__('Aceptar')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
