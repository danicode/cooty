
<br>
<br>

<section id="services">
    <div class="container">

        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <center>
                     <?php echo $this->Html->image('logo.png', ['alt' => '', 'width' => 240]);?>
                </center>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-md-offset-5">
                <br>
                <?= $this->Form->create() ?>
                <?= $this->Form->input('email', ['label' => __('Correo'), 'autofocus' => true, 'type' => 'email']) ?>
                <?= $this->Form->input('password', ['label' => __('Clave')]) ?>
                <?= $this->Form->button(__('Entrar'), ['class' => 'btn btn-primary']) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>

    </div>
</div>
