<div class="row">
     <div class="col-md-2">
        <div class="pull-right">
             <a class="btn btn-default" title="Ir a la lista de Repartidores." href="<?=$this->Url->build(["action" => "index"])?>" role="button">
                <span class="glyphicon icon-undo2"  aria-hidden="true"></span>
            </a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        <?= $this->Form->create($delivery) ?>
        <fieldset>
            <legend><?= __('Editar Repartidor') ?></legend>
            <?php
                echo $this->Form->input('name', ['label' => __('Nombre')]);
                echo $this->Form->input('description', ['label' => __('Descripción')]);
                echo $this->Form->input('enable', ['label' => __('Habilitar')]);
            ?>
        </fieldset>
        <?= $this->Form->button(__('Guardar')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
