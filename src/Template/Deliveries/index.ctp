<style type="text/css">

    #table-deliveries {
        width: 100% !important;
    }

    #table-deliveries td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 5px 0px 5px !important;
        vertical-align: middle;
    }

    .modal-actions a.btn {
        width: 100%;
        margin-bottom: 5px;
    }

    tr.selected {
        background-color: #8eea99;
        color: #333 !important;
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }

</style>

<div id="btns-tools">
    <div class="pull-right btns-tools margin-bottom-5">
        <?php if ($userAdmin): ?>
            <a class="btn btn-default" title="Nuevo Repartidor" href="<?=$this->Url->build(["controller" => "Deliveries", "action" => "add"])?>" role="button">
                <span class="glyphicon icon-plus" aria-hidden="true"></span>
            </a>
        <?php endif; ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-deliveries" >
            <thead>
                <tr>
                    <th><?= __('#') ?></th>
                    <th><?= __('Nombre') ?></th>
                    <th><?= __('Descripción') ?></th>
                    <th><?= __('Creado') ?></th>
                    <th><?= __('Modificado') ?></th>
                    <th><?= __('Habilitado') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($deliveries as $delivery): ?>
                <tr data-id="<?=$delivery->id?>">
                    <td><?= $delivery->id ?></td>
                    <td><?= h($delivery->name) ?></td>
                    <td><?= h($delivery->description) ?></td>
                    <td><?= ($delivery->created) ? date('d/m/Y H:i', strtotime($delivery->created->format('Y-m-d H:i:s'))) : '' ?></td>
                    <td><?= ($delivery->modified) ? date('d/m/Y H:i', strtotime($delivery->modified->format('Y-m-d H:i:s'))) : '' ?></td>
                    <?php if ($delivery->enable): ?>
                        <td>
                            <span class="icon-checkmark2 green" aria-hidden="true"></span>
                            <span style="display:none;" >1</span>
                        </td>
                    <?php else: ?>
                        <td>
                            <span class="icon-cancel-circle red" aria-hidden="true"></span>
                            <span style="display:none;" >0</span>
                        </td>
                    <?php endif; ?>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?= __('Acciones') ?></h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a class="btn btn-default " id="btn-edit" href="#" role="button">
                        <span class="glyphicon glyphicon-edit"  aria-hidden="true"></span>
                        <?= __('Editar') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>
                    <a class="btn btn-danger btn-delete2" href="#" role="button" >
                        <span class="glyphicon icon-bin"  aria-hidden="true"></span>
                        <?= __('Eliminar') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>
                </center>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

   var table_deliveries = null;
   var userAdmin = <?= $userAdmin; ?>;

   $(document).ready(function () {

        $('#table-deliveries').removeClass('display');

		table_deliveries = $('#table-deliveries').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "columnDefs": [
                {
                    "targets": [ 3 ],
                    "visible": true,
                    "searchable": true
                },
            ],
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cangando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguinte",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "Todas"]],
            /*"dom": '<"toolbar">frtip'*/
		});

		$('#table-deliveries_filter').closest('div').before($('#btns-tools').contents());

    });

    $('#table-deliveries tbody').on( 'click', 'tr', function (e) {
        if (!$(this).find('.dataTables_empty').length) {

            table_deliveries.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            if (userAdmin) {
                $('.modal-actions').modal('show');
            }
        }
    });

    $('a#btn-edit').click(function() {
       var action = '/app/deliveries/edit/' + table_deliveries.$('tr.selected').data('id');
       window.open(action, '_self');
    });

    $(document).on("click", ".btn-delete2", function(e) {

        var text = 'Esta Seguro que desea eliminar el repartidor ?';
        var controller  = 'Deliveries';
        var id  = table_deliveries.$('tr.selected').data('id');

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/app/'+controller+'/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"})))
                .find('#replacer').submit();
            }
        });
    });

</script>
