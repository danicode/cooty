<h3>Se ha restablecido su Clave</h3>

<p>Esta es su nueva Clave: <b><?= $psw ?></b></p>

<p>Por favor, intente ingresar a su cuenta de nuevo: <a href="www.cootchycoo.com">Cootchy Coo.</a></p>
