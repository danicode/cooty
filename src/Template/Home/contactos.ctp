<!-- banner -->
<!--<?php foreach ($coversPages as $coversPage): ?>-->
<!--	<?php if ($coversPage->principal): ?>-->
<!--	<div style="background:url(/app/images/<?= $coversPage->url ?>) no-repeat 0px 0px;" class="banner10" id="home1">-->
<!--	<?php endif; ?>-->
<!--<?php endforeach; ?>-->

<!-- //banner-bottom -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
  	<?php foreach ($promotions as $key => $promotion): ?>
    	<li data-target="#myCarousel" data-slide-to="<?= $key ?>" class="<?= ($key == 0) ? 'active' : '' ?>"></li>
    <?php endforeach; ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
  	<?php foreach ($promotions as $key => $promotion): ?>
	    <div class="item <?= ($key == 0) ? 'active' : '' ?>">
	      <a href="/app/catalogo/index"><img src="/app/images/<?= $promotion->promotions_assets[0]->url ?>" alt="" style="width: 100%;"></a>
	    </div>
    <?php endforeach; ?>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only"><?= __('Anterior') ?></span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only"><?= __('Siguiente') ?></span>
  </a>
</div>

	<!--	<div class="container">-->
	<!--		<h2><?= __('Contactos') ?></h2>-->
	<!--	</div>-->
	<!--</div>-->
<!-- //banner -->

<!-- breadcrumbs -->
	<div class="breadcrumb_dress">
		<div class="container">
			<ul>
				<li><a href="/app/home/index"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> <?= __('Inicio') ?></a> <i>/</i></li>
				<li><?= __('Contactos') ?></li>
			</ul>
		</div>
	</div>
<!-- //breadcrumbs -->

<!-- mail -->
	<div class="mail">
		<div class="container">
			<h3>Contactos</h3>
			<div class="agile_mail_grids">
				<div class="col-md-5 contact-left">
					<h4><?= __('Datos de contacto') ?></h4>
					<p><?= __('Paseo Boggiani') ?>
						<span><?= __('Boggiani 5976') ?></span></p>
					<ul>
						<li><?= __('Teléfono :021 602 439') ?></li>
						<li><a href="mailto:info@cootchycoo.com"><?= __('info@cootchycoo.com') ?></a></li>
					</ul>
				</div>
				<div class="col-md-7 contact-left">
					<h4><?= __('Formulario de Contacto') ?></h4>
					<form action="#" method="post">
						<input type="text" name="Name" value="<?= __('Nombre') ?>" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Nombre';}" required="">
						<input type="email" name="Email" value="<?= __('Correo') ?>" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Correo';}" required="">
						<input type="text" name="Telephone" value="<?= __('Teléfono') ?>" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Teléfono';}" required="">
						<textarea name="message" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Mensaje...';}" required=""><?= __('Mensaje...') ?></textarea>
						<input type="submit" value="<?= __('Enviar') ?>" >
					</form>
				</div>
				<div class="clearfix"> </div>
			</div>

			<div class="contact-bottom">
				<iframe src="https://www.google.com/maps/d/embed?mid=1IH33eNjxj0Ov4_4XpHWEV7GAeyU" width="640" height="480"></iframe>
			</div>
		</div>
	</div>
<!-- //mail -->

<script type="text/javascript">
	var shopping_cart;
	var customer;

	$(document).ready(function() {

		shopping_cart = <?= json_encode($shopping_cart) ?>;
		customer = <?= json_encode($customer) ?>;
		var total = 0;
		var amount = 0;
		if (shopping_cart) {

			$.each(shopping_cart, function (i, article) {
				total += parseFloat(article.price) * parseInt(article.amount);
				amount += parseInt(article.amount);
			});
		}

		$("#shopping-cart-quantity").text(amount);
		$("#shopping-cart-total").text('Gs.' + parseFloat(total);
	});

</script>
