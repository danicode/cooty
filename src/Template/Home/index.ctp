<style type="text/css">

	.add_wishlist img {
	    width: 18px;
	    height: 18px;
	    opacity: .7;
	    cursor: pointer;
	}

	.add_wishlist img:hover {
	    opacity: 1;
	}

	.add_wishlist {
	    display: block;
	    position: relative;
		margin-left: 23px;
		margin-bottom: 15px;
	}

	img .iWish {
	    border: 0;
	    width: 100%;
	    vertical-align: middle;
	}

	.lbl-add-favorite {
		color: #8c8c8c;
		cursor: pointer;
		font-size: 14px;
	}
	
	.item {
		max-height: 600px;
	}

	.simpleCart_shelfItem p span {
	    text-decoration: none;
	}

</style>

<!-- //banner-bottom -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
  	<?php foreach ($promotions as $key => $promotion): ?>
    	<li data-target="#myCarousel" data-slide-to="<?= $key ?>" class="<?= ($key == 0) ? 'active' : '' ?>"></li>
    <?php endforeach; ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
  	<?php foreach ($promotions as $key => $promotion): ?>
	    <div class="item <?= ($key == 0) ? 'active' : '' ?>">
	      <a href="/app/catalogo/index"><img src="/app/images/<?= $promotion->promotions_assets[0]->url ?>" alt="" style="width: 100%;"></a>
	    </div>
    <?php endforeach; ?>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only"><?= __('Anterior') ?></span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only"><?= __('Siguiente') ?></span>
  </a>
</div>


		<div class="container">
		</div>
	</div>
<!-- //banner -->

<!-- banner-bottom -->
	<div class="banner-bottom">
		<div class="container">
			<div class="col-md-5 wthree_banner_bottom_left">
				<div class="video-img">
					<a class="play-icon popup-with-zoom-anim" href="#small-dialog">
						<span class="glyphicon glyphicon-expand" aria-hidden="true"></span>
					</a>
				</div>
				<!-- pop-up-box -->    
					<link href="/app/css/ecommerce/popuo-box.css" rel="stylesheet" type="text/css" property="" media="all" />
					<script src="/app/js/ecommerce/jquery.magnific-popup.js" type="text/javascript"></script>
				<!--//pop-up-box -->
				<div id="small-dialog" class="mfp-hide">
					<iframe src="https://player.vimeo.com/video/163173773?title=0&byline=0&portrait=0"></iframe>
				</div>
				<script>
					$(document).ready(function() {
						$('.popup-with-zoom-anim').magnificPopup({
							type: 'inline',
							fixedContentPos: false,
							fixedBgPos: true,
							overflowY: 'auto',
							closeBtnInside: true,
							preloader: false,
							midClick: true,
							removalDelay: 300,
							mainClass: 'my-mfp-zoom-in'
						});
					});
				</script>
			</div>
			<div class="col-md-7 wthree_banner_bottom_right">
				<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
					<ul id="myTab" class="nav nav-tabs" role="tablist">
						<?php $flag = true; ?>
						<?php foreach ($seasons as $season): ?>
							<?php $selector = strtolower($season->name); $selector = str_replace(" ", "_", $selector); ?>
							<li role="presentation" class="<?= ($flag) ? 'active' :'' ?>"><a href="#<?= $selector ?>" id="<?= $selector ?>-tab" role="tab" data-toggle="tab" aria-controls="<?= $selector ?>"><?= $season->name ?></a></li>
							<?php $flag = false; ?>
						<?php endforeach; ?>
					</ul>
					<div id="myTabContent" class="tab-content">
						<?php $flag = true; ?>
						<?php foreach ($seasons as $season): ?>
						<?php $selector = strtolower($season->name); $selector = str_replace(" ", "_", $selector); ?>
						<div role="tabpanel" class="tab-pane fade <?= ($flag) ? 'active' :'' ?> in" id="<?= $selector ?>" aria-labelledby="<?= $selector ?>-tab">
								<?php $flag = false; ?>
							<div class="agile_ecommerce_tabs">
								<?php foreach ($season->products as $product): ?>
									<div class="col-md-4 agile_ecommerce_tab_left">
										<div class="hs-wrapper">
											<?php $display = "block"; ?>
											<?php foreach ($product->styles as $style): ?>
											 	<div class="section-images-styles" data-product-id="<?= $product->id; ?>" data-id="<?= $style->id; ?>" style="display: <?= $display; ?>">
													<?php $images = array(); $i = 0; $img = ''?>
													<?php foreach ($style->assets as $asset): ?>
														<?php if ($i == 0): ?>
															<?php $img = $asset->url; ?>
														<?php endif; $i++; ?>
													    <!-- imagen debe tener un width: 300 px - height: 400 px -->
														<?php array_push($images, '/app/images/' . $asset->url); ?>
													<?php endforeach; ?>
													<div class="images-rotation" data-images='["<?= implode("\", \"", $images); ?>"]'>
												    	<img style="cursor: pointer;" data-type='news' data-id="<?= $product->id ?>" src="/app/images/<?= $img ?>" alt="" class="btn-view-product img-responsive">
												  	</div>
													<?php $display = "none"; ?>
												</div>
											<?php endforeach; ?>
										</div>
										<h5><a href="javascript:void(0)"><?= __($product->name) ?></a></h5>
										<div class="simpleCart_shelfItem">
											<p><i class="item_price">Gs.<?= $product->price ?></i></p>
										</div>
										<div class="row">
										  <?php $product_selected = 'product-selected' ?>
								          <?php foreach ($product->styles as $style): ?>
									          <div class="col-sm-2">
							            			<a class='img-styles <?= $product_selected ?>' data-product-id="<?= $product->id ?>" data-id="<?= $style->id ?>" href="javascript:void(0)"><img src="/app/images/<?= $style->img_url ?>" class'img-responsive' style='border-radius: 30px; height: 30px; width: 30px;' alt="<?= $style->name ?>"></a>
									          </div>
									          <?php $product_selected = '' ?>
								          <?php endforeach; ?>
							       		</div>
							       		<?php $i++; ?>
										<?php if ($i == 3): ?>
											<?php echo '<div class="clearfix"></div>' ?>
											<?php $i = 0; ?>
										<?php endif; ?>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
				<!--modal-video-->
				<div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
							</div>
							<section>
								<div class="modal-body">
									<div class="col-md-5 modal_body_left">
										<img src="/app/images/20.jpg" alt=" " class="img-responsive" />
									</div>
									<div class="col-md-7 modal_body_right">
										<h4>a good look women's shirt</h4>
										<p>Ut enim ad minim veniam, quis nostrud 
											exercitation ullamco laboris nisi ut aliquip ex ea 
											commodo consequat.Duis aute irure dolor in 
											reprehenderit in voluptate velit esse cillum dolore 
											eu fugiat nulla pariatur. Excepteur sint occaecat 
											cupidatat non proident, sunt in culpa qui officia 
											deserunt mollit anim id est laborum.</p>
										<div class="rating">
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star.png" alt=" " class="img-responsive" />
											</div>
											<div class="clearfix"> </div>
										</div>
										<div class="modal_body_right_cart simpleCart_shelfItem">
											<p><span>Gs.320</span> <i class="item_price">Gs.250</i></p>
											<p><a class="item_add" href="#">Add to cart</a></p>
										</div>
										<h5>Color</h5>
										<div class="color-quality">
											<ul>
												<li><a href="#"><span></span>Red</a></li>
												<li><a href="#" class="brown"><span></span>Yellow</a></li>
												<li><a href="#" class="purple"><span></span>Purple</a></li>
												<li><a href="#" class="gray"><span></span>Violet</a></li>
											</ul>
										</div>
									</div>
									<div class="clearfix"> </div>
								</div>
							</section>
						</div>
					</div>
				</div>
				<div class="modal video-modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModal1">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
							</div>
							<section>
								<div class="modal-body">
									<div class="col-md-5 modal_body_left">
										<img src="/app/images/63.jpg" alt=" " class="img-responsive" />
									</div>
									<div class="col-md-7 modal_body_right">
										<h4>a good look black women's jeans</h4>
										<p>Ut enim ad minim veniam, quis nostrud 
											exercitation ullamco laboris nisi ut aliquip ex ea 
											commodo consequat.Duis aute irure dolor in 
											reprehenderit in voluptate velit esse cillum dolore 
											eu fugiat nulla pariatur. Excepteur sint occaecat 
											cupidatat non proident, sunt in culpa qui officia 
											deserunt mollit anim id est laborum.</p>
										<div class="rating">
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star.png" alt=" " class="img-responsive" />
											</div>
											<div class="clearfix"> </div>
										</div>
										<div class="modal_body_right_cart simpleCart_shelfItem">
											<p><span>Gs.320</span> <i class="item_price">Gs.250</i></p>
											<p><a class="item_add" href="#">Add to cart</a></p>
										</div>
										<h5>Color</h5>
										<div class="color-quality">
											<ul>
												<li><a href="#"><span></span>Red</a></li>
												<li><a href="#" class="brown"><span></span>Yellow</a></li>
												<li><a href="#" class="purple"><span></span>Purple</a></li>
												<li><a href="#" class="gray"><span></span>Violet</a></li>
											</ul>
										</div>
									</div>
									<div class="clearfix"> </div>
								</div>
							</section>
						</div>
					</div>
				</div>
				<div class="modal video-modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModal2">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
							</div>
							<section>
								<div class="modal-body">
									<div class="col-md-5 modal_body_left">
										<img src="/app/images/23.jpg" alt=" " class="img-responsive" />
									</div>
									<div class="col-md-7 modal_body_right">
										<h4>a good look women's Watch</h4>
										<p>Ut enim ad minim veniam, quis nostrud 
											exercitation ullamco laboris nisi ut aliquip ex ea 
											commodo consequat.Duis aute irure dolor in 
											reprehenderit in voluptate velit esse cillum dolore 
											eu fugiat nulla pariatur. Excepteur sint occaecat 
											cupidatat non proident, sunt in culpa qui officia 
											deserunt mollit anim id est laborum.</p>
										<div class="rating">
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star.png" alt=" " class="img-responsive" />
											</div>
											<div class="clearfix"> </div>
										</div>
										<div class="modal_body_right_cart simpleCart_shelfItem">
											<p><span>$320</span> <i class="item_price">$250</i></p>
											<p><a class="item_add" href="#">Add to cart</a></p>
										</div>
										<h5>Color</h5>
										<div class="color-quality">
											<ul>
												<li><a href="#"><span></span>Red</a></li>
												<li><a href="#" class="brown"><span></span>Yellow</a></li>
												<li><a href="#" class="purple"><span></span>Purple</a></li>
												<li><a href="#" class="gray"><span></span>Violet</a></li>
											</ul>
										</div>
									</div>
									<div class="clearfix"> </div>
								</div>
							</section>
						</div>
					</div>
				</div>
				<div class="modal video-modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModal3">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
							</div>
							<section>
								<div class="modal-body">
									<div class="col-md-5 modal_body_left">
										<img src="/app/images/24.jpg" alt=" " class="img-responsive" />
									</div>
									<div class="col-md-7 modal_body_right">
										<h4>a good look women's Sandal</h4>
										<p>Ut enim ad minim veniam, quis nostrud 
											exercitation ullamco laboris nisi ut aliquip ex ea 
											commodo consequat.Duis aute irure dolor in 
											reprehenderit in voluptate velit esse cillum dolore 
											eu fugiat nulla pariatur. Excepteur sint occaecat 
											cupidatat non proident, sunt in culpa qui officia 
											deserunt mollit anim id est laborum.</p>
										<div class="rating">
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star.png" alt=" " class="img-responsive" />
											</div>
											<div class="clearfix"> </div>
										</div>
										<div class="modal_body_right_cart simpleCart_shelfItem">
											<p><span>$320</span> <i class="item_price">$250</i></p>
											<p><a class="item_add" href="#">Add to cart</a></p>
										</div>
										<h5>Color</h5>
										<div class="color-quality">
											<ul>
												<li><a href="#"><span></span>Red</a></li>
												<li><a href="#" class="brown"><span></span>Yellow</a></li>
												<li><a href="#" class="purple"><span></span>Purple</a></li>
												<li><a href="#" class="gray"><span></span>Violet</a></li>
											</ul>
										</div>
									</div>
									<div class="clearfix"> </div>
								</div>
							</section>
						</div>
					</div>
				</div>
				<div class="modal video-modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModal4">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
							</div>
							<section>
								<div class="modal-body">
									<div class="col-md-5 modal_body_left">
										<img src="/app/images/22.jpg" alt=" " class="img-responsive" />
									</div>
									<div class="col-md-7 modal_body_right">
										<h4>a good look women's Necklace</h4>
										<p>Ut enim ad minim veniam, quis nostrud 
											exercitation ullamco laboris nisi ut aliquip ex ea 
											commodo consequat.Duis aute irure dolor in 
											reprehenderit in voluptate velit esse cillum dolore 
											eu fugiat nulla pariatur. Excepteur sint occaecat 
											cupidatat non proident, sunt in culpa qui officia 
											deserunt mollit anim id est laborum.</p>
										<div class="rating">
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star.png" alt=" " class="img-responsive" />
											</div>
											<div class="clearfix"> </div>
										</div>
										<div class="modal_body_right_cart simpleCart_shelfItem">
											<p><span>$320</span> <i class="item_price">$250</i></p>
											<p><a class="item_add" href="#">Add to cart</a></p>
										</div>
										<h5>Color</h5>
										<div class="color-quality">
											<ul>
												<li><a href="#"><span></span>Red</a></li>
												<li><a href="#" class="brown"><span></span>Yellow</a></li>
												<li><a href="#" class="purple"><span></span>Purple</a></li>
												<li><a href="#" class="gray"><span></span>Violet</a></li>
											</ul>
										</div>
									</div>
									<div class="clearfix"> </div>
								</div>
							</section>
						</div>
					</div>
				</div>
				<div class="modal video-modal fade" id="myModal5" tabindex="-1" role="dialog" aria-labelledby="myModal5">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
							</div>
							<section>
								<div class="modal-body">
									<div class="col-md-5 modal_body_left">
										<img src="/app/images/35.jpg" alt=" " class="img-responsive" />
									</div>
									<div class="col-md-7 modal_body_right">
										<h4>a good look women's Jacket</h4>
										<p>Ut enim ad minim veniam, quis nostrud 
											exercitation ullamco laboris nisi ut aliquip ex ea 
											commodo consequat.Duis aute irure dolor in 
											reprehenderit in voluptate velit esse cillum dolore 
											eu fugiat nulla pariatur. Excepteur sint occaecat 
											cupidatat non proident, sunt in culpa qui officia 
											deserunt mollit anim id est laborum.</p>
										<div class="rating">
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star.png" alt=" " class="img-responsive" />
											</div>
											<div class="clearfix"> </div>
										</div>
										<div class="modal_body_right_cart simpleCart_shelfItem">
											<p><span>$320</span> <i class="item_price">$250</i></p>
											<p><a class="item_add" href="#">Add to cart</a></p>
										</div>
										<h5>Color</h5>
										<div class="color-quality">
											<ul>
												<li><a href="#"><span></span>Red</a></li>
												<li><a href="#" class="brown"><span></span>Yellow</a></li>
												<li><a href="#" class="purple"><span></span>Purple</a></li>
												<li><a href="#" class="gray"><span></span>Violet</a></li>
											</ul>
										</div>
									</div>
									<div class="clearfix"> </div>
								</div>
							</section>
						</div>
					</div>
				</div>
				<div class="modal video-modal fade" id="myModal6" tabindex="-1" role="dialog" aria-labelledby="myModal6">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
							</div>
							<section>
								<div class="modal-body">
									<div class="col-md-5 modal_body_left">
										<img src="/app/images/39.jpg" alt=" " class="img-responsive" />
									</div>
									<div class="col-md-7 modal_body_right">
										<h4>a good look women's Long Skirt</h4>
										<p>Ut enim ad minim veniam, quis nostrud 
											exercitation ullamco laboris nisi ut aliquip ex ea 
											commodo consequat.Duis aute irure dolor in 
											reprehenderit in voluptate velit esse cillum dolore 
											eu fugiat nulla pariatur. Excepteur sint occaecat 
											cupidatat non proident, sunt in culpa qui officia 
											deserunt mollit anim id est laborum.</p>
										<div class="rating">
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star-.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star.png" alt=" " class="img-responsive" />
											</div>
											<div class="rating-left">
												<img src="/app/images/star.png" alt=" " class="img-responsive" />
											</div>
											<div class="clearfix"> </div>
										</div>
										<div class="modal_body_right_cart simpleCart_shelfItem">
											<p><span>$320</span> <i class="item_price">$250</i></p>
											<p><a class="item_add" href="#">Add to cart</a></p>
										</div>
										<h5>Color</h5>
										<div class="color-quality">
											<ul>
												<li><a href="#"><span></span>Red</a></li>
												<li><a href="#" class="brown"><span></span>Yellow</a></li>
												<li><a href="#" class="purple"><span></span>Purple</a></li>
												<li><a href="#" class="gray"><span></span>Violet</a></li>
											</ul>
										</div>
									</div>
									<div class="clearfix"> </div>
								</div>
							</section>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>

<!-- new-products -->

	<div class="w3l_related_products">
		<div class="container">
			<h3 class="prod"><?= __('Productos Nuevos') ?></h3>
			<ul id="flexiselDemo2">
				<?php foreach ($products as $product): ?>
				<li>
					<div class="w3l_related_products_grid">
						<div class="agile_ecommerce_tab_left dresses_grid">
							<div class="hs-wrapper hs-wrapper3">
								<?php $display = "block"; ?>
								<?php foreach ($product->styles as $style): ?>
								 	<div class="section-images-stylesx" data-product-id="<?= $product->id; ?>" data-id="<?= $style->id; ?>" style="display: <?= $display; ?>">
										<?php $images = array(); $i = 0; $img = ''?>
										<?php foreach ($style->assets as $asset): ?>
											<?php if ($i == 0): ?>
												<?php $img = $asset->url; ?>
											<?php endif; $i++; ?>
										    <!-- imagen debe tener un width: 300 px - height: 400 px -->
											<?php array_push($images, '/app/images/' . $asset->url); ?>
										<?php endforeach; ?>
										<div class="images-rotation" data-images='["<?= implode("\", \"", $images); ?>"]'>
									    	<img style="cursor: pointer;" data-type='news' data-id="<?= $product->id ?>" src="/app/images/<?= $img ?>" alt="" class="btn-view-productx img-responsive">
									  	</div>
										<?php $display = "none"; ?>
									</div>
								<?php endforeach; ?>
							</div>
							<h5><a href="javascript:void(0)"><?= __($product->name) ?></a></h5>
							<div class="simpleCart_shelfItem">
								<p><i class="item_price">Gs.<?= $product->price ?></i></p>
							</div>
							<div class="row">
							  <?php $product_selected = 'product-selected' ?>
					          <?php foreach ($product->styles as $style): ?>
						          <div class="col-sm-2">
				            			<a class='img-stylex <?= $product_selected ?>' data-product-id="<?= $product->id ?>" data-id="<?= $style->id ?>" href="javascript:void(0)"><img src="/app/images/<?= $style->img_url ?>" class'img-responsive' style='border-radius: 30px; height: 30px; width: 30px;' alt="<?= $style->name ?>"></a>
						          </div>
						          <?php $product_selected = '' ?>
					          <?php endforeach; ?>
				       		</div>
						</div>
					</div>
				</li>
				<?php endforeach; ?>
			</ul>
			<script type="text/javascript">
				$(window).load(function() {
					$("#flexiselDemo2").flexisel({
						visibleItems:4,
						animationSpeed: 1000,
						autoPlay: true,
						autoPlaySpeed: 3000,    		
						pauseOnHover: true,
						enableResponsiveBreakpoints: true,
						responsiveBreakpoints: { 
							portrait: { 
								changePoint:480,
								visibleItems: 1
							}, 
							landscape: { 
								changePoint:640,
								visibleItems:2
							},
							tablet: { 
								changePoint:768,
								visibleItems: 3
							}
						}
					});

					$('.images-rotation').imagesRotation();

					$('.btn-view-productx').click(function() {
						var id = $(this).data('id');

						$.each (products, function(i, product) {
							if (id == product.id) {
								product_selected = product;
							}
						});

						var style_id;

						$(".section-images-stylesx[data-product-id='" + product_selected.id + "']").each(function(index, value) {
						    if ($(this).css("display") == "block") {
								style_id = $(this).data('id');
						    }
						});

						$.each (product_selected.styles, function(i, style) {
							if (style_id == style.id) {
								style_selected = style;
							}
						});

						stylesLoad();
						styleChanged(style_selected);
						
						$('#custom-description').html("");
						if (product_selected.custom_description) {
							$('#custom-description').append("<legend style='font-family: Glegoo, serif;font-size: 18px;'>Descripción</legend>");
							$('#custom-description').append(product_selected.custom_description);
						}
						
						$('.size-guide').attr('data-id', article_selected.id);

						//agrega el id del articulo al boton de favoritos
						$('.btn-favorite').data('id', article_selected.id);
						$('.btn-favorite img').attr('src', '/app/img/icon_wishlist.png');
						if (customer) {
							if (customer.hasOwnProperty("favorites")) {
								$.each (customer.favorites, function(i, favorite) {
									if (favorite.article.id == article_selected.id) {
										$('.btn-favorite img').attr('src', '/app/img/icon_wishlist_added.png');
									}
								});
							}
						}

						$('#quick-view-popup').modal('show');
					});

					//seleccion del estilo
					$('div').on("click", "a.img-stylex", function() {

						var style_id = $(this).data('id');
						var product_id = $(this).data('product-id');

						$(".section-images-stylesx[data-product-id='" + product_id +"']").each(function(index, value) {
						    var id = $(this).data('id');

						    if (id == style_id) {
						    	$("a.img-stylex[data-id='" + id +"']").addClass('product-selected');
								$(this).css('display', '');
						    } else {
						    	$("a.img-stylex[data-id='" + id +"']").removeClass('product-selected');
								$(this).css('display', 'none');
						    }
						});
					});

				});
			</script>
			<script type="text/javascript" src="/app/js/ecommerce/jquery.flexisel.js"></script>
		</div>
	</div>
<!-- //new-products -->

<!-- quick-view-popup -->
<div data-backdrop="true" data-keyboard="true" id="quick-view-popup" class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="dialog">
        <div class="modal-content">
            <div class="modal-body">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <br>
                  <br>
                  <div class="row">
                      <div class="section-assets-options col-md-1">
                      </div>
                      <div class="section-main-assets col-md-5">
                      </div>
                      <div class="simpleCart_shelfItem col-md-6">
                      	  <h4 style="font-size: 20px; color: #222; font-weight: bold; font-family: Arial,Helvetica,sans-serif; text-transform:capitalize;" id='lbl-title' id="gridSystemModalLabel"></h4>
                          <p>Gs.<i id="lbl-price" class="item_price"></i></p>

                          <h4><?= __('Color') ?></h4>
                          <div class="row">
                              <div class="section-styles">
                              </div>
                          </div>
                          <div style="position: relative;display: inline;">
                          	<h4 style="position: relative;display: inline;"><?= __('Talle') ?></h4>  <span data-id="" class="size-guide" style="position: relative;display: inline;border-bottom: 1px solid #ded5d5;cursor: pointer;color: #908d8d;"><?= __('Guia de Talles') ?></span>
                          </div>
                          <div class="row">
                              <div class="section-size col-md-12">
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-5">
                                  <h4><?= __('Cantidad') ?></h4>
                                  <div class="quantity"> 
										<div class="quantity-select">                           
											<div class="entry value-minus">&nbsp;</div>
											<div id ="article_amount" class="item_Quantity entry value"><span>1</span></div>
											<div class="entry value-plus active">&nbsp;</div>
										</div>
									</div>
                              </div>
                          	  <div style="margin-top: 20px" class="col-md-12">
									<p><a id="btn-add-to-cart" class="item_add" href="javascript:void(0)"><?= __('Agregar al carrito') ?></a></p>
							  </div>
							  <div data-id="" class="btn-favorite add_wishlist pull-left">
								<img class="iWish" src="/app/img/icon_wishlist.png"> <lable class="lbl-add-favorite"><?= __('Añadir lista de favoritos') ?></label>
							  </div>
							  <div style="display: block;padding: 15px;top: 20px;" id="custom-description">
							  </div>
                          </div>
                      </div>
                  </div>
            </div>
        </div>
    </div>
</div>
<!-- //quick-view-popup -->

<div id="size-image-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 style="padding-left: 12px;" class="modal-title">Guia de Talles</h4>
      </div>
      <div class="modal-body">
  		<img id="img-size-image" style="width:100%; height: auto;" src="" class="img-responsive" alt="">
  		<label id="lbl-size-image">Sin Imagen de Talles disponible</label>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
	var shopping_cart;
	var customer;
	var products;
	var product_selected;
	var style_selected;
	var main_asset;
	var article_selected;
	var seasons;

	//carga la imagen de la izquierda por defecto el primer Style
	var assetsOptionsLoad = function(style) {
	$('.section-assets-options').html('');
		$.each (style.assets, function(i, asset) {
			var img = "<a class='asset-option' data-id='"+asset.id+"' href='javascript:void(0)'><img src='/app/images/" + asset.url + "' class'img-responsive' style='width: 50px;' alt='Image'></a>";
			$('.section-assets-options').append(img).append('<br>').append('<br>');
		});
	}

	//cargar imagen principal
	var mainAssetLoad = function(asset) {
	$('.section-main-assets').html('');
		var img_main_asset = "<img style='max-width:100%; height: auto; display: block;' src='/app/images/" + asset.url + "' class='img-responsive' alt='Image'>"
		$('.section-main-assets').append(img_main_asset);
	}

	//carga de precio
	var articlePriceLoad = function(article) {
		$('#lbl-price').text(article.price);
	}

	//carga de estilos
	var stylesLoad = function() {
		$('.section-styles').html('');
		var id = style_selected.id;
		$.each (product_selected.styles, function (i, style) {
			var sel = (id == style.id) ? 'product-selected' : ''
			var img = "<div class='col-sm-1'><a class='" + sel + " img-style' data-id='" + style.id + "' href='javascript:void(0)'><img src='/app/images/" + style.img_url + "' class'img-responsive' style='border-radius: 30px; height: 30px; width: 30px;' alt='Image'></a></div>";
			$('.section-styles').append(img);
		});
	}

	//carga de talles
	var articleSizeLoad = function(style) {
		$('.section-size').html('');

		$.each (style.articles, function(i, article) {
			var btnSelection = 'btn-sizes';
			if (i == 0) {
				btnSelection = 'btn-sizes-selected';
				var article_name = (article_selected.name_display) ? article_selected.name_display : article_selected.description;
				$('#lbl-title').text(article_name);
			}

			var size = "<a data-id='" + article.id + "' style='margin-right: 4px;' type='button' class='btn-size " + btnSelection + "'>" + article.size + "</a>";
			$('.section-size').append(size);
		});
	}

	//funcion para cambiar el estilo
	var styleChanged = function (style) {
		assetsOptionsLoad(style);

		main_asset  = style.assets[0];
		mainAssetLoad(main_asset);

		article_selected = style.articles[0];
		articlePriceLoad(article_selected);

		articleSizeLoad(style);
	}

	//cambiar el talle y seleccionar articulo
	var sizeChanged = function(id) {
		$.each (style_selected.articles, function (i, article) {
			if (article.id == id) {
				article_selected = article;
				var article_name = (article_selected.name_display) ? article_selected.name_display : article_selected.description;
				$('#lbl-title').text(article_name);
			}
		});
	}

	$('.btn-favorite').click(function() {
		var img = $(this).find('img');
		var id = $(this).data('id');
		addToFavorites(id, img);
	});

	$(document).ready(function() {

		$('.images-rotation').imagesRotation();
		customer = <?= json_encode($customer) ?>;
		shopping_cart = <?= json_encode($shopping_cart) ?>;
		products = <?= json_encode($products); ?>;
		seasons = <?= json_encode($seasons); ?>;

		var total = 0;
		var amount = 0;
		if (shopping_cart) {

			$.each(shopping_cart, function (i, article) {
				total += parseFloat(article.price) * parseInt(article.amount);
				amount += parseInt(article.amount);
			});
		}

		$("#shopping-cart-quantity").text(amount);
		$("#shopping-cart-total").text('Gs.' + parseFloat(total));

		$('.btn-view-product').click(function() {
			var id = $(this).data('id');

			$.each (seasons, function(i, season) {
				$.each (season.products, function(i, product) {
					if (id == product.id) {
						product_selected = product;
					}
				});
			});

			var style_id;
			
			$(".section-images-styles[data-product-id='" + product_selected.id + "']").each(function(index, value) {
			    if ($(this).css("display") == "block") {
					style_id = $(this).data('id');
			    }
			});

			$.each (product_selected.styles, function(i, style) {
				if (style_id == style.id) {
					style_selected = style;
				}
			});

			stylesLoad();
			styleChanged(style_selected);

			$('#custom-description').html("");
			if (product_selected.custom_description) {
				$('#custom-description').append("<legend style='font-family: Glegoo, serif;font-size: 18px;'>Descripción</legend>");
				$('#custom-description').append(product_selected.custom_description);
			}

			//agrega el id del articulo al boton de favoritos
			$('.btn-favorite').data('id', article_selected.id);
			$('.btn-favorite img').attr('src', '/app/img/icon_wishlist.png');
			if (customer) {
				$.each (customer.favorites, function(i, favorite) {
					if (favorite.article.id == article_selected.id) {
						$('.btn-favorite img').attr('src', '/app/img/icon_wishlist_added.png');
					}
				});
			}

			$('#article_amount').text("1");
			$('#quick-view-popup').modal('show');
		});
		
		//seleccion del estilo
		$('div').on("click", "a.img-styles", function() {

			var style_id = $(this).data('id');
			var product_id = $(this).data('product-id');

			$(".section-images-styles[data-product-id='" + product_id +"']").each(function(index, value) {
			    var id = $(this).data('id');

			    if (id == style_id) {
			    	$("a.img-styles[data-id='" + id +"']").addClass('product-selected');
					$(this).css('display', '');
			    } else {
			    	$("a.img-styles[data-id='" + id +"']").removeClass('product-selected');
					$(this).css('display', 'none');
			    }
			});
		});

		//seleccion de las imagnes del costado si se hace click aparece como imagen principal
	    $('div').on("click", "a.asset-option", function() {
			var id = $(this).data('id');
			var asset;
			$.each (style_selected.assets, function(i, asst) {
				if (asst.id == id) {
					asset = asst;
				}
			});
			mainAssetLoad(asset);
		});

		//seleccion del estilo
	    $('div').on("click", "a.img-style", function() {
			var id = $(this).data('id');

			$.each (product_selected.styles, function(i, style) {
				if (style.id == id) {
				    $("a.img-style[data-id='" + style.id +"']").addClass('product-selected');
					style_selected = style;
					styleChanged(style_selected);
				} else {
					$("a.img-style[data-id='" + style.id +"']").removeClass('product-selected');
				}
			});
	    });

	    //cambia el estilo de la seleccion del boton de talles
	    $('div').on("click", "a.btn-size", function() {
			var id = $(this).data('id');
			$('.section-size a').each (function(index) {
				$(this).removeClass("btn-sizes-selected").addClass("btn-sizes");
				if (id == $(this).data('id')) {
					$(this).addClass("btn-sizes-selected");
					sizeChanged(id);
				}
			});
	    });

	    //quantity
		$('.value-plus').on('click', function(){
			var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) + 1;
			divUpd.text(newVal);
		});

		$('.value-minus').on('click', function(){
			var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) - 1;
			if(newVal >= 1) divUpd.text(newVal);
		});

		//agrega un articulo al carrito
		$('#btn-add-to-cart').click(function() {

			var amount = parseInt($("#article_amount").text());
			var id = article_selected.id;

			addToCart(id, amount);
		});

		$('.size-guide').click(function() {
			if ( article_selected.size_image) {
				$('#lbl-size-image').hide();
				$("#img-size-image").show();
				$("#img-size-image").attr("src", "/app/images/" + article_selected.size_image);
			} else {
				$("#img-size-image").hide();
				$('#lbl-size-image').show();
			}

			$('#size-image-modal').modal('show');
		});
	});
</script>
