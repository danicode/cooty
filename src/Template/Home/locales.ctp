<style type="text/css">
    
    .panel-body {
    	padding: 0;
    }
    
    .store-data {
	    font-family: monospace;
	    font-size: 14px;
	    color: #544b4b;
	    margin-top: 15px;
	    margin-bottom: 25px;
    }

    .modal-title {
    	padding-left: 0;
    }

</style>

<!-- banner -->
<!--<?php foreach ($coversPages as $coversPage): ?>-->
<!--	<?php if ($coversPage->principal): ?>-->
<!--	<div style="background:url(/app/images/<?= $coversPage->url ?>) no-repeat 0px 0px;" class="banner10" id="home1">-->
<!--	<?php endif; ?>-->
<!--<?php endforeach; ?>-->
<!--		<div class="container">-->
<!--			<h2><?= __('Locales') ?></h2>-->
<!--		</div>-->
<!--	</div>-->
<!-- //banner -->
<div class="container text-center">
	<h2><?= __('Locales') ?></h2>
</div>

<!-- //banner-bottom -->
<!--<div id="myCarousel" class="carousel slide" data-ride="carousel">-->
  <!-- Indicators -->
<!--  <ol class="carousel-indicators">-->
<!--  	<?php foreach ($promotions as $key => $promotion): ?>-->
<!--    	<li data-target="#myCarousel" data-slide-to="<?= $key ?>" class="<?= ($key == 0) ? 'active' : '' ?>"></li>-->
<!--    <?php endforeach; ?>-->
<!--  </ol>-->

  <!-- Wrapper for slides -->
<!--  <div class="carousel-inner">-->
<!--  	<?php foreach ($promotions as $key => $promotion): ?>-->
<!--	    <div class="item <?= ($key == 0) ? 'active' : '' ?>">-->
<!--	      <a href="/app/catalogo/index"><img src="/app/images/<?= $promotion->promotions_assets[0]->url ?>" alt="" style="width: 100%;"></a>-->
<!--	    </div>-->
<!--    <?php endforeach; ?>-->
<!--  </div>-->

  <!-- Left and right controls -->
<!--  <a class="left carousel-control" href="#myCarousel" data-slide="prev">-->
<!--    <span class="glyphicon glyphicon-chevron-left"></span>-->
<!--    <span class="sr-only"><?= __('Anterior') ?></span>-->
<!--  </a>-->
<!--  <a class="right carousel-control" href="#myCarousel" data-slide="next">-->
<!--    <span class="glyphicon glyphicon-chevron-right"></span>-->
<!--    <span class="sr-only"><?= __('Siguiente') ?></span>-->
<!--  </a>-->
<!--</div>-->

<!-- breadcrumbs -->
	<div class="breadcrumb_dress">
		<div class="container">
			<ul>
				<li><a href="/app/home/index"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> <?= __('Inicio') ?></a> <i>/</i></li>
				<li><?= __('Locales') ?></li>
			</ul>
		</div>
	</div>
<!-- //breadcrumbs -->

<!-- locales -->
	<?php $i = 3; ?>
	<?php $j = 1; ?>
	<?php $z = 1; ?>
	<?php foreach ($sucursales as $sucursal): ?>
		<?php if ($i % 3 == 0): ?>
		<div style="margin-top: 25px;" class="container">
			<div class="row">
		<?php endif;?>  

		    <div class="col-sm-4">
		      <div style="cursor: pointer;" data-id="<?= $sucursal->id ?>" class="sucursal-item">
		      	<div style="height: 56px;" class=""><?= h($sucursal->name) ?></div>
		        <div style="position: relative; display: block;"><img style="height: auto;max-width: 300px;position: relative;display: inline-block;" src="/app/images/<?= $sucursal->url ?>" class="img-responsive" style="width:100%" alt="Image"></div>
		        <div style="" class="row store-data">
		        	<div class="col-md-12">
	        			<i class="fa fa-map-marker" aria-hidden="true"></i> <span> <?= $sucursal->address ?></span>
	        		</div>
	        		<div class="col-md-12">
		        		<i class="fa fa-phone" aria-hidden="true"></i> <span><?= ($sucursal->phone) ? $sucursal->phone : 'Muy pronto' ?></span>
		        	</div>
		        	<div class="col-md-12">
		        		<i class="fa fa-clock-o" aria-hidden="true"></i> <span><?= $sucursal->open_hours ?></span>
		        	</div>
	        	</div>
		      </div>
		    </div>

		<?php if ($j == 3 || $sucursales->count() == $z): ?>
		<?php $j = 0; ?>
		</div>
		</div>
		<?php endif;?>
		<?php $j++; ?>
		<?php $z++; ?>
	<?php $i++ ?>
	<!--<div style="display: none;" id="hidden-content-<?= $store->id ?>">-->
	<!--	<?php if ($store->stores_assets): ?>-->
	<!--		<?php foreach ($store->stores_assets as $sa): ?>-->
	<!--		    <a href="/app/images/<?= $sa->url ?>" data-fancybox="group<?= $store->id ?>" data-caption="">-->
	<!--				<img style="max-width: 200px;" src="/app/images/<?= $sa->url ?>" alt="" />-->
	<!--			</a>-->
	<!--		<?php endforeach; ?>-->
	<!--	<?php else: ?>-->
	<!--		<p style="margin: 12px; font-size: 20px; font-family: serif; color: #767471;">Muy pronto podrá ver las imagenes del local.</p>-->
	<!--	<?php endif;?>-->
	<!--</div>-->
	<?php endforeach; ?>
<!-- //locales -->

<div id="sucursal-map-popup" class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="lbl-title-suscursal-map"></h4>
      </div>
      <div class="modal-body">
        <div id="container-map" class="row">
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	var shopping_cart;
	var customer;
	var sucursales;

	$(document).ready(function() {

		customer = <?= json_encode($customer) ?>;
		shopping_cart = <?= json_encode($shopping_cart) ?>;
		sucursales = <?= json_encode($sucursales) ?>;

		var total = 0;
		var amount = 0;
		if (shopping_cart) {

			$.each(shopping_cart, function (i, article) {
				total += parseFloat(article.price) * parseInt(article.amount);
				amount += parseInt(article.amount);
			});
		}

		$("#shopping-cart-quantity").text(amount);
		$("#shopping-cart-total").text('Gs.' + parseFloat(total));

		$('.sucursal-item').click(function() {
			var id = $(this).data('id');
			var sucursal;
			$.each(sucursales, function(index, sucu) {
				if (sucu.id == id) {
					sucursal = sucu;
				}
			});
			$('#lbl-title-suscursal-map').text(sucursal.name);
			$('#container-map').html("");
			$('#container-map').append(sucursal.map);
			$('#sucursal-map-popup').modal('show');
		});
		
	});
</script>
