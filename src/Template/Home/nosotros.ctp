<!-- banner -->
<!--<?php foreach ($coversPages as $coversPage): ?>-->
<!--	<?php if ($coversPage->principal): ?>-->
<!--	<div style="background:url(/app/images/<?= $coversPage->url ?>) no-repeat 0px 0px;" class="banner10" id="home1">-->
<!--	<?php endif; ?>-->
<!--<?php endforeach; ?>-->

<!-- //banner-bottom -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
  	<?php foreach ($promotions as $key => $promotion): ?>
    	<li data-target="#myCarousel" data-slide-to="<?= $key ?>" class="<?= ($key == 0) ? 'active' : '' ?>"></li>
    <?php endforeach; ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
  	<?php foreach ($promotions as $key => $promotion): ?>
	    <div class="item <?= ($key == 0) ? 'active' : '' ?>">
	      <a href="/app/catalogo/index"><img src="/app/images/<?= $promotion->promotions_assets[0]->url ?>" alt="" style="width: 100%;"></a>
	    </div>
    <?php endforeach; ?>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only"><?= __('Anterior') ?></span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only"><?= __('Siguiente') ?></span>
  </a>
</div>

		<!--<div class="container">-->
		<!--	<h2><?= __('Nosotros') ?></h2>-->
		<!--</div>-->
	<!--</div>-->
<!-- //banner -->

<!-- breadcrumbs -->
	<div class="breadcrumb_dress">
		<div class="container">
			<ul>
				<li><a href="/app/home/index"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> <?= __('Inicio') ?></a> <i>/</i></li>
				<li><?= __('Nosotos') ?></li>
			</ul>
		</div>
	</div>
<!-- //breadcrumbs -->

<!-- about -->
	<div class="about">
		<div class="container">	
			<div class="w3ls_about_grids">
				<div class="col-md-6 w3ls_about_grid_left">
					<p><?= __('En tan bonita ciudad de Barcelona estudiaba Margarita, quien realizando sus sueños atraveasó el Atlántico para asistir al instituto Técnico de Enseñanza.') ?></p>
					<div class="col-xs-2 w3ls_about_grid_left1">
						<span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>
					</div>
					<div class="col-xs-10 w3ls_about_grid_left2">
						<p><?= __('Comenzaba la década del 90, cuando al graduarse en Diseño de Modas, decide junto a una de sus compañeras crear una marca de ropa para bebes y niños.
Con sus pequeñas hijas como inspiración fueron surgiendo hermosos diseños de estilo español clásico pero innovador. Sus confecciones comenzaron a venderse por toda España. Llegando las prendas de Cootchy Coo a los escaparates de las tiendas más prestigiosas del país.') ?></p>
					</div>
					<div class="clearfix"> </div>
					<div class="col-xs-2 w3ls_about_grid_left1">
						<span class="glyphicon glyphicon-flash" aria-hidden="true"></span>
					</div>
					<div class="col-xs-10 w3ls_about_grid_left2">
						<p><?= __('En el 93 Margarita retorna a su país de origen donde continua con la exitosa empresa junto a su familia.
Actualmente COOTCHY COO goza con gran éxito en Paraguay donde cuenta con 5 tiendas en la capital y presencia en tiendas de ciudades del interior del país.') ?></p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-6 w3ls_about_grid_right">
					<?php $flag = true; ?>
					<?php foreach ($coversPages as $coversPage): ?>
						<?php if ($coversPage->nosotros && $flag): ?>
							<?php $flag = false; ?>
							<img src="/app/images/<?= $coversPage->url ?>" alt=" " class="img-responsive" />
						<?php endif; ?>
					<?php endforeach; ?>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //about -->

<script type="text/javascript">
	var shopping_cart;
	var customer;

	$(document).ready(function() {

		shopping_cart = <?= json_encode($shopping_cart) ?>;
		customer = <?= json_encode($customer) ?>;
		var total = 0;
		var amount = 0;
		if (shopping_cart) {

			$.each(shopping_cart, function (i, article) {
				total += parseFloat(article.price) * parseInt(article.amount);
				amount += parseInt(article.amount);
			});
		}

		$("#shopping-cart-quantity").text(amount);
		$("#shopping-cart-total").text('Gs.' + parseFloat(total));
	});

</script>
