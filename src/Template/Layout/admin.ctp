<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Cootchycoo | ' . $version;
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="logo-icon.ico" type="image/x-icon">

    <?php echo $this->Html->meta(
      'logo-icon.ico',
      '/logo-icon.ico',
      array('type' => 'icon')
      );
    ?>
    <title>
      <?= $cakeDescription ?>
    </title>

    <?= $this->Html->css([
      'common/font-awesome/css/font-awesome.min',
      'common/bootstrap/bootstrap.min',
      'common/noty/animate',
      'common/noty/font-awesome.min',
      'common/dataTables/datatables.min',
      'common/jquery-ui.custom/jquery-ui.min',
      'common/google-font',
      'admin/icomoon/style',
      'admin/multilevelpushmenu/jquery.multilevelpushmenu',
      'admin/multilevelpushmenu/responsive',
      'admin/main',
      'admin/common',
      'admin/jquery-te-1.4.0'
    ]) ?>

    <?= $this->Html->script([
      'common/jquery/jquery.min',
      'common/bootstrap/bootstrap',
      'common/bootbox/bootbox.min',
      'common/noty/packaged/jquery.noty.packaged.min',
      'common/dataTables/datatables.min',
      'common/dataTables/sort',
      'common/jquery-ui.custom/jquery-ui.min',
      'common/tableExpor/jquery.base64',
      'common/tableExpor/tableExport',
      'admin/multilevelpushmenu/jquery.multilevelpushmenu',
      'admin/jquery-te-1.4.0.min'
    ]) ?>

    <script type="text/javascript">

    var menuesTitles = {
      Users: { login: 'Login', index: 'Usuarios', add: 'Agregar Usuarios', edit: 'Editar Usuario', dashboard: 'Tablero' },
      Articles: { index: 'Articulos', edit: 'Editar Articulo', editNameDisplay: 'Editar Nombre a Mostrar' },
      Stores: { index: 'Depositos', view: 'Deposito', edit: 'Editar Deposito' },
      SyncsLogs: { index: 'Logs' },
      Products: { index: 'Productos', add: 'Agregar Producto', edit: 'Editar Producto' },
      Categories: { index: 'Categorias', add: 'Agregar Categoria', edit: 'Editar Categoria' },
      Seasons: { index: 'Temporadas', add: 'Agregar Temporada', edit: 'Editar Temporada' },
      Deliveries: { index: 'Repartidores', add: 'Agregar Repartidor', edit: 'Editar Repartidor' },
      Promotions: { index: 'Banners', add: 'Agregar Banners', edit: 'Editar Banners' },
      CoversPages: { index: 'Imagenes', add: 'Agregar Imagenes', edit: 'Editar Imagenes' },
      Orders: { index: 'Pedidos' },
      Groups: { index: 'Grupos', add: 'Agregar Grupo', edit: 'Editar Grupo' },
      Sucursales: { index: 'Sucursales', add: 'Agregar Sucursal', edit: 'Editar Sucursal' },
    };

    var arrMenu;
    var loggedIn = <?= $loggedIn; ?>;
    var userAdmin = <?= $userAdmin; ?>;

    $(document).ready(function () {

      var desc = '<?=$cakeDescription?>';

      $('.dropdown-toggle').dropdown();
      $('#userlogin').dropdown('toggle');

      var current_controller = $('#current_controller').val();
      var current_action = $('#current_action').val();
      var text = menuesTitles[current_controller][current_action];
      $('#head-title').html(text);
      document.title = text  + ' | ' + desc;
    });

    /**
     * notificaciones 
     * type: warning, error, information, success
     * text: html
    */
    function generateNoty(type, text) {

      var n = noty({
        text        : text,
        type        : type,
        dismissQueue: true,
        layout      : 'topRight',
        closeWith   : ['click'], /*['click', 'button', 'hover', 'backdrop']*/
        theme       : 'relax',
        maxVisible  : 5,
        animation   : {
            open  : 'animated bounceInRight',
            close : 'animated bounceOutRight',
            easing: 'swing',
            speed : 500
        }
      });
    }

   /**
    * confirmacion de eliminacion general 
    */
    $(document).on("click", ".btn-delete", function(e) {
        
        var text = $(this).data('text');
        var controller  = $(this).data('controller');
        var id  = $(this).data('id');
        
        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/app/'+controller+'/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"})))
                .find('#replacer').submit();
            }
        });
    });

    window.onload = function(e) {
      $(".se-pre-con").fadeOut("slow");
    }

    window.onbeforeunload = function(e) {
      $(".se-pre-con").show();
    }

    if (loggedIn) {
      arrMenu = [
      {
        title: 'Cootchycoo',
        id: 'menuID',
        icon: 'fa fa-reorder',

        items: []
      }];

      if (userAdmin) {
        arrMenu[0].items = [
          {
            name: 'Tablero',
            icon: 'fa fa-tachometer',
            link: '/app/users/dashboard',
            id: 'home'
          },
          {
            name: 'Pedidos',
            icon: 'fa fa-list-alt',
            link: '/app/orders/index',
            id: 'orders-index'
          },
          {
            name: 'Usuarios',
            icon: 'fa fa-users',
            link: '#',
            id: 'users',
            
            items: [
              {
                title: 'Usuarios',
                icon: 'fa fa-users',
                items: [
                  {
                    name: 'Agregar Nuevo',
                    link: '/app/users/add',
                    icon: 'fa fa-user-plus',
                    id: 'users-add'
                  },
                  {
                    name: 'Ver Lista',
                    link: '/app/users/index',
                    icon: 'fa fa-table',
                    id: 'users-index'
                  },
                ]
              }
            ]
          },
          {
            name: 'Productos',
            icon: 'fa fa-product-hunt',
            link: '#',
            id: 'products',
            
            items: [
              {
                title: 'Productos',
                icon: 'fa fa-cube',
                items: [
                  {
                    name: 'Agregar Nuevo',
                    link: '/app/products/add',
                    icon: 'fa fa-plus-circle',
                    id: 'products-add'
                  },
                  {
                    name: 'Ver Lista',
                    link: '/app/products/index',
                    icon: 'fa fa-list-alt',
                    id: 'products-index'
                  },
                  {
                    name: 'Temporadas',
                    link: '/app/seasons/index',
                    icon: 'fa fa-calendar',
                    id: 'seasons-index'
                  },
                ]
              }
            ]
          },
          {
            name: 'Articulos',
            icon: 'fa fa-shopping-basket',
            link: '/app/articles/index',
            id: 'articles-index',
          },
          {
            name: 'Depositos',
            icon: 'fa fa-cloud',
            link: '/app/stores/index',
            id: 'stores-index',
          },
          {
            name: 'Sucursales',
            icon: 'fa fa-fort-awesome',
            link: '/app/sucursales/index',
            id: 'locations-index',
          },
          {
            name: 'Banners',
            icon: 'fa fa-bullhorn',
            link: '/app/promotions/index',
            id: 'sucursales-index',
          },
          {
            name: 'Repartidor',
            icon: 'fa fa-motorcycle',
            link: '/app/deliveries/index',
            id: 'deliveries-index',
          },
          {
            name: 'Logs',
            icon: 'fa fa-file-text-o',
            link: '/app/syncs-logs/index',
            id: 'syncs-logs-index',
          },
          {
            name: 'Otras Configuraciones',
            icon: 'fa fa-cube',
            link: '#',
            id: 'MainPage',
  
            items: [
              {
                title: 'Otras Configuraciones',
                icon: 'fa fa-cube',
                items: [
                  {
                    name: 'Imagenes',
                    link: '/app/covers-pages/index',
                    icon: 'fa fa-picture-o',
                    id: 'covers-pages-index'
                  },
                   {
                    name: 'Grupos',
                    link: '/app/groups/index',
                    icon: 'fa fa-object-group',
                    id: 'groups-index'
                  },
                  {
                    name: 'Categorias',
                    link: '/app/categories/index',
                    icon: 'fa fa-sitemap',
                    id: 'categories-index'
                  },
                 
                ]
              }
            ]
          },
          {
            name: 'Cerrar Sesión',
            icon: 'fa fa-sign-out',
            link: '/app/users/logout',
          },
        ]
      } else {
        arrMenu[0].items = [
          {
            name: 'Tablero',
            icon: 'fa fa-tachometer',
            link: '/app/users/dashboard',
            id: 'home'
          },
          {
            name: 'Pedidos',
            icon: 'fa fa-list-alt',
            link: '/app/orders/index',
            id: 'orders-index'
          },
          {
            name: 'Articulos',
            icon: 'fa fa-shopping-basket',
            link: '/app/articles/index',
            id: 'articles-index',
          },
          {
            name: 'Depositos',
            icon: 'fa fa-fort-awesome',
            link: '/app/stores/index',
            id: 'stores-index',
          },
          {
            name: 'Sucursales',
            icon: 'fa fa-fort-awesome',
            link: '/app/sucursales/index',
            id: 'sucursales-index',
          },
          {
            name: 'Cerrar Sesión',
            icon: 'fa fa-sign-out',
            link: '/app/users/logout',
          },
        ]
      }
    } else {
      arrMenu = [];
    }

    $(document).ready(function() {

      $('.form-load').submit(function() {
          $('.form-load .btn').hide(); 
      });

    	$('.btn-back-page').click(function() {
    		parent.history.back();
    		return false;
    	});

    	var menuWidth = '13%';

    	if (screen.width < 500 ||
    	 navigator.userAgent.match(/Android/i) ||
    	 navigator.userAgent.match(/webOS/i) ||
    	 navigator.userAgent.match(/iPhone/i) ||
    	 navigator.userAgent.match(/iPod/i)) {

    		menuWidth = '50%';
    	}
    	else if (screen.width < 1200 ||
    	 navigator.userAgent.match(/Android/i) ||
    	 navigator.userAgent.match(/webOS/i) ||
    	 navigator.userAgent.match(/iPhone/i) ||
    	 navigator.userAgent.match(/iPod/i)) {

  		 menuWidth = '20%';
    	}

      var onExpandMenu = false;
      var currentlevelMenuExpand = -1;

    	$('#menu').multilevelpushmenu({
    		containersToPush: [$( '.container-fuild' )],
    		menuWidth: menuWidth,
    		menuHeight: '100%',
    		collapsed: true, 
    		backText: 'Atras',
    		menu: arrMenu,
    		preventItemClick: true,                                
        preventGroupItemClick: true, 
        onItemClick: function() {
            var event = arguments[0], $menuLevelHolder = arguments[1], $item = arguments[2], options = arguments[3];
            var itemHref = $item.find( 'a:first' ).attr('href');
             if (itemHref.match(/logout/i)) {
              bootbox.confirm('Salir del sistema ?', function(result) {
                if (result) {
                    window.open(itemHref, '_self');
                  }
              }).find("div.modal-content").addClass("confirm-width-sm");
            }
            else {
              location.href = itemHref;
            }
        },

        onExpandMenuStart: function(event) {

          if (!onExpandMenu) {
            onExpandMenu = true;
          }
          currentlevelMenuExpand++;
        },
        onCollapseMenuStart: function() {
          currentlevelMenuExpand--;
          if (currentlevelMenuExpand == -1) {
            onExpandMenu = false;
          }
        },
    	});

    	$(document).keydown(function(e) {

            if (e.keyCode == 220) { //tecla arriba del tab
              if (onExpandMenu) {
                $('#menu').multilevelpushmenu('collapse');
                onExpandMenu = false;
              } else {
                onExpandMenu = true;
                $('#menu').multilevelpushmenu('expand');
              }
            }

            if (e.keyCode == 27) { //tecla ESC

              if (onExpandMenu) {

                if (currentlevelMenuExpand == 0) {
                  $('#menu').multilevelpushmenu('collapse');
                }
                else if (currentlevelMenuExpand > 0) {
                  $('#menu').multilevelpushmenu('collapse', currentlevelMenuExpand - 1);
                }
              }
            }
      });
    });

    $(window).resize(function () {
    	$('#menu').multilevelpushmenu('redraw');
    });
  </script>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

</head>
<body  id="page-top">

  <div class="se-pre-con"></div>

    <form>
      <input type="hidden" name="current_controller" id="current_controller" value="<?=$current_controller?>"  />
      <input type="hidden" name="current_action" id="current_action" value="<?=$current_action?>"  />
    </form>

    <?= $this->Flash->render() ?>
    <?= $this->Flash->render('auth') ?>

<?php if ($loggedIn): ?>

    <div class="container-fluid">

        <div class="row header-title">

            <div class="col-md-12">
                <span id="head-title"></span>
                <div class="pull-right">
                 
                </div>
            </div>
        </div>

    <?php endif;?>

        <?= $this->fetch('content') ?>

    <?php if ($loggedIn): ?>

        <div id="menu" class="fixed"></div>

    <?php endif;?>

    </div>
  <footer>
  </footer>
</body>
</html>
