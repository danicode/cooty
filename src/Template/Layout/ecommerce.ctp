<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Cootchycoo | '.$version;
?>
<!DOCTYPE html>
<html>
  <head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="logo-icon.ico" type="image/x-icon">

    <?php echo $this->Html->meta(
      'logo-icon.ico',
      '/logo-icon.ico',
      array('type' => 'icon')
      );
    ?>
    <title>
      <?= $cakeDescription ?>
    </title>

    <?= $this->Html->css([
      'common/font-awesome/css/font-awesome.min',
      'common/bootstrap/bootstrap.min',
      // 'common/noty/animate',
      // 'common/noty/font-awesome.min',
      'common/noty2/noty',
      'common/noty2/animate',
      'common/dataTables/datatables.min',
      'common/jquery-ui.custom/jquery-ui.min',
      'common/google-font',
      'ecommerce/fasthover',
      'ecommerce/flexslider',
      'ecommerce/jquery.countdown',
      'ecommerce/popuo-box',
      'ecommerce/style',
      'ecommerce/jquery.fancybox',
    ]) ?>

    <?= $this->Html->script([
      'ecommerce/jquery.min',
      'ecommerce/bootstrap-3.1.1.min',
      'common/bootbox/bootbox.min',
      'ecommerce/easyResponsiveTabs',
      'ecommerce/imagezoom',
      'ecommerce/jquery.countdown',
      'ecommerce/jquery.flexisel',
      'ecommerce/jquery.flexslider',
      'ecommerce/jquery.magnific-popup',
      'ecommerce/jquery.wmuSlider',
      'ecommerce/jquery.images-rotation',
      'ecommerce/script',
      'ecommerce/jquery.fancybox',
      // 'common/noty/packaged/jquery.noty.packaged.min',
      'common/noty2/noty',
    ]) ?>

  <script type="text/javascript">
   /** 
    * notificaciones 
    * type: warning, error, information, success
    * text: html
    */
    function generateNoty(type, text) {

      // var n = noty({
      //   text        : text,
      //   type        : type,
      //   dismissQueue: true,
      //   layout      : 'topRight',
      //   closeWith   : ['click'], /*['click', 'button', 'hover', 'backdrop']*/
      //   theme       : 'relax',
      //   maxVisible  : 5,
      //   animation   : {
      //       open  : 'animated bounceInRight',
      //       close : 'animated bounceOutRight',
      //       easing: 'swing',
      //       speed : 500
      //   }
      // });
      var n = new Noty({
        text        : text,
        type        : type,
        layout   : 'center',
        theme    : 'mint',
        closeWith: ['click'],
        animation: {
            open : 'animated fadeInRight',
            close: 'animated fadeOutRight'
        }
      }).show();
    };

    window.onload = function(e) {
      $(".se-pre-con").fadeOut("slow");
    }

    window.onbeforeunload = function(e) {
      $(".se-pre-con").show();
    };
  </script>
  <?= $this->fetch('meta') ?>
  <?= $this->fetch('css') ?>
  <link href='//fonts.googleapis.com/css?family=Glegoo:400,700' rel='stylesheet' type='text/css'>
  <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
  <?= $this->fetch('script') ?>

  </head>
  
  <div class="modal fade" id="myModal88" tabindex="-1" role="dialog" aria-labelledby="myModal88"
  		aria-hidden="true">
  		<div class="modal-dialog modal-md">
  			<div class="modal-content">
  				<div class="modal-header">
  					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
  						&times;</button>
  						<h3 style="color: #948989;" id="title-account-popup" class="modal-title" id="gridSystemModalLabel"></h3>
  				</div>
  				<div class="modal-body modal-body-sub">
  					<div class="row">
  						<div class="col-md-12 modal_body_left modal_body_left1">
  							<div class="sap_tabs">
  								<div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
  									<ul>
  										<li id="tab-login" class="resp-tab-item" aria-controls="tab_item-0"><span><?= __('Iniciar Sesión') ?></span></li>
  										<li id="tab-register" class="resp-tab-item" aria-controls="tab_item-1"><span><?= __('Registrarse') ?></span></li>
  										<li id="tab-no-login" class="resp-tab-item" aria-controls="tab_item-2"><span id="btn-continue"><?= __('Continuar compra sin registrarse') ?></span></li>
  									</ul>

  									<div id="content-login" class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
  										<div class="facts">
  											<div class="register login ">
  												<?= $this->Form->create(null, ['id' => 'login-form']) ?>
                          <?= $this->Form->input('', ['label' => __('Correo'), 'required' => true, 'type' => 'email', 'name' => 'email', 'id' => 'email']) ?>
                          <?= $this->Form->input('', ['label' => __('Clave'), 'required' => true, 'type' => 'password', 'name' => 'password', 'id' => 'password']) ?>
                          <div class="sign-up">
                            <?= $this->Form->input(__('Entrar'), ['id' => 'btn-login', 'type' => 'submit']) ?>
                            <a id="forget-password" href="javascript:void(0)" onmouseover="this.style.color='#ff9b05';" onmouseout="this.style.color='#212121';" style="color: #212121; cursor: pointer; font-family: monospace; font-size: 15px;"><?= __('Ha olvidado su Clave') ?></a>
                          </div>
                          <?= $this->Form->end() ?>
  											</div>
  										</div>
  									</div>

  									<div id="content-register" class="tab-2 resp-tab-content" aria-labelledby="tab_item-1">
  										<div class="facts">
  											<div class="register registerx">
  											  <?= $this->Form->create(null, ['id' => 'register-form', 'data-toggle' => "validator"]) ?>
  											  <?= $this->Form->input('', ['label' => __('Nombre'), 'required' => true, 'type' => 'text', 'name' => 'name', 'id' => 'name']) ?>
                          <?= $this->Form->input('', ['label' => __('Correo'), 'required' => true, 'type' => 'email', 'name' => 'email', 'id' => 'email']) ?>
                          <?= $this->Form->input('', ['label' => __('Domicilio'), 'required' => true, 'type' => 'text', 'name' => 'address', 'id' => 'address']); ?>
                          <?= $this->Form->input('', ['label' => __('Teléfono'), 'required' => true, 'type' => 'text', 'name' => 'phone', 'id' => 'phone']); ?>
                          <?= $this->Form->input('', ['label' => __('Clave'), 'required' => true, 'type' => 'password', 'name' => 'password', 'id' => 'password']) ?>
                          <?= $this->Form->input('', ['label' => __('Repita Clave'), 'required' => true, 'type' => 'password', 'name' => 're-password', 'id' => 're-password']) ?>
                          <div class="sign-up">
                            <?= $this->Form->input(__('Guardar'), ['id' => 'btn-register', 'type' => 'submit']) ?>
                          </div>
                          <?= $this->Form->end() ?>
  											</div>
  										</div>
  									</div>

  									<div id="content-no-login" class="tab-3 resp-tab-content" aria-labelledby="tab_item-2">
  										<div class="facts">
  											<div class="register continue">
  											</div>
  										</div>
  									</div>

  								</div>
  							</div>
  							<script src="/app/js/ecommerce/easyResponsiveTabs.js" type="text/javascript"></script>
  							<script type="text/javascript">
  								$(document).ready(function () {
  									$('#horizontalTab').easyResponsiveTabs({
  										type: 'default', //Types: default, vertical, accordion           
  										width: 'auto', //auto or any width like 600px
  										fit: true   // 100% fit in a container
  									});
  								});
  							</script>
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>
  	</div>

  	<div id="user-logged-popup" class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="row">
                <div class="col-md-12">
                    <center>
                        <a class="btn btn-primary btn-block" id="btn-edit" href="/app/mi-cuenta/edit/<?= isset($customer) ? $customer->id : '' ?>" role="button">
                            <span class="glyphicon glyphicon-user"  aria-hidden="true"></span>
                            <?= __('Perfil') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </a>
                        <a class="btn btn-danger btn-block" href="/app/mi-cuenta/favoritos" role="button">
                            <span class="glyphicon glyphicon-star"  aria-hidden="true"></span>
                            <?= __('Favoritos') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </a>
                        <a id="btn-logout" class="btn btn-warning btn-block" href="#" role="button">
                            <span class="glyphicon glyphicon-log-out"  aria-hidden="true"></span>
                            <?= __('Cerrar Sesisón') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </a>
                    </center>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="forget-password-popup" class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="row">
                <div class="col-md-12">

                  <div class="register forget">
										<?= $this->Form->create(null, ['url' => ['controller' => 'mi-cuenta', 'action' => 'recuperar-clave']]) ?>
                    <?= $this->Form->input('', ['label' => __('Correo'), 'required' => true, 'type' => 'email', 'name' => 'email', 'id' => 'email']) ?>
                    <?= $this->Form->hidden('controller', ['value'=>$current_controller]); ?>
                    <?= $this->Form->hidden('action', ['value'=>$current_action]); ?>
                    <div class="sign-up">
                      <?= $this->Form->input(__('Entrar'), ['id' => 'btn-forget', 'type' => 'submit']) ?>
                    </div>
                    <?= $this->Form->end() ?>
									</div>

                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- popup confirmacion para vaciar carrito -->
    <div class="modal fade" id="confirm-empty-cart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><?= __('Confirmación para vaciar carrito') ?></h4>
                </div>
                <div class="modal-body">
                    <p><?= __('Se vaciara el carrito y no podrá recuperar los articulos guardados.') ?></p>
                    <p><?= __('Esta Seguro de vaciar el carrito ?') ?></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Cancelar') ?></button>
                    <a id="btn-confirm-empty-cart" class="btn btn-danger btn-ok"><?= __('Aceptar') ?></a>
                </div>
            </div>
        </div>
    </div>
    <!-- // -->

    <!-- popup mensaje -->
    <div id="messenger-popup" class="modal fade" role="dialog">
      <div class="modal-dialog modal-sm">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <p id="message-popup"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
    
      </div>
    </div>
  
  <body>
    <div class="se-pre-con"></div>

    <?= $this->Flash->render('auth') ?>

    <!-- header -->
  	<div class="header" id="top">
  		<div class="container">
  			<div class="w3l_login">
  				<a id="btn-user" href="#" data-toggle="modal"><span class="glyphicon glyphicon-user" aria-hidden="true"></span><span class="lbl-session-register" style="display: <?= ($customer) ? 'none' : 'block'  ?>">Iniciar Sesión/Registrase</span></a>
			    <label id="lbl-username"></label>
  			</div>
  			<div class="w3l_logo">
  				<h1><a href="index.html"><?= $this->Html->image("logo.png", array(
              "alt" => "Cootchy Coo",
              'url' => array('controller' => 'home', 'action' => 'index')
          )); ?></a></h1>
  			</div>
  			<div class="search">
  				<input class="search_box" type="checkbox" id="search_box">
  				<label class="icon-search" for="search_box"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></label>
  				<div class="search_form">
  					<form action="/app/catalogo/buscador" method="post">
  					  <input autofocus="autofocus" id="search" required type="text" name="search" placeholder="Buscar...">
  					  <input id="order" required type="hidden" name="order" value="DESC">
	            <input type="submit" value="OK">
  					</form>
  				</div>
  			</div>
  			<div class="cart box_1">
  				<a id="btn-checkout-cart" href="javascript:void(0)">
  					<div class="total">
  					<span id="shopping-cart-total" class="simpleCart_total"></span> (<span id="shopping-cart-quantity" class="simpleCart_quantity"></span> <?= __('items') ?>)</div>
  					<img src="/app/images/bag.png" alt="" />
  				</a>
  				<p style="display:none;"><a id="btn-empty-cart" href="javascript:;" class="simpleCart_empty"><?= __('Vaciar Carrito') ?></a></p>
  				<div class="clearfix"> </div>
  			</div>
  			<div class="favorite-header">
  				<a id="btn-header-favorite" href="javascript:void(0)">
  					<img src="/app/img/icon_wishlist.png" alt="<?= __('Lista de Favoritos') ?>" />
  				</a>
  				<div class="clearfix"> </div>
  			</div>
  			<div class="clearfix"> </div>
  		</div>
  	</div>
  	<div class="navigation">
  		<div class="container">
  			<nav class="navbar navbar-default">
  				<!-- Brand and toggle get grouped for better mobile display -->
  				<div class="navbar-header nav_2">
  					<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
  						<span class="sr-only">Toggle navigation</span>
  						<span class="icon-bar"></span>
  						<span class="icon-bar"></span>
  						<span class="icon-bar"></span>
  					</button>
  				</div> 
  				<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
  					<ul class="nav navbar-nav">
  						<li class="<?= ($current_controller == 'Home' && $current_action == 'index') ? 'active' : '' ?>"><a href="/app/home/" class="<?= ($current_controller == 'Home' && $current_action == 'index') ? 'act' : '' ?>"><?= __('Inicio') ?></a></li>	
  						<li class="<?= ($current_controller == 'Catalogo' && $current_action == 'index') ? 'active' : '' ?>"><a href="/app/catalogo/index" class="<?= ($current_controller == 'Catalogo' && $current_action == 'index') ? 'act' : '' ?>"><?= __('Productos') ?></a></li>
  						<li class="dropdown <?= ($current_action == 'temporadas') ? 'active' : '' ?>">
                <a class="dropdown-toggle <?= ($current_action == 'temporadas') ? 'act' : '' ?>" data-toggle="dropdown" href="#"><?= __('Temporadas') ?>  <b class="caret"></b></a>
                <ul class="dropdown-menu pull-right">
                  <?php foreach ($seasons as $season): ?>
                    <li><a href="/app/catalogo/temporadas/<?= $season->id ?>"> <?= __($season->name) ?></a></li>
                  <?php endforeach; ?>
                </ul>
              </li>
						  <li class="<?= ($current_action == 'inicio') ? 'active' : '' ?>"><a class="<?= ($current_action == 'nosotros') ? 'act' : '' ?>" href="/app/home/nosotros"><?= __('Nosotros') ?></a></li>
						  <li class="<?= ($current_action == 'contactos') ? 'active' : '' ?>"><a class="<?= ($current_action == 'contactos') ? 'act' : '' ?>" href="/app/home/contactos"><?= __('Contactos') ?></a></li>
  						<li class="<?= ($current_action == 'locales') ? 'active' : '' ?>"><a class="<?= ($current_action == 'locales') ? 'act' : '' ?>" href="/app/home/locales"><?= __('Locales') ?></a></li>
  					</ul>
  				</div>
  			</nav>
  		</div>
  	</div>
    <!-- //header -->

    <!--contenido-->
    <?= $this->fetch('content') ?>

    <!-- newsletter -->
    <div class="newsletter">
    	<div class="container">
    		<div class="col-md-6 w3agile_newsletter_left">
    			<h3><?= __('Newsletter') ?></h3>
    			<p><?= __('Suscribete a nuestro Newsletter.') ?></p>
    		</div>
    		<div class="col-md-6 w3agile_newsletter_right">
    			 <?= $this->Form->create(null, ['url' => ['controller' => 'home' , 'action' => 'newsletter']]) ?>
    				<input type="email" name="email" value="<?= __('Correo') ?>" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Correo';}" required="">
    				<input type="submit" value="" />
    			<?= $this->Form->end() ?>
    		</div>
    		<div class="clearfix"> </div>
    	</div>
    </div>
    <!-- //newsletter -->

    <!-- footer -->
  	<div class="footer">
  		<div class="container">
  			<div class="w3_footer_grids">
  				<div class="col-md-3 w3_footer_grid">
  					<h3><?= __('Contactos') ?></h3>
  					<p><?= __('Puedes contactarnos por estos medios.') ?></p>
  					<ul class="address">
  						<li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i><?= __('Paseo Boggiani Boggiani 5976, <span>Asunción, Paraguay.') ?></span></li>
  						<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:info@example.com"><?= __('info@cootchycoo.com') ?></a></li>
  						<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i><?= __('021 602 439') ?></li>
  					</ul>
  				</div>
  				<div class="col-md-3 w3_footer_grid">
  					<h3><?= __('Información') ?></h3>
  					<ul class="info"> 
  						<li><a href="/app/home/nosotros"><?= __('Nosotros') ?></a></li>
  						<li><a href="/app/home/contactos"><?= __('Contactos') ?></a></li>
  						<li><a href="/app/home/locales"><?= __('Locales') ?></a></li>
  					</ul>
  				</div>
  				<div class="col-md-3 w3_footer_grid">
  					<h3><?= __('Temporadas') ?></h3>
  					<ul class="info">
							<?php foreach ($seasons as $season): ?>
                <li><a href="/app/catalogo/temporadas/<?= $season->id ?>"> <?= __($season->name) ?></a></li>
              <?php endforeach; ?>
  					</ul>
  				</div>
  				<div class="col-md-3 w3_footer_grid">
  					<h3><?= __('Redes Sociales') ?></h3>
  					<h4><?= __('Síguenos') ?></h4>
  					<div class="agileits_social_button">
  						<ul>
  							<li><a href="https://web.facebook.com/cootchycoo/" class="facebook fa fa-facebook"> </a></li>
  							<li><a href="https://www.instagram.com/cootchycoo/" class="instagram fa fa-instagram"> </a></li>
  							<li><a href="https://www.youtube.com/channel/UC5kgOlDPtaXMaWiFbUnLj3g" class="google fa fa-google-plus"> </a></li>
  							<li><a href="https://es.pinterest.com/cootchycoo/" class="pinterest fa fa-pinterest-p"> </a></li>
  						</ul>
  					</div>
  				</div>
  				<div class="clearfix"> </div>
  			</div>
  		</div>
  		<div class="footer-copy">
  			<div class="footer-copy1">
  				<div class="footer-copy-pos">
  					<a href="#top" class="scroll"><img src="/app/images/arrow.png" alt=" " class="img-responsive" /></a>
  				</div>
  			</div>
  			<div class="container">
  				<p>&copy; <span id="lbl_current_year"></span><?= __(' Cootchy Coo. Todos los derechos reservados | Design by') ?> <a href="http://w3layouts.com/"><?= __('W3layouts') ?> </a></p>
  			</div>
  		</div>
  	</div>
    <!-- //footer -->

    <?= $this->Flash->render() ?>

  </body>
  
  <script type="text/javascript">
      var shopping_cart;
      var customer;
      var current_controller;
      var confirm_purchase = false;

      var addToCart = function(id, amount) {
        $(".se-pre-con").show();
        $.ajax({
            url: "<?=$this->Url->build(['controller' => 'ShoppingCarts', 'action' => 'add'])?>",
            type: 'POST',
            dataType: "json",
            data : {
              id : id, // will be accessible in $_POST['id']
              amount: amount // will be accessible in $_POST['amount']
            },
            success: function(data) {
                $(".se-pre-con").fadeOut("slow");

                if (data.shopping_cart) {
                  var total = 0;
              		var amount = 0;
              		shopping_cart = data.shopping_cart;
              		if (shopping_cart) {

              			$.each(shopping_cart, function (i, article) {
              				total += (parseFloat(article.price).toFixed(2) * parseInt(article.amount));
				              amount += parseInt(article.amount);
              			});

              			$("#shopping-cart-quantity").text(amount);
		                $("#shopping-cart-total").text('Gs.' + parseFloat(total));
		                generateNoty('success', 'Articulo agregado al carrito.');

		                if (current_controller == 'ShoppingCarts') {
		                  var action = '/app/shopping-carts';
                      window.open(action, '_self');
		                }
              		}
                }
            },
            error: function (e) {
              $(".se-pre-con").fadeOut("slow");
              generateNoty('error', 'Error al cargar el articulo.');
            }
        });
      };

      var emptyCart = function() {
        $(".se-pre-con").show();
        $.ajax({
            url: "<?=$this->Url->build(['controller' => 'ShoppingCarts', 'action' => 'emptyCart'])?>",
            type: 'POST',
            dataType: "json",
            success: function(data) {
              $(".se-pre-con").fadeOut("slow");
              var total = 0.00;
          		var amount = 0;
          		shopping_cart = null;
        			$("#shopping-cart-quantity").text(amount);
              $("#shopping-cart-total").text('Gs.' + total);
              $('#confirm-empty-cart').modal('hide');

              if (current_controller == 'ShoppingCarts' || current_controller == 'ConfirmarCompra') {
                var action = '/app/catalogo';
                window.open(action, '_self');
              }
              generateNoty('success', 'Carrito vaciado.');
            },
            error: function (e) {
              $(".se-pre-con").fadeOut("slow");
              generateNoty('error', 'Error al vaciar el Carrito.');
            }
        });
      };

      var showPopup = function(message) {
        $('#message-popup').text(message);
        $('#messenger-popup').modal('show');
      }

      var deleteCart = function(id, callBack) {
        $(".se-pre-con").show();
        $.ajax({
            url: "<?=$this->Url->build(['controller' => 'ShoppingCarts', 'action' => 'delete'])?>",
            type: 'POST',
            dataType: "json",
            data : {
              id : id,
            },
            success: function(data) {
              $(".se-pre-con").fadeOut("slow");
              var total = 0;
          		var amount = 0;
              if (data.shopping_cart) {
            		shopping_cart = data.shopping_cart;
            		if (!jQuery.isEmptyObject(shopping_cart)) {
            			$.each(shopping_cart, function (i, article) {
            				total += (parseFloat(article.price).toFixed(2) * parseInt(article.amount));
		                amount += parseInt(article.amount);
            			});
            			$("#shopping-cart-quantity").text(amount);
                  $("#shopping-cart-total").text('Gs.' + parseFloat(total));
                  callBack(id);
            		} else {
                  window.location = "/app/catalogo/";
                }
              }
            },
            error: function (e) {
              $(".se-pre-con").fadeOut("slow");
              generateNoty('error', 'Error al eliminar el producto del carrito.');
            }
        });
      };

      var addToFavorites = function(id, img) {
      
  			if (customer) {
          $(".se-pre-con").show();
  				$.ajax({
            url: "<?= $this->Url->build(['controller' => 'favorites', 'action' => 'add']) ?>",
            type: 'POST',
            dataType: "json",
            data :  {
				      id : id,
            },
            success: function(data) {
              $(".se-pre-con").fadeOut("slow");
              img.attr('src','/app/img/icon_wishlist_added.png');
            	customer = data.customer;
			        generateNoty(data.type_notification, data.response);
            },
            error: function (e) {
              $(".se-pre-con").fadeOut("slow");
				      generateNoty('error', 'Error al añadir el articulo a la lista de favoritos. Por favor, intenta nuevamente.');
            }
  				});
  			} else {
  				$('#title-account-popup').text("Para añadir un producto a la lista de favoritos debes registrarte en Cootchy Coo o si ya posees una ingresa a la misma.");
      		$('#tab-no-login').hide();
			    $('#content-no-login').hide();
			    $('#tab-login').trigger('click');
      		$('#myModal88').modal('show');
  			}
  		};

      $(document).ready(function() {

        customer = <?=json_encode($customer)?>;
        console.log(customer);
        current_controller = "<?=$current_controller?>";
        current_action = "<?=$current_action?>";

        var current_year = new Date().getFullYear();
        $('#lbl_current_year').text(current_year);

        if (customer) {
          $('#lbl-username').text(customer.name);
          $('.lbl-session-register').hide();
        }

        $('#btn-user').click(function() {
          confirm_purchase = false;
          if (customer) {
            $('#user-logged-popup').modal('show');
          } else {
            $('#title-account-popup').text("");
            $('#tab-no-login').hide();
				    $('#content-no-login').hide();
				    $('#tab-login').trigger('click');
            $('#myModal88').modal('show');
          }
        });

        $("#login-form").submit(function(e) {
          e.preventDefault();
          $(".se-pre-con").show();
          $.ajax({
              url: "<?= $this->Url->build(['controller' => 'mi-cuenta', 'action' => 'login']) ?>",
              type: 'POST',
              dataType: "json",
              data :  JSON.stringify({
                email : $('.login input#email').val(),
                password : $('.login input#password').val(),
              }),
              success: function(data) {
                $(".se-pre-con").fadeOut("slow");
                if (data.data.type_notification == 'warning') {
                  generateNoty(data.data.type_notification, data.data.response);
                } else {
                  $('.login input#email').val("");
                  $('.login input#password').val("");
                  customer = data.data.customer;
                  $('#lbl-username').text(customer.name);
                  $('.lbl-session-register').hide();
                  $('#btn-edit').attr("href", "/app/mi-cuenta/edit/" + customer.id);
                  $('#myModal88').modal('hide');
                  generateNoty('success', 'Bienvenido ' + customer.name);
                  if (confirm_purchase) {
                    window.location = "/app/confirmar-compra";
                  }
                }
              },
              error: function (e) {
                $(".se-pre-con").fadeOut("slow");
                generateNoty('error', 'Error al hacer login.');
              }
            });
        });

        $("#register-form").submit(function(e) {
          e.preventDefault();
          if ($('.registerx input#password').val() !== $('.registerx input#re-password').val()) {
            generateNoty('warning', 'Las claves no coinciden.');
            return;
          }
          $(".se-pre-con").show();
          $.ajax({
            url: "<?= $this->Url->build(['controller' => 'mi-cuenta', 'action' => 'add']) ?>",
            type: 'POST',
            dataType: "json", 
            data : {
              name : $('.registerx input#name').val(),
              email : $('.registerx input#email').val(),
              address : $('.registerx input#address').val(),
              phone : $('.registerx input#phone').val(),
              password : $('.registerx input#password').val(),
              controller: current_controller,
            },
            success: function(data) {
              $(".se-pre-con").fadeOut("slow");
              if (data.data.type_notification == 'error') {
                generateNoty(data.data.type_notification, data.data.response);
              } else {
                $('.registerx input#name').val("");
                $('.registerx input#email').val("");
                $('.registerx input#address').val("");
                $('.registerx input#phone').val("");
                $('.registerx input#password').val("");
                $('.registerx input#re-password').val("");
                customer = data.data.customer;
                $('#lbl-username').text(customer.name);
                $('.lbl-session-register').hide();
                $('#myModal88').modal('hide');
                generateNoty('success', 'Gracias por registrarte en Cootchy Coo, recibirás un correo de confirmación.');

                if (confirm_purchase) {
                  window.location = "/app/confirmar-compra";
                }
              }
            },
            error: function (e) {
              $(".se-pre-con").fadeOut("slow");
              generateNoty('error', 'Error al crear cuenta.');
            }
          });
        });

        $('#btn-continue').click(function() {
          window.location = "/app/confirmar-compra";
        });

        $('#btn-logout').click(function() {
           $(".se-pre-con").show();
           $.ajax({
            url: "<?= $this->Url->build(['controller' => 'mi-cuenta', 'action' => 'logout']) ?>",
            type: 'POST',
            dataType: "json", 
            success: function(data) {
              $(".se-pre-con").fadeOut("slow");
              customer = null;
              $('#lbl-username').text("");
              $('.lbl-session-register').show();
              $('#user-logged-popup').modal('hide');
              if (current_controller == 'MiCuenta' && (current_action == 'edit' || current_action == 'favoritos')) {
                window.location = "/app/home/index";
              }
              generateNoty('success', 'Se ha cerrado correctamente su sesión');
            },
            error: function (e) {
              $(".se-pre-con").fadeOut("slow");
              generateNoty('error', 'Error al intentar cerrar sesión.');
            }
          });
        });

        $('#btn-empty-cart').click(function() {
          if (jQuery.isEmptyObject(shopping_cart)) {
            showPopup('Carrito Vacio');
          } else {
            $('#confirm-empty-cart').modal('show');
          }
        });

        $('#btn-confirm-empty-cart').click(function() {
          emptyCart();
        });

        $('#btn-checkout-cart').click(function() {
          if (jQuery.isEmptyObject(shopping_cart)) {
            showPopup('Carrito Vacio');
          } else {
            window.location = "/app/shopping-carts/";
          }
        });

        $('.login input#password').keypress(function(e) {
          if (e.keyCode == 13) {
            $('#btn-login').trigger('click');
          }
        });

        $('.register input#re-password').keypress(function(e) {
          if (e.keyCode == 13) {
            $('#btn-register').trigger('click');
          }
        });

        $('#btn-header-favorite').click(function() {
          if (customer) {
            window.location = "/app/mi-cuenta/favoritos";
    			} else {
    				$('#title-account-popup').text("Para añadir un producto a la lista de favoritos debes registrarte en Cootchy Coo o si ya posees una ingresa a la misma.");
        		$('#tab-no-login').hide();
  			    $('#content-no-login').hide();
  			    $('#tab-login').trigger('click');
        		$('#myModal88').modal('show');
    			}
        });

        $('#forget-password').click(function() {
          $('#myModal88').modal('hide');
          $('#forget-password-popup').modal('show');
        });

        $('.group-name').click(function() {
          var id = $(this).data('id');
          $('body')
            .append( $('<form/>').attr({'action': '/app/catalogo/index/', 'method': 'post', 'id': 'replacer'})
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'group_selected', 'value': id}))
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'order', 'value': null})))
            .find('#replacer').submit();
        });

        $('.icon-search').click(function() {
          $('#search').focus();
        });
      });
    </script>
</html>
