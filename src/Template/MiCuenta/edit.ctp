<!-- breadcrumbs -->
<div class="breadcrumb_dress">
	<div class="container">
		<ul>
			<li><a href="/app/home"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> <?= __('Incio') ?></a> <i>/</i></li>
			<li><?= __('Perfil') ?></li>
		</ul>
	</div>
</div>
<!-- //breadcrumbs -->

<div style="margin-top: 25px; margin-bottom: 25px;" class="container">
    <div class="row">
        <div class="col-md-3">
            <?= $this->Form->create($customer,  ['class' => 'form-load']) ?>
            <fieldset>
                <legend><?= __('Perfil') ?></legend>

                <?= $this->Form->input('name', ['label' => __('Nombre'), 'required' => true, 'type' => 'text', 'name' => 'name', 'id' => 'name']) ?>
                <?= $this->Form->input('email', ['label' => __('Correo'), 'required' => true, 'type' => 'email', 'name' => 'email', 'id' => 'email']) ?>
                <?= $this->Form->input('address', ['label' => __('Domicilio'), 'required' => true, 'type' => 'text', 'name' => 'address', 'id' => 'address']); ?>
                <?= $this->Form->input('phone', ['label' => __('Teléfono'), 'required' => true, 'type' => 'text', 'name' => 'phone', 'id' => 'phone']); ?>
                <?= $this->Form->input('password', ['label' => __('Clave'), 'required' => false, 'type' => 'password', 'name' => 'password', 'id' => 'password', 'value' => '']) ?>
            </fieldset>
            <?= $this->Form->button(__('Modificar'), ['class' => 'btn-block btn-success']) ?>
            <?= $this->Form->end() ?>
        </div>
        <div class="col-md-9">
            <legend><?= __('Historial Compras') ?></legend>
            <div class="col-md-3 pull-right">
              <form action="#" method="get">
                <div class="input-group">
                  <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
                  <input class="form-control" id="system-search" name="q" placeholder="<?= __('Buscar...') ?>" required>
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                  </span>
                </div>
              </form>
            </div>
            <table class="table table-hover table-list-search" id="table-orders">
                <thead>
                    <tr>
                        <th><?= __('#') ?></th>
                        <th><?= __('Estado') ?></th>
                        <th><?= __('Fecha') ?></th>
                        <th><?= __('Total') ?></th>
                        <th><?= __('Formas de pago') ?></th>
                        <th><?= __('Detalle') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($orders->count() > 0): ?>
                    <?php $i = 1; ?>
                    <?php foreach ($orders as $order): ?>
                        <tr style="cursor: pointer;">
                            <td><?= $i ?></td>
                            <td><?= h($status[$order->status]) ?></td>
                            <td><?= date('d/m/Y H:i', strtotime($order->created->format('Y-m-d H:i:s'))) ?></td>
                            <td>Gs.<?= $order->total ?></td>
                            <td><?= h($order->payment_methods) ?></td>
                            <td class="order-detail" data-id="<?= $order->id ?>"><p data-placement="top" data-toggle="tooltip" title="Detalle"><button class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-list-alt"></span></button></p></td>
                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                    <?php else: ?>
                      <tr>
                        <td><?= __('Sin Compras') ?></td>
                      </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div id="history-popup" class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?= __('Detalle Compra') ?></h4>
      </div>
      <div class="modal-body">
        <div class="row">
           <div class="col-md-12">
             <table class="table table-hover" id="table-history" >
                <thead>
                  <tr>
                    <th><?= __('#') ?></th>
      							<th><?= __('Producto') ?></th>
      							<th><?= __('Cantidad') ?></th>
      							<th><?= __('Nombre') ?></th>
      							<th><?= __('Precio') ?></th>
                  </tr>
                </thead>
                <tbody class="content">
                </tbody>
              </table>
           </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  var orders;
  var shopping_cart;

  $(document).ready(function() {

      orders = <?= json_encode($orders) ?>;
      shopping_cart = <?= json_encode($shopping_cart) ?>;

      var total = 0;
  		var amount = 0;
  		if (shopping_cart) {

  			$.each(shopping_cart, function (i, article) {
  				total += parseFloat(article.price) * parseInt(article.amount);
  				amount += parseInt(article.amount);
  			});
  		}

  		$("#shopping-cart-quantity").text(amount);
  		$("#quantity-articles").text(amount);
  		$("#shopping-cart-total").text('Gs.' + parseFloat(total));

      var activeSystemClass = $('.list-group-item.active');

      //something is entered in search form
      $('#system-search').keyup(function() {
         var that = this;
          // affect all table rows on in systems table
          var tableBody = $('.table-list-search tbody');
          var tableRowsClass = $('.table-list-search tbody tr');
          $('.search-sf').remove();
          tableRowsClass.each(function(i, val) {

              //Lower text for case insensitive
              var rowText = $(val).text().toLowerCase();
              var inputText = $(that).val().toLowerCase();
              if (inputText != '')
              {
                  $('.search-query-sf').remove();
                  tableBody.prepend('<tr class="search-query-sf"><td colspan="6"><strong>Buscando por: "'
                      + $(that).val()
                      + '"</strong></td></tr>');
              }
              else
              {
                  $('.search-query-sf').remove();
              }
  
              if (rowText.indexOf(inputText) == -1)
              {
                  //hide rows
                  tableRowsClass.eq(i).hide();
              }
              else
              {
                  $('.search-sf').remove();
                  tableRowsClass.eq(i).show();
              }
          });
          //all tr elements are hidden
          if (tableRowsClass.children(':visible').length == 0)
          {
              tableBody.append('<tr class="search-sf"><td class="text-muted" colspan="6">Sin resultados.</td></tr>');
          }
      });

      $('.order-detail').click(function() {

        var id = $(this).data('id');
        var order = null;

        $.each(orders, function(index, ord) {
          if (id == ord.id) {
            order = ord;
          }
        });

        if (order) {
          $('#table-history .content').html('');

          if (order.orders_has_articles.length > 0) {
            $.each(order.orders_has_articles, function (i, oha) {
              var index = i + 1;
              var article_name = (oha.name_display) ? oha.name_display :  oha.description;
              var tr = "<tr><td>" + index + "</td><td><a href='javascript:void(0)'><img style='width: 113px; height: auto;' src='/app/images/" + oha.url + "' alt=' ' class='img-responsive' /></a></td><td>" + oha.amount + "</td><td>" + article_name  + "</td><td>Gs." + oha.price + "</td></tr>";
              $('#table-history .content').append(tr);
            });
          } else {
               var tr = "<tr><td>Sin articulos</td><td></td><td></td><td></td><td></td></tr>";
             $('#table-history .content').append(tr);
          }

          $('#history-popup').modal('show');
        }
      });
  });
</script>
