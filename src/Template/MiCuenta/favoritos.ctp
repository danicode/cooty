<style type="text/css">

    .modal-actions a.btn {
        width: 100%;
        margin-bottom: 5px;
    }

    tr.selected {
        background-color: rgba(255, 152, 0, 0.06);
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
      background-color: rgba(158, 158, 158, 0.12);
    }

</style>

<!-- breadcrumbs -->
<div class="breadcrumb_dress">
	<div class="container">
		<ul>
			<li><a href="/app/home"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> <?= __('Incio') ?></a> <i>/</i></li>
			<li><?= __('Favoritos') ?></li>
		</ul>
	</div>
</div>
<!-- //breadcrumbs -->

<div style="margin-top: 25px; margin-bottom: 25px;" class="container">
    <div class="row">
        <div class="col-md-12">
            <legend><?= __('Lista de Favoritos') ?></legend>
            <div class="col-md-3 pull-right">
              <form action="#" method="get">
                <div class="input-group">
                  <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
                  <input class="form-control" id="system-search" name="q" placeholder="<?= __('Buscar...') ?>" required>
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                  </span>
                </div>
              </form>
            </div>
            <table class="table table-hover table-list-search" id="table-favorites">
              <thead>
                <tr>
                  <th><?= __('#') ?></th>
                  <th><?= __('Producto') ?></th>
    							<th><?= __('Nombre') ?></th>
    							<th><?= __('Precio') ?></th>
                </tr>
              </thead>
              <tbody>
                <?php if (count($customer->favorites) > 0): ?>
                  <?php $i = 1; ?>
                  <?php foreach ($customer->favorites as $favorite): ?>
                    <?php
                      $image = "";
                      if ($favorite->article->style_id != null && count($favorite->article->style->assets) > 0) {
                          $image = '/app/images/' . $favorite->article->style->assets[0]->url;
                      }
                    ?>
                    <tr data-id="<?= $favorite->article->id ?>">
                      <td><?= $i ?></td>
                      <td><a href="javascript:void(0)"><img style="width: 113px; height: auto;" src="<?= $image ?>" alt="Sin imagen" class="img-responsive"/></a></td>
                      <td><?= ($favorite->article->name_display) ?  __($favorite->article->name_display) :  __($favorite->article->description) ?></td>
                      <td>Gs.<?= $favorite->article->price  ?></td>
                    </tr>
                    <?php $i++; ?>
                  <?php endforeach; ?>
                <?php else: ?>
                  <tr>
                    <td><?= __('Sin articulos') ?></td>
                  </tr>
                <?php endif; ?>
              </tbody>
            </table>
        </div>
    </div>
</div>

<div id="favorites-actions-popup" class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Acciones</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a class="btn btn-success " id="btn-add-to-cart" href="#" role="button">
                        <span class="glyphicon glyphicon-shopping-cart"  aria-hidden="true"></span>
                        <?= __('Añadir al Carrito') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>
                    <a class="btn btn-danger btn-favorites-delete" href="#" role="button" >
                        <span class="glyphicon glyphicon-trash"  aria-hidden="true"></span>
                        <?= __('Eliminar') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>
                </center>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  var customer;

  $(document).ready(function() {

      customer = <?= json_encode($customer) ?>;
      shopping_cart = <?= json_encode($shopping_cart) ?>;

      var total = 0;
  		var amount = 0;
  		if (shopping_cart) {

  			$.each(shopping_cart, function (i, article) {
  				total += parseFloat(article.price) * parseInt(article.amount);
  				amount += parseInt(article.amount);
  			});
  		}

  		$("#shopping-cart-quantity").text(amount);
  		$("#quantity-articles").text(amount);
  		$("#shopping-cart-total").text('Gs.' + parseFloat(total));

      var activeSystemClass = $('.list-group-item.active');

      //something is entered in search form
      $('#system-search').keyup(function() {
         var that = this;
          // affect all table rows on in systems table
          var tableBody = $('.table-list-search tbody');
          var tableRowsClass = $('.table-list-search tbody tr');
          $('.search-sf').remove();
          tableRowsClass.each(function(i, val) {

              //Lower text for case insensitive
              var rowText = $(val).text().toLowerCase();
              var inputText = $(that).val().toLowerCase();
              if (inputText != '')
              {
                  $('.search-query-sf').remove();
                  tableBody.prepend('<tr class="search-query-sf"><td colspan="6"><strong>Buscando por: "'
                      + $(that).val()
                      + '"</strong></td></tr>');
              }
              else
              {
                  $('.search-query-sf').remove();
              }
  
              if (rowText.indexOf(inputText) == -1)
              {
                  //hide rows
                  tableRowsClass.eq(i).hide();
              }
              else
              {
                  $('.search-sf').remove();
                  tableRowsClass.eq(i).show();
              }
          });
          //all tr elements are hidden
          if (tableRowsClass.children(':visible').length == 0)
          {
              tableBody.append('<tr class="search-sf"><td class="text-muted" colspan="6">Sin resultados.</td></tr>');
          }
      });

      $('a#btn-add-to-cart').click(function() {
        var id = $('#table-favorites tr.selected').data('id');
        $('#favorites-actions-popup').modal('hide');
			  addToCart(id, 1);
      });

      $(document).on("click", ".btn-favorites-delete", function(e) {

        var text = 'Esta Seguro que desea eliminar el articulo de la lista de favoritos ?';
        var controller = 'favorites';
        var id  = $('#table-favorites tr.selected').data('id');

        bootbox.confirm(text, function(result) {
          if (result) {
            $('body')
            .append( $('<form/>').attr({'action': '/app/'+controller+'/delete/', 'method': 'post', 'id': 'replacer'})
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"})))
            .find('#replacer').submit();
          }
        });
      });
  });

  $('#table-favorites tbody').on('click', 'tr', function (e) {
    var tbody = $("#table-favorites tbody");
    var id = $(this).data('id');

    if (tbody.children().length != 0) {
      $('#table-favorites tr.selected').removeClass('selected');
      $(this).addClass('selected');

      var article = null;
      console.log(customer);
      $.each(customer.favorites, function(key, favorite) {
        if (favorite.article_id == id) {
           article = favorite.article;
         }
      });

      if (article.style_id != null && article.style.assets.length > 0) {
        $('#btn-add-to-cart').show();
      } else {
        $('#btn-add-to-cart').hide();
      }

      $('#favorites-actions-popup').modal('show');
    }
  });
</script>
