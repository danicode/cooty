<style type="text/css">

    #table-orders {
        width: 100% !important;
    }

    #table-orders td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 5px 0px 5px !important;
        vertical-align: middle;
    }

    #table-history {
        width: 100% !important;
    }

    #table-history td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 5px 0px 5px !important;
        vertical-align: middle;
    }

    .modal-actions a.btn {
        width: 100%;
        margin-bottom: 5px;
    }

    tr.selected {
        background-color: #d2d2d2;
        color: #333 !important;
    }

    td.details-control {
      background: url('/app/img/details_open.png') no-repeat center center;
      cursor: pointer;
    }

    tr.shown td.details-control {
      background: url('/app/img/details_close.png') no-repeat center center;
    }
    
    .my-hidden{
      display: none !important;
    }
    
    .table.article th {
      background-color: #fff;
      border-bottom-color: #000;
      color: #000;
    }
    
    .table.article thead {
      border-bottom: 0.5px solid #ede8e6;
    }

</style>

<div id="btns-tools">
    <div class="pull-right btns-tools margin-bottom-5">
        <button type="button" class="btn btn-default btn-export" title="Exportar en formato excel el contenido de la tabla" >
            <span class="glyphicon icon-file-excel"  aria-hidden="true" ></span>
        </button>
    </div>
</div>

<div class="row">
    <div id="table_wrapper" class="col-md-12">
        <table class="table table-bordered table-hover" id="table-orders" >
            <thead>
                <tr>
                    <th class="details-control sorting_disabled" rowspan="1" colspan="1" aria-label="" style="width: 18px;"></th>
                    <th><?= __('Nro de Pedidos') ?></th>
                    <th class="my-hidden"><?= __('Código') ?></th>
                    <th class="my-hidden"><?= __('Descripción') ?></th>
                    <th class="my-hidden"><?= __('Cantidad') ?></th>
                    <th><?= __('Estado') ?></th>
                    <th><?= __('Fecha') ?></th>
                    <th><?= __('Total') ?></th>
                    <th><?= __('Cliente') ?></th>
                    <th><?= __('Estado Cliente') ?></th>
                    <th><?= __('Correo') ?></th>
                    <th><?= __('Domicilio') ?></th>
                    <th><?= __('Teléfono') ?></th>
                    <th><?= __('Repartidor') ?></th>
                    <th><?= __('Sucursal') ?></th>
                    <th><?= __('Formas de pago') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($orders as $order): ?>
                  <?php $flag = true; ?>
                  <?php foreach ($order->orders_has_articles as $oha): ?>
                    <tr class="<?= $flag ? '' : 'my-hidden' ?> <?= h(strtolower($status[$order->status])) ?>" data-id="<?= $order->id ?>">
                        <td class="details-control"></td>
                        <td><?= $order->id ?></td>
                        <td class="my-hidden"><?= h($oha->article->code) ?></td>
                        <td class="my-hidden"><?= h($oha->article->description) ?></td>
                        <td class="my-hidden"><?= $oha->amount ?></td>
                        <td><?= h($status[$order->status]) ?></td>
                        <td><?= date('d/m/Y H:i', strtotime($order->created->format('Y-m-d H:i:s'))) ?></td>
                        <td>Gs.<?= $order->total ?></td>
                        <td><?= h($order->customer_name) ?></td>
                        <td><?= ($order->customer) ? __('Registrado') : __('Sin Registrarse') ?></td>
                        <td><?= h($order->email) ?></td>
                        <td><?= h($order->address) ?></td>
                        <td><?= h($order->phone) ?></td>
                        <td><?= ($order->delivery) ? h($order->delivery->name) : 'Sin asignar' ?></td>
                        <td><?= ($order->store) ? h($order->store->name) : 'Sin asignar' ?></td>
                        <td><?= h($order->payment_methods) ?></td>
                    </tr>
                    <?php $flag = false; ?>
                  <?php endforeach; ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div id="assign-popup" class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?= __('Asignación repartidor - sucursal') ?></h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
              <form method="post" action="/app/orders/assignment">
                <div class="form-group">
                  <select name="delivery_id" class="form-control" id="deliveries-select" required>
                  </select>
                </div>
                <div class="form-group">
                  <select name="store_id" class="form-control" id="stores-select" required>
                  </select>
                </div>
                <input type="hidden" name="order_id" value="">
                <button type="submit" class="btn btn-primary"><?= __('Asignar') ?></button>
              </form>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="change-status-popup" class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?= __('Acciones') ?></h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a class="btn btn-pending " id="btn-assign" href="#" role="button">
                        <span class="glyphicon glyphicon-edit"  aria-hidden="true"></span>
                        <?= __('Reasignar') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>
                    <a id="btn-dispatched" class="btn btn-info" href="#" role="button" >
                        <span class="glyphicon glyphicon-send"  aria-hidden="true"></span>
                        <?= __('Despachado') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>
                    <a id="btn-completed" class="btn btn-success" href="#" role="button" >
                        <span class="glyphicon glyphicon-ok"  aria-hidden="true"></span>
                        <?= __('Completado') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>
                    <a id="btn-cancelled" class="btn btn-danger" href="#" role="button" >
                        <span class="glyphicon glyphicon-remove"  aria-hidden="true"></span>
                        <?= __('Cancelado') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>
                    <a id="btn-history" class="btn btn-default" href="#" role="button" >
                        <span class="glyphicon glyphicon-list-alt"  aria-hidden="true"></span>
                        <?= __('Historial') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>
                </center>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="history-popup" class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?= __('Historial') ?></h4>
      </div>
      <div class="modal-body">
        <div class="row">
           <div class="col-md-12">
             <table class="table table-bordered table-hover" id="table-history" >
                <thead>
                  <tr>
                    <th><?= __('#') ?></th>
                    <th><?= __('Usuario') ?></th>
                    <th><?= __('Estado') ?></th>
                    <th><?= __('Fecha') ?></th>
                  </tr>
                </thead>
                <tbody class="content">
                </tbody>
              </table>
           </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  var table_orders = null;
  var table_history = null;
  var orders;
  var userAdmin;

  var assign = function(order) {
    if (userAdmin) {
      $.ajax({
  			url: "<?= $this->Url->build(['controller' => 'orders', 'action' => 'assign']) ?>",
  			type: 'POST',
  			dataType: "json",
  			data : {
  			  id : order.id,
  			},
  			success: function(data) {

  				var deliveries = data.deliveries;
          $('#deliveries-select').html('');
  				$('#deliveries-select').append('<option value="">Repartidor</option>');
  				$.each (deliveries, function(i, delivery) {
  				  selection = delivery.name == ((order.delivery) ? order.delivery.name : '') ? 'selected' : '';
  				  $('#deliveries-select').append('<option ' + selection + ' value="' + delivery.id + '">' + delivery.name + ' (' + delivery.assigned_order_amount + ')</option>');
  				});
  
  				var stores = data.stores_availables;
  				$('#stores-select').html('');
  			  $('#stores-select').append('<option value="">Sucursal</option>');
  				$.each (stores, function(i, store) {
  				  selection = store.name == ((order.store) ? order.store.name : '') ? 'selected' : '';
  				  $('#stores-select').append('<option ' + selection + ' value="' + store.id + '">' + store.name + '</option>');
  				});
  				$('input[name="order_id"]').val(order.id);
  				$('#change-status-popup').modal('hide');
  				$('#assign-popup').modal('show');
  			},
  			error: function (e) {
  		    console.log(e);
  				generateNoty('error', 'Error al asignar el pedido.');
  			}
  	  });
    } else {
      generateNoty('warning', 'Únicamente el administrador puede asignar al pedido repartidor y sucursal.');
    }
  };

  /* Formatting function for row details - modify as you need */
  function format(d) {

    var id = d[1];
    // `d` is the original data object for the row
    var table =
    '<table class="table article table-condensed">'+
      '<thead>'+
        '<tr>'+
          '<th>Código</td>'+
          '<th>Producto</td>'+
          '<th>Cantidad</td>'+
          '<th>Total</td>'+
        '</tr>'+
      '</thead>'+
      '<tbody>';

      $.each(orders, function(index, order) {
        $.each(order.orders_has_articles, function(index, oha) {
          if (order.id == id) {
            table +=
            '<tr>' +
              '<td>' + oha.code + '</td>' +
              '<td>' + oha.name_display + '</td>' +
              '<td>' + oha.amount + '</td>' +
              '<td>Gs.' + oha.amount * oha.price + '</td>' +
            '</tr>';
          }
        });
      });

      table +=
      '</tbody>' +
    '</table>';

    return table;
  }

  $(document).ready(function() {

    orders = <?= json_encode($orders) ?>;
    userAdmin = <?= $userAdmin; ?>;

    $('#table-orders').removeClass('display');

  		table_orders = $('#table-orders').DataTable({
  		    "order": [[ 0, 'asc' ]],
  		    "autoWidth": true,
  		    "scrollY": '450px',
  		    "scrollCollapse": true,
  		    "scrollX": true,
  		    "language": {
              "decimal":        "",
              "emptyTable":     "Sin resultados.",
              "info":           "_START_ al _END_ de _TOTAL_",
              "infoEmpty":      "0 al 0 de 0",
              "infoFiltered":   "",
              "infoPostFix":    "",
              "thousands":      ",",
              "lengthMenu":     "Mostrar _MENU_ filas",
              "loadingRecords": "Cangando...",
              "processing":     "Procesando...",
              "search":         "Buscar:",
              "zeroRecords":    "Sin resultados",
              "paginate": {
                  "first":      "Primero",
                  "last":       "Ultimo",
                  "next":       "Siguinte",
                  "previous":   "Anterior"
              },
              "aria": {
                  "sortAscending":  ": Activar para ordenar la columna ascendente",
                  "sortDescending": ": Activar para ordenar la columna descendente"
              }
          },
          "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "Todas"]],
          /*"dom": '<"toolbar">frtip'*/
  		});

  		$('#table-orders_filter').closest('div').before($('#btns-tools').contents());

  		// Add event listener for opening and closing details
      $('#table-orders tbody').on('click', 'td.details-control', function () {
          var tr = $(this).closest('tr');
          var row = table_orders.row( tr );

          if ( row.child.isShown() ) {
              // This row is already open - close it
              row.child.hide();
              tr.removeClass('shown');
          }
          else {
              // Open this row
              row.child( format(row.data()) ).show();
              tr.addClass('shown');
          }
      });
    });

    $('#table-orders tbody').on( 'click', 'tr td', function (e) {
        if (!$(this).hasClass('details-control')) {
          if (!$(this).find('.dataTables_empty').length) {

            var id = $(this).parent().data('id');
            var ord = null;
            $.each(orders, function( index, order ) {
              if (id == order.id) {
                ord = order;
              }
            });

            if (ord) {
              // Estado 0 = PENDIENTE ó Estado 4 = CANCELADO
              if (ord.status == 0 || ord.status == 4) {
                if (ord.status == 0) {  //Estado 0 = PENDIENTE
                   assign(ord);
                } else if (ord.status == 4) { // Estado 4 = CANCELADO
                  if (userAdmin) {
                    $("#btn-assign").show();
                  } else {
                    $("#btn-assign").hide();
                  }
                  $("#btn-dispatched").hide();
                  $("#btn-completed").hide();
                  $("#btn-cancelled").hide();
                  $("#btn-history").show();
                  $('#change-status-popup').modal('show');
                }
              } else if (ord.status == 3) { // Estado 3 = COMPLETADO
                $("#btn-assign").hide();
                $("#btn-dispatched").hide();
                $("#btn-completed").hide();
                $("#btn-cancelled").hide();
                $("#btn-history").show();
                $('#change-status-popup').modal('show');
              } else { // Estado 1 = ASIGNADO
                $("#btn-assign").hide();
                $("#btn-dispatched").show();
                $("#btn-completed").hide();
                $("#btn-cancelled").show();
                $("#btn-history").show();
                if (ord.status == 2) { // Estado 2 = DESPACHADO
                  $("#btn-assign").hide();
                  $("#btn-dispatched").hide();
                  $("#btn-completed").show();
                  $("#btn-cancelled").show();
                  $("#btn-history").show();
                }
                $('#change-status-popup').modal('show');
              }
            }

            table_orders.$('tr.selected').removeClass('selected');
            $(this).parent().addClass('selected');
          }
        }
    });

    $(".btn-export").click(function(e) {
        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
              $('#table-orders').find('.my-hidden').removeClass('my-hidden').addClass('my-show');
              $('#table-orders').tableExport({tableName: 'Pedidos', type:'excel', escape:'false'});
              $('#table-orders').find('.my-show').removeClass('my-show').addClass('my-hidden');
            }
        });
    });

    $("#btn-assign").click(function() {
      var id  = table_orders.$('tr.selected').data('id');
      var ord;
      $.each(orders, function( index, order ) {
        if (id == order.id) {
          ord = order;
        }
      });
      assign(ord);
    });

    $('#btn-dispatched').click(function() {
      var id  = table_orders.$('tr.selected').data('id');
      $('body')
        .append( $('<form/>').attr({'action': '/app/orders/change-status/', 'method': 'post', 'id': 'replacer'})
        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'status', 'value': 2})))
        .find('#replacer').submit();
    });

    $('#btn-completed').click(function() {
      var id  = table_orders.$('tr.selected').data('id');
      $('body')
        .append( $('<form/>').attr({'action': '/app/orders/change-status/', 'method': 'post', 'id': 'replacer'})
        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'status', 'value': 3})))
        .find('#replacer').submit();
    });

    $('#btn-cancelled').click(function() {
      var id  = table_orders.$('tr.selected').data('id');
      $('body')
        .append( $('<form/>').attr({'action': '/app/orders/change-status/', 'method': 'post', 'id': 'replacer'})
        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'status', 'value': 4})))
        .find('#replacer').submit();
    });

    $('#btn-history').click(function() {
      var id  = table_orders.$('tr.selected').data('id');
      $.ajax({
  			url: "<?= $this->Url->build(['controller' => 'orders', 'action' => 'history']) ?>",
  			type: 'POST',
  			dataType: "json",
  			data : {
  			  id : id,
  			},
  			success: function(data) {

  			  if (table_history) {
            table_history.destroy();
          }

          $('#table-history .content').html('');

          $.each(data.orders_status, function (i, order_status) {
              var username = order_status.user == null ? '-' : order_status.user.username;
              var tr = "<tr class="+data.status[order_status.status].toLowerCase()+" data-id='"+order_status.id+"'><td>"+order_status.id+"</td><td>"+username+"</td><td>"+data.status[order_status.status]+"</td><td>"+order_status.created.date+"</td></tr>";
              $('#table-history .content').append(tr);
          });

          table_history = $('#table-history').DataTable({
    		    "order": [[ 0, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '450px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "columnDefs": [
              {
                "targets": [ 3 ],
                "visible": true,
                "searchable": true,
                "type": "date-custom",
              },
            ],
    		    "language": {
              "decimal":        "",
              "emptyTable":     "Sin resultados.",
              "info":           "_START_ al _END_ de _TOTAL_",
              "infoEmpty":      "0 al 0 de 0",
              "infoFiltered":   "",
              "infoPostFix":    "",
              "thousands":      ",",
              "lengthMenu":     "Mostrar _MENU_ filas",
              "loadingRecords": "Cangando...",
              "processing":     "Procesando...",
              "search":         "Buscar:",
              "zeroRecords":    "Sin resultados",
              "paginate": {
                "first":      "Primero",
                "last":       "Ultimo",
                "next":       "Siguinte",
                "previous":   "Anterior"
              },
              "aria": {
                "sortAscending":  ": Activar para ordenar la columna ascendente",
                "sortDescending": ": Activar para ordenar la columna descendente"
              }
            },
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "Todas"]],
            /*"dom": '<"toolbar">frtip'*/
		      });

      		$('#history-popup').modal('show');
  			},
  			error: function (e) {
  		    console.log(e);
  				//generateNoty('error', 'Error al cargar los articulos.');
  			}
  	  });
    });

    $('#history-popup').on('shown.bs.modal', function (e) {
      table_history.draw();
    });

</script>
