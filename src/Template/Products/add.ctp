<style type="text/css">

    .jqte {
        margin: 0px 0;
    }

    .jqte_tool.jqte_tool_1 .jqte_tool_label {
        position: relative;
        display: block;
        padding: 3px;
        width: 70px;
        height: 25px;
        overflow: hidden;
    }

</style>

<div class="row">
     <div class="col-md-2">
        <div class="pull-right">
            <a class="btn btn-default" title="<?= __('Volver a la página anterior') ?>" href="<?= $this->Url->build(["controller" => "products", "action" => "index"]) ?>" role="button">
                <span class="glyphicon icon-undo2"  aria-hidden="true"></span>
            </a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        <?= $this->Form->create($product) ?>
        <fieldset>
            <legend><?= __('Agregar producto') ?></legend>
            <?php
                echo $this->Form->input('name', ['label' =>  __('Nombre')]);
                echo $this->Form->input('price', ['label' =>  __('Precio')]);
                echo $this->Form->input('enable', ['label' =>  __('Habilitar')]);
                echo $this->Form->input('visible', ['label' =>  __('Visible')]);
                echo $this->Form->input('news', ['label' =>  __('Nuevo')]);
                echo $this->Form->input('category_id', ['label' =>  __('Categorias'), 'options' => $categories]);
                echo $this->Form->input('season_id', ['label' =>  __('Temporadas'), 'options' => $seasons]);
            ?>
            <div class="form-group text">
                <label class="control-label" for="custom-description"> <?= __('Descripción a Mostrar') ?> </label>
                <textarea name="custom_description" class="jqte-test"></textarea>
            </div>
        </fieldset>
        <?= $this->Form->button(__('Agregar')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<script>

    $(document).ready(function() {
        $('.jqte-test').jqte();

    	// settings of status
    	var jqteStatus = true;
    	$(".status").click(function()
    	{
    		jqteStatus = jqteStatus ? false : true;
    		$('.jqte-test').jqte({"status" : jqteStatus})
    	});
    });

</script>
