<style type="text/css">

    #table-styles {
        width: 100% !important;
    }

    #table-styles td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 5px 0px 5px !important;
        vertical-align: middle;
    }

     #table-articles {
        width: 100% !important;
    }

    #table-articles td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 5px 0px 5px !important;
        vertical-align: middle;
    }

     #table-add-articles {
        width: 100% !important;
    }

    #table-add-articles td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 5px 0px 5px !important;
        vertical-align: middle;
    }

    .modal-actions a.btn {
        width: 100%;
        margin-bottom: 5px;
    }

    tr.selected {
        background-color: #8eea99;
        color: #333 !important;
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }

    #gallery img {
        width: 100%;
    }

    .jqte {
        margin: 0px 0;
    }

    .jqte_tool.jqte_tool_1 .jqte_tool_label {
        position: relative;
        display: block;
        padding: 3px;
        width: 70px;
        height: 25px;
        overflow: hidden;
    }

</style>

<div id="btns-styles-tools">
    <div class="pull-right btns-tools margin-bottom-5">
        <a class="btn btn-default" title="Nuevo Estilo" href="#" role="button">
            <span class="glyphicon icon-plus"  aria-hidden="true"></span>
        </a>
    </div>
</div>

<div id="btns-articles-tools">
    <div class="pull-right btns-tools margin-bottom-5">
        <a class="btn btn-default" title="Nuevo Articulo" href="#" role="button">
            <span class="glyphicon icon-plus" aria-hidden="true"></span>
        </a>
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        <legend><?= __('Producto') ?><a class="pull-right btn btn-default" title="<?= __('Ir a la lista de Productos.') ?>" href="<?= $this->Url->build(["controller" => "products", "action" => "index"]) ?>" role="button">
            <span class="glyphicon icon-undo2"  aria-hidden="true"></span>
        </a></legend>
        <?= $this->Form->create($product) ?>
        <fieldset>
            <?php
                echo $this->Form->input('name', ['label' => __('Nombre')]);
                echo $this->Form->input('price', ['label' => __('Precio')]);
                echo $this->Form->input('enable', ['label' => __('Habilitado')]);
                echo $this->Form->input('visible', ['label' => __('Visible')]);
                echo $this->Form->input('news', ['label' => __('Nuevo')]);
                echo $this->Form->input('category_id', ['label' => __('Categorias'), 'options' => $categories]);
                echo $this->Form->input('season_id', ['label' => __('Temporadas'), 'options' => $seasons]);
            ?>
            <div class="form-group text">
                <label class="control-label" for="custom-description"> <?= __('Descripción a Mostrar') ?> </label>
                <textarea name="custom_description" class="jqte-test"><?= $product->custom_description ? $product->custom_description : '' ?></textarea>
            </div>
        </fieldset>
        <?= $this->Form->button(__('Modificar')) ?>
        <?= $this->Form->end() ?>
    </div>
    <div class="col-md-5">
        <div class="row">
            <div class="col-md-12">
                <legend><?= __('Estilos') ?></legend>
                <table class="table table-bordered table-hover" id="table-styles" >
                    <thead>
                        <tr>
                            <th><?= __('Nombre') ?></th>
                            <th><?= __('Imagen') ?></th>
                            <th><?= __('Acción') ?></th>
                        </tr>
                    </thead>
                    <tbody class="content">
                        <?php foreach ($product->styles as $style): ?>
                        <tr data-id="<?= $style->id ?>">
                            <td><?= h($style->name) ?></td>
                            <td style="text-align: center;"><img style="max-width: 48px; height: auto; display: inline;" class="img-circle" src="/app/images/<?= $style->img_url ?>"></td>
                            <td><i style="cursor: pointer;" class='glyphicon glyphicon-remove btn-delete-style' data-id="<?= $style->id ?>" href='javascript:void(0)'></i></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12">
                <legend><?= __('Articulos') ?></legend>
                <table class="table table-bordered table-hover" id="table-articles" >
                    <thead>
                        <tr>
                            <th><?= __('Código') ?></th>
                            <th><?= __('Descripción') ?></th>
                            <th><?= __('Nombre a Mostrar') ?></th>
                            <th><?= __('Talle') ?></th>
                            <th><?= __('Habilitado') ?></th>
                            <th><?= __('Acción') ?></th>
                        </tr>
                    </thead>
                    <tbody class="content">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="row">
            <div class="col-md-11">
                <legend><?= __('Galeria') ?></legend>
            </div>
            <div class="col-md-1">
                <div class="pull-right">
                    <a class="btn btn-default" title="Nueva Imagen" href="#" role="button" id="btn-add-asset" >
                        <span class="glyphicon icon-plus" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div id="gallery" class="row">
                </div>
            </div>
        </div>
    </div>
</div>

<div id="styles-popup" class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?= __('Añadir estilo') ?></h4>
      </div>
      <div class="modal-body">
        <?= $this->Form->create(null, ['url' => ['controller' => 'Styles' , 'action' => 'add'], 'enctype' => "multipart/form-data" ]) ?>
        <fieldset>
            <?php
                echo $this->Form->input('name', ['label' => __('Estilo')]);
                echo $this->Form->input('product_id', ['type' => 'hidden', 'value' => $product->id]);
                echo $this->Form->file('file', ['required' => true, 'type'=>'file']);
            ?>
        </fieldset>
        <br>

        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Cancelar') ?></button>
            <?= $this->Form->button(__('Agregar')) ?>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>

<div id="assets-popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?=  __('Añadir Imagen') ?></h4>
      </div>
      <div class="modal-body">
           <?= $this->Form->create(null, ['url' => ['controller' => 'Assets' , 'action' => 'add', $product->id], 'enctype' => "multipart/form-data" ]) ?>
            <fieldset>
                <?php
                    echo $this->Form->file('files[]', ['required' => true, 'multiple'=>'multiple']);
                    echo $this->Form->input('style_id', ['type' => 'hidden']);
                ?>
            </fieldset>
            <br>

            <button type="button" class="btn btn-default" data-dismiss="modal"><?=  __('Cancelar') ?></button>
            <?= $this->Form->button(__('Agregar')) ?>
            <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>

<div id="articles-popup" class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?=  __('Articulos disponibles') ?></h4>
      </div>
      <div class="modal-body">
        <div class="row">
           <div class="col-md-12">
               <table class="table table-bordered table-hover" id="table-add-articles" >
                    <thead>
                        <tr>
                            <th><?=  __('Código') ?></th>
                            <th><?=  __('Descripción') ?></th>
                            <th><?=  __('Nombre a Mostrar') ?></th>
                            <th><?=  __('Talle') ?></th>
                        </tr>
                    </thead>
                    <tbody class="content">
                    </tbody>
                </table>
                <br>
                 <button type="button" class="btn btn-default" data-dismiss="modal"><?=  __('Cancelar') ?></button>
                 <button id="btn-add-talle" type="button" class="btn btn-default"><?=  __('Agregar') ?></button>
           </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="select-talle" class="hidden">
    <select style="width:100%;" name="talle" class="form-control talle">
        <option value="" ><?=  __('Seleccionar Talle') ?></option>
        <option value="1A" ><?=  __('1A') ?></option>
        <option value="2A" ><?=  __('2A') ?></option>
        <option value="3A" ><?=  __('3A') ?></option>
        <option value="4A" ><?=  __('4A') ?></option>
        <option value="5A" ><?=  __('5A') ?></option>
        <option value="6A" ><?=  __('6A') ?></option>
        <option value="7A" ><?=  __('7A') ?></option>
        <option value="8A" ><?=  __('8A') ?></option>
        <option value="9A" ><?=  __('9A') ?></option>
        <option value="10A" ><?=  __('10A') ?></option>
        <option value="11A" ><?=  __('11A') ?></option>
        <option value="12A" ><?=  __('12A') ?></option>
        <option value="13A" ><?=  __('13A') ?></option>
        <option value="14A" ><?=  __('14A') ?></option>
        <option value="15A" ><?=  __('15A') ?></option>
        <option value="16A" ><?=  __('16A') ?></option>
        <option value="17A" ><?=  __('17A') ?></option>
        <option value="18A" ><?=  __('18A') ?></option>
        <option value="19A" ><?=  __('19A') ?></option>
        <option value="20A" ><?=  __('20A') ?></option>
        <option value="ACC" ><?=  __('ACC') ?></option>
    </select>
</div>

<script type="text/javascript">

   var product = <?= json_encode($product); ?>;
   var table_styles = null;
   var table_articles = null;
   var table_add_articles = null;
   var select_talle = '';

   $(document).ready(function () {
       
        $('.jqte-test').jqte();

    	// settings of status
    	var jqteStatus = true;
    	$(".status").click(function()
    	{
    		jqteStatus = jqteStatus ? false : true;
    		$('.jqte-test').jqte({"status" : jqteStatus})
    	});

        $('a#btn-add-asset').hide();

        $('#table-styles').removeClass('display');
        $('#table-articles').removeClass('display');

		table_styles = $('#table-styles').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cangando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "Todas"]],
            /*"dom": '<"toolbar">frtip'*/
		});
        selectStyle(product.style_selected);

		table_articles = $('#table-articles').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cangando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todas"]],
            /*"dom": '<"toolbar">frtip'*/
		});

		$('#table-styles_filter').closest('div').before($('#btns-styles-tools').contents());
		$('#table-articles_filter').closest('div').before($('#btns-articles-tools').contents());

		select_talle = $('#select-talle').html();

    });

    function selectStyle(id) {

        $('#table-styles tbody tr').each(function() {

            if (id == $(this).data('id')) {
                $(this).trigger('click');
            }
        });
    }

    var style_id_selected = null;

    $('#table-styles tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_styles.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            $.each(product.styles, function (i, style) {

                var id = table_styles.$('tr.selected').data('id');
                style_id_selected = id;
                
                $('#assets-popup input#style-id').val(id);
                $('a#btn-add-asset').show();    

                if (style.id == id) {

                    $('#table-articles .content').html('');

                    //articulos - talles
                    $.each(style.articles, function (i, article) {
                        var article_name = (article.name_display) ? article.name_display : '';
                        var tr = "<tr data-id='"+article.id+"'><td>"+article.code+"</td><td>"+article.description+"</td><td>"+article_name+"</td><td>"+article.size;
                        (article.enable) ? tr += '<td><span class="icon-checkmark2 green" aria-hidden="true"></span><span style="display:none;">1</span></td>' :  tr += '<td><span class="icon-cancel-circle red" aria-hidden="true"></span><span style="display:none;">0</span></td>'
                        tr += "<td><i style='cursor: pointer;' class='glyphicon glyphicon-remove btn-delete-article' data-id='"+article.id+"' href='javascript:void(0)'></i></td>"
                        tr += "</tr>";
                        
                        $('#table-articles .content').append(tr);
                    });

                    //galeria imagen
                    $('#gallery').html('');
                    $.each(style.assets, function (i, asset) {

                        var image = "<div class='col-md-6'>";
                        image += "<div class='row'><div class='col-md-12'><img style='max-width: 200px;' src='/app/images/"+asset.url+"'></img></div></div><div class='row'><div class='col-md-12'><a class='btn-delete-asset' data-id='"+asset.id+"' href='javascript:void(0)'>Quitar</a></div></div>";
                        image += "</div>";
                        $('#gallery').append(image);
                    });
               
                };
            });
        };
    });

    $('#table-articles tbody').on( 'click', 'tr', function (e) {
        if (!$(this).find('.dataTables_empty').length) {
             table_styles.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    $('a#btn-add-asset').click(function() {
        $('#assets-popup').modal('show');
    });

    $('#btns-styles-tools a.btn-default').on( 'click', function (e) {
        $('#styles-popup.modal-actions').modal('show');
    });

    $('#btns-articles-tools a.btn-default').on( 'click', function (e) {

        if (style_id_selected) {

            $.ajax({
                url: "<?=$this->Url->build(['controller' => 'Articles', 'action' => 'getArticlesAjax'])?>",
                type: 'POST',
                dataType: "json",

                success: function(data) {

                    if (data.articles) {
                        if (table_add_articles) {
                            table_add_articles.destroy();
                        }

                        $('#table-add-articles .content').html('');

                        $.each(data.articles, function(i, article) {
                            var tr = "<tr data-id="+article.id+"><td>"+article.code+"</td><td>"+article.description+"</td><td><input style='width:100%' class='display-name' placeholder='Ingrese Nombre a Mostrar...' type='text' value=''/></td><td>"+select_talle+"</td></tr>";
                            $('#table-add-articles .content').append(tr);
                        });

                        table_add_articles = $('#table-add-articles').DataTable({
                		    "order": [[ 0, 'asc' ]],
                		    "autoWidth": true,
                		    "scrollY": '450px',
                		    "scrollCollapse": true,
                		    "scrollX": true,
                		    "language": {
                                "decimal":        "",
                                "emptyTable":     "Sin resultados.",
                                "info":           "_START_ al _END_ de _TOTAL_",
                                "infoEmpty":      "0 al 0 de 0",
                                "infoFiltered":   "",
                                "infoPostFix":    "",
                                "thousands":      ",",
                                "lengthMenu":     "Mostrar _MENU_ filas",
                                "loadingRecords": "Cangando...",
                                "processing":     "Procesando...",
                                "search":         "Buscar:",
                                "zeroRecords":    "Sin resultados",
                                "paginate": {
                                    "first":      "Primero",
                                    "last":       "Ultimo",
                                    "next":       "Siguiente",
                                    "previous":   "Anterior"
                                },
                                "aria": {
                                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                                    "sortDescending": ": Activar para ordenar la columna descendente"
                                }
                            },
                            "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "Todas"]],
                            /*"dom": '<"toolbar">frtip'*/
                		});

                		$('#articles-popup').modal('show');
                    } else {
                        generateNoty('warning', 'Todos los articulos tienen estilo asignado.');
                    }
                },
                error: function () {
                    generateNoty('error', 'Error al obtener los articulos.');
                }
            });
        } else {
            generateNoty('warning', 'Debe seleccionar un estilo.');
        }
    });

    //seleccion tabla table-add-articles
    $('#table-add-articles tbody').on('click', 'tr', function(e) {
        if (!$(this).find('.dataTables_empty').length) {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                $(this).addClass('selected');
            }
        }
    });

    $('#articles-popup').on('shown.bs.modal', function (e) {
      table_add_articles.draw();
    });

    $('a#btn-info').click(function() {
      var action = '/app/colors/view/' + table_products_has_colors.$('tr.selected').data('id');
      window.open(action, '_black');
    });

    $('a#btn-edit').click(function() {
       var action = '/app/colors/edit/' + table_products.$('tr.selected').data('id');
       window.open(action, '_self');
    });

    $(document).on("click", "a.btn-delete-asset", function(e) {

        var id  = $(this).data('id');
        var text = 'Esta Seguro que desea eliminar la imagen ? ';
        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/app/assets/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'product_id', 'value': product.id})))
                .find('#replacer').submit();
            }
        });
    });

    $(document).on("click", "i.btn-delete-style", function(e) {

        var id  = $(this).data('id');
        var text = 'Esta Seguro que desea eliminar el estilo ? ';
        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/app/styles/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'product_id', 'value': product.id})))
                .find('#replacer').submit();
            }
        });
    });

    $(document).on("click", "i.btn-delete-article", function(e) {

        var id  = $(this).data('id');
        var text = 'Esta Seguro que desea eliminar el articulo ? ';
        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/app/articles/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'product_id', 'value': product.id})))
                .find('#replacer').submit();
            }
        });
    });

    //agregar talles
    $('#btn-add-talle').click(function() {

        var ids = [];
        var validate = true;

        $('#table-add-articles tbody tr.selected').each(function(index, value) {
            var id = $(this).data('id');

            if ($(this).find('select.talle').val() == '') {
                generateNoty('warning', 'Debe especificar los talles.');
                validate = false;
            }

            ids.push(id);
            ids.push($(this).find('select.talle').val());
            ids.push($(this).find('input.display-name').val());
        });

        if (ids.length == 0) {
            generateNoty('warning', 'Debe seleccionar los articulos.');
            validate = false;
        }

        if (validate) {
            $('body')
            .append( $('<form/>').attr({'action': '/app/articles/edit/', 'method': 'post', 'id': 'replacer'})
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'ids', 'value': ids}))
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'product_id', 'value': product.id}))
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'style_id', 'value': style_id_selected})))
            .find('#replacer').submit();
        }
    });

    $(document).on("click", ".btn-delete2", function(e) {

        var text = 'Esta Seguro que desea eliminar el color ?';
        var controller  = 'Colors';
        var id  = table_colors.$('tr.selected').data('id');

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/app/'+controller+'/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"})))
                .find('#replacer').submit();
            }
        });
    });
</script>
