<style type="text/css">

    #table-products {
        width: 100% !important;
    }

    #table-products td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 5px 0px 5px !important;
        vertical-align: middle;
    }

    .modal-actions a.btn {
        width: 100%;
        margin-bottom: 5px;
    }

     tr.selected {
        background-color: #8eea99;
        color: #333 !important;
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }

</style>

<div id="btns-tools">
    <div class="pull-right btns-tools margin-bottom-5">
        <a class="btn btn-default" title="Nuevo producto" href="<?=$this->Url->build(["controller" => "Products", "action" => "add"])?>" role="button">
            <span class="glyphicon icon-plus"  aria-hidden="true"></span>
        </a>
        <button type="button" class="btn btn-default btn-export" title="Exportar en formato excel el contenido de la tabla" >
            <span class="glyphicon icon-file-excel"  aria-hidden="true" ></span>
        </button>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-products" >
            <thead>
                <tr>
                    <th><?= __('#') ?></th>
                    <th><?= __('Nombre') ?></th>
                    <th><?= __('Precio') ?></th>
                    <th><?= __('Habilitado') ?></th>
                    <th><?= __('Visible') ?></th>
                    <th><?= __('Creado') ?></th>
                    <th><?= __('Modificado') ?></th>
                    <th><?= __('Nuevo') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($products as $product): ?>
                <tr data-id="<?= $product->id ?>">
                    <td><?= $product->id ?></td>
                    <td><?= h($product->name) ?></td>
                    <td>Gs.<?= $product->price ?></td>
                    <?php if ($product->enable): ?>
                        <td>
                            <span class="icon-checkmark2 green" aria-hidden="true"></span>
                            <span style="display:none;" >1</span>
                        </td>
                    <?php else: ?>
                        <td>
                            <span class="icon-cancel-circle red" aria-hidden="true"></span>
                            <span style="display:none;" >0</span>
                        </td>
                    <?php endif; ?>
                    <?php if ($product->visible): ?>
                        <td>
                            <span class="icon-checkmark2 green" aria-hidden="true"></span>
                            <span style="display:none;" >1</span>
                        </td>
                    <?php else: ?>
                        <td>
                            <span class="icon-cancel-circle red" aria-hidden="true"></span>
                            <span style="display:none;" >0</span>
                        </td>
                    <?php endif; ?>
                    <td><?= date('d/m/Y H:i', strtotime($product->created->format('Y-m-d H:i:s'))) ?></td>
                    <td><?= date('d/m/Y H:i', strtotime($product->modified->format('Y-m-d H:i:s'))) ?></td>
                    <?php if ($product->news): ?>
                        <td>
                            <span class="icon-checkmark2 green" aria-hidden="true"></span>
                            <span style="display:none;" >1</span>
                        </td>
                    <?php else: ?>
                        <td>
                            <span class="icon-cancel-circle red" aria-hidden="true"></span>
                            <span style="display:none;" >0</span>
                        </td>
                    <?php endif; ?>
                <?php endforeach; ?>
                </tr>
            </tbody>
        </table>
    <div>
</div>

<div class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?= __('Acciones') ?></h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a class="btn btn-default " id="btn-edit" href="#" role="button">
                        <span class="glyphicon glyphicon-edit"  aria-hidden="true"></span>
                        <?= __('Editar') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>
                    <a class="btn btn-danger btn-delete2" href="#" role="button" >
                        <span class="glyphicon icon-bin"  aria-hidden="true"></span>
                        <?= __('Eliminar') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>
                </center>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

   var table_products = null;

   $(document).ready(function () {

        $('#table-products').removeClass('display');
		
		table_products = $('#table-products').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cangando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguinte",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "Todas"]],
            /*"dom": '<"toolbar">frtip'*/
		});
		
		$('#table-products_filter').closest('div').before($('#btns-tools').contents());

    });

    $('#table-products tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_products.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            $('.modal-actions').modal('show');
        }
    });

    $('a#btn-info').click(function() {
      var action = '/app/products/view/' + table_products.$('tr.selected').data('id');
      window.open(action, '_black');
    });

    $('a#btn-edit').click(function() {
       var action = '/app/products/edit/' + table_products.$('tr.selected').data('id');
       window.open(action, '_self');
    });

    $('.btn-export-pdf').click(function() {
       var action = '/app/products/viewpdf/';
       window.open(action, '_black');
    });

    $(".btn-export").click(function() {
        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-products').tableExport({tableName: 'Productos', type:'excel', escape:'false'});
            }
        });
    });

    $(document).on("click", ".btn-delete2", function(e) {

        var text = 'Esta Seguro que desea eliminar el producto ?';
        var controller  = 'Products';
        var id  = table_products.$('tr.selected').data('id');

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/app/'+controller+'/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"})))
                .find('#replacer').submit();
            }
        });
    });

</script>
