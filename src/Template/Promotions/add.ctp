<div class="row">
     <div class="col-md-2">
        <div class="pull-right">
            <a class="btn btn-default" title="<?= __('Volver a la lista de Banners') ?>" href="<?=$this->Url->build(["controller" => "promotions", "action" => "index"])?>" role="button">
                <span class="glyphicon icon-undo2" aria-hidden="true"></span>
            </a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        <legend><?= __('Banner') ?></legend>
        <?= $this->Form->create($promotion) ?>
        <fieldset>
            <?php
                echo $this->Form->input('title', ['label' =>  __('Titulo')]);
                echo $this->Form->input('description', ['label' =>  __('Descripción')]);
                echo $this->Form->input('enable', ['label' =>  __('Habilitar')]);
                echo $this->Form->input('deleted', ['label' =>  __('Eliminado')]);
            ?>
        </fieldset>
        <?= $this->Form->button(__('Agregar')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
