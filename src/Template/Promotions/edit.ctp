<div class="row">
     <div class="col-md-2">
        <div class="pull-right">
            <a class="btn btn-default" title="Volver a la lista de Banners" href="<?=$this->Url->build(["controller" => "promotions", "action" => "index"])?>" role="button">
                <span class="glyphicon icon-undo2" aria-hidden="true"></span>
            </a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        <legend><?= __('Banner') ?></legend>
        <?= $this->Form->create($promotion) ?>
        <fieldset>
            <?php
                echo $this->Form->input('title', ['label' => __('Titulo')]);
                echo $this->Form->input('description', ['label' => __('Descripción')]);
                echo $this->Form->input('enable', ['label' => __('Habilitar')]);
                echo $this->Form->input('deleted', ['label' => __('Eliminado')]);
            ?>
        </fieldset>
        <?= $this->Form->button(__('Guardar')) ?>
        <?= $this->Form->end() ?>
    </div>

    <div class="col-md-6">
        <div class="row">
            <div class="col-md-11">
                <legend><?= __('Galeria') ?></legend>
            </div>
            <div class="col-md-1">
                <div class="pull-right">
                    <a class="btn btn-default" title="<?= __('Nueva Imagen') ?>" href="#" role="button" id="btn-add-gallery" >
                        <span class="glyphicon icon-plus" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div id="gallery" class="row">
                    <?php foreach ($promotion->promotions_assets as $pa): ?>
                        <div class='col-md-12'>
                            <img style="max-width: 200px;" src="/app/images/<?= $pa->url ?>"></img>
                        </div>
                        <div class='row'>
                            <div class='col-md-12'>
                                <a class='btn-delete-gallery' data-id="<?= $pa->id ?>" href='javascript:void(0)'><?= __('Quitar') ?></a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="assets-popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?= __('Añadir Imagen') ?></h4>
      </div>
      <div class="modal-body">
           <?= $this->Form->create(null, ['url' => ['controller' => 'PromotionsAssets' , 'action' => 'add'], 'enctype' => "multipart/form-data" ]) ?>
            <fieldset>
                <?php
                    echo $this->Form->file('files[]', ['required' => true, 'multiple'=>'multiple']);
                    echo $this->Form->input('promotion_id', ['type' => 'hidden', 'value' => $promotion->id]);
                ?>
            </fieldset>
            <br>

            <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Cancelar') ?></button>
            <?= $this->Form->button(__('Agregar')) ?>
            <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

    var promotion = <?= json_encode($promotion); ?>;

    $(document).ready(function () {
       
    });

    $('a#btn-add-gallery').click(function() {
        $('#assets-popup').modal('show');
    });

    $(document).on("click", "a.btn-delete-gallery", function(e) {

        var id  = $(this).data('id');
        var text = 'Esta Seguro que desea eliminar la imagen ? ';
        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/app/promotions-assets/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id})))
                .find('#replacer').submit();
            }
        });
    });
</script>
