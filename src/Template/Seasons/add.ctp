<div class="row">
     <div class="col-md-2">
        <div class="pull-right">
             <a class="btn btn-default" title="Ir a la lista de Temporadas." href="<?=$this->Url->build(["action" => "index"])?>" role="button">
                <span class="glyphicon icon-undo2"  aria-hidden="true"></span>
            </a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        <?= $this->Form->create($season) ?>
        <fieldset>
            <?php
                echo $this->Form->input('name', ['label' => 'Nombre']);
                echo $this->Form->input('visible', ['label' => __('Visible')]);
            ?>
        </fieldset>
        <?= $this->Form->button(__('Aceptar')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
