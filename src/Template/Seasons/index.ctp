<style type="text/css">

    #table-seasons {
        width: 100% !important;
    }

    #table-seasons td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 5px 0px 5px !important;
        vertical-align: middle;
    }

    .modal-actions a.btn {
        width: 100%;
        margin-bottom: 5px;
    }

    tr.selected {
        background-color: #8eea99;
        color: #333 !important;
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }

</style>

<div id="btns-tools">
    <div class="pull-right btns-tools margin-bottom-5">
        <a class="btn btn-default" title="Nueva Temporada" href="<?=$this->Url->build(["controller" => "Seasons", "action" => "add"])?>" role="button">
            <span class="glyphicon icon-plus" aria-hidden="true"></span>
        </a>
        <a class="btn btn-default btn-back-page" title="<?= __('Volver a la página anterior') ?>" href="#" role="button">
            <span class="glyphicon icon-undo2" aria-hidden="true"></span>
        </a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-seasons" >
            <thead>
                <tr>
                    <th><?= __('#') ?></th>
                    <th><?= __('Nombre') ?></th>
                    <th><?= __('Visible') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($seasons as $season): ?>
                <tr data-id="<?=$season->id?>">
                    <td><?= $season->id ?></td>
                    <td><?= h($season->name) ?></td>
                    <?php if ($season->visible): ?>
                        <td>
                            <span class="icon-checkmark2 green" aria-hidden="true"></span>
                            <span style="display:none;" >1</span>
                        </td>
                    <?php else: ?>
                        <td>
                            <span class="icon-cancel-circle red" aria-hidden="true"></span>
                            <span style="display:none;" >0</span>
                        </td>
                    <?php endif; ?>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?= __('Acciones') ?></h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <a class="btn btn-default " id="btn-edit" href="#" role="button">
                        <span class="glyphicon glyphicon-edit"  aria-hidden="true"></span>
                        <?= __('Editar') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>
                    <a class="btn btn-danger btn-delete2" href="#" role="button" >
                        <span class="glyphicon icon-bin"  aria-hidden="true"></span>
                        <?= __('Eliminar') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>
                </center>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

   var table_seasons = null;

   $(document).ready(function () {

        $('#table-seasons').removeClass('display');

		table_seasons = $('#table-seasons').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cangando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguinte",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "Todas"]],
            /*"dom": '<"toolbar">frtip'*/
		});

		$('#table-seasons_filter').closest('div').before($('#btns-tools').contents());

    });

    $('#table-seasons tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_seasons.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            $('.modal-actions').modal('show');
        }
    });

    $('.btn-export-pdf').click(function(){
       var action = '/app/seasons/viewpdf/';
       window.open(action, '_black');
    });

    $(".btn-export").click(function() {
        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-seasons').tableExport({tableName: 'Seasons', type:'excel', escape:'false'});
            }
        });
    });

    $('a#btn-edit').click(function() {
       var action = '/app/seasons/edit/' + table_seasons.$('tr.selected').data('id');
       window.open(action, '_self');
    });

</script>
