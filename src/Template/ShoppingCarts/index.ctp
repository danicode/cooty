<!-- breadcrumbs -->
	<div class="breadcrumb_dress">
		<div class="container">
			<ul>
				<li><a href="/app/home"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> <?= __('Incio') ?></a> <i>/</i></li>
				<li><?= __('Carrito') ?></li>
			</ul>
		</div>
	</div>
<!-- //breadcrumbs -->

<div class="container">
	<h2 class="text-center"><?= __('Carrito') ?> <img src="/app/images/bag.png" alt="" /></h2>
</div>

<!-- checkout -->
	<div class="checkout">
		<div class="container">
			<h3><?= __('Carrito contiene') ?>: <span id='quantity-articles'></span></h3>

			<div class="checkout-right">
				<table class="timetable_sub">
					<thead>
						<tr>
							<th><?= __('Nro') ?></th>
							<th><?= __('Producto') ?></th>
							<th><?= __('Cantidad') ?></th>
							<th><?= __('Nombre') ?></th>
							<th><?= __('Precio') ?></th>
							<th><?= __('Eliminar') ?></th>
						</tr>
					</thead>

                    <?php if ($shopping_cart): ?>
                    	<?php $i = 1; ?>
    					<?php foreach ($shopping_cart as $id => $article): ?>
    					    <tr class="rem<?= $id ?>">
    					    	<td class="invert"><?= $i++ ?></td>
        						<td class="invert-image"><a href="javascript:void(0)"><img style="width: 113px; height: auto;" src="/app/images/<?= $article->style->assets[0]->url ?>" alt=" " class="img-responsive" /></a></td>
        						<td class="invert">
        						    <div class="quantity"> 
    									<div class="quantity-select">                           
        									<div class="entry value-minus">&nbsp;</div>
        									<div data-id="<?= $id ?>" class="entry value"><span><?= __($article->amount) ?></span></div>
        									<div class="entry value-plus active">&nbsp;</div>
        								</div>
        							</div>
    							</td>
    							<td class="invert"><?= ($article->name_display) ? __($article->name_display) :  __($article->description) ?></td>
        						<td class="invert">Gs.<?= __($article->price) ?></td>
        						<td class="invert">
        							<div class="rem">
        								<div data-id="<?= $id ?>" class="close-remove"> </div>
        							</div>
        							<script>
        								$(document).ready(function(c) {
        									var postDeleteArticule = function (id) {
    											$('.rem' + id).fadeOut('slow', function(c) {
													$('.rem' + id).remove();
													$("table.timetable_sub td").each(function(index) {
														if (index == 0) {
															$(this).text(++index);
														}
													});

													var total = 0;
													var amount = 0;
													$("ul#summary-section").html('');
													var x = 0;
													$.each (shopping_cart, function(i, article) {
														amount += parseInt(article.amount);
														total += parseInt(article.amount) * parseFloat(article.price);
														x++;
														$("ul#summary-section").append('<li>Total Producto ' + x +' <i>-</i> <span>$' + parseInt(article.amount) * parseFloat(article.price) + '</span></li>');
													});
													$("#quantity-articles").text(amount);
													$("ul#summary-section").append('<li class="last">Total <i>-</i> <span id = "lbl-total">$' + parseFloat(total).toFixed(2) + '</span></li>');
												});
        									}

        									$(".close-remove[data-id='<?= $id ?>']").on('click', function(c) {
	        									bootbox.confirm('Se eliminara el articulo del carrito', function(result) {
										            if (result) {
										            	var id = <?= $id ?>;
									            		deleteCart(id, postDeleteArticule);
										            }
										        });
	    									});
    									});
        						   </script>
        						</td>
    					  	</tr>      
    					<?php endforeach; ?>
					<?php endif; ?>

					<!--quantity-->
					<script>
					$('.value-plus').on('click', function() {
						var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) + 1;
						divUpd.text(newVal - 1);
						update(divUpd.data('id'), 1);
					});

					$('.value-minus').on('click', function() {
						var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) - 1;
						if (newVal >= 1) {
							divUpd.text(newVal + 1);
							update(divUpd.data('id'), 0);
						}
					});
					</script>
					<!--quantity-->
				</table>
			</div>
			<div class="checkout-left">	
				<div class="checkout-left-basket">
					<h4><?= __('Resumen') ?></h4>
					<ul id="summary-section">
						<?php $i = 1; ?>
						<?php $total = 0; ?>
						<?php if ($shopping_cart): ?>
						    <?php foreach ($shopping_cart as $article): ?>
						        <li><?= __('Total Producto') ?> <?= $i ?> <i>-</i> <span data-id="<?= $article->id ?>">Gs.<?= $article->price * $article->amount ?></span></li>
						        <?php $i++; ?>
						        <?php $total += $article->price * $article->amount; ?>
						    <?php endforeach; ?>
					    <?php endif; ?>
						<li class="last"><?= __('Total') ?> <i>-</i> <span id = "lbl-total">Gs.<?= $total ?></span></li>
					</ul>
					<a id="btn-buy" href="javascript:void(0)"><span class="" aria-hidden="true"></span><?= __('REALIZAR COMPRA') ?></a>
					<div class="clearfix"> </div>
				</div>
				<div class="checkout-right-basket">
					<a href="/app/catalogo/"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span><?= __('Continuar compras') ?></a>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<div class="w3l_related_products">
		<div class="container">
			<h3><?= __('Accesorios') ?></h3>
			<ul id="flexiselDemo2">
				<?php foreach ($news_products as $product): ?>
				<li>
					<div class="w3l_related_products_grid">
						<div class="agile_ecommerce_tab_left dresses_grid">
							<div class="hs-wrapper hs-wrapper3">
								<?php $display = "block"; ?>
								<?php foreach ($product->styles as $style): ?>
								 	<div class="section-images-stylesx" data-product-id="<?= $product->id; ?>" data-id="<?= $style->id; ?>" style="display: <?= $display; ?>">
										<?php $images = array(); $i = 0; $img = ''?>
										<?php foreach ($style->assets as $asset): ?>
											<?php if ($i == 0): ?>
											<?php $img = $asset->url; ?>
											<?php endif; $i++; ?>
										    <!-- imagen debe tener un width: 300 px - height: 400 px -->
											<?php array_push($images, '/app/images/' . $asset->url); ?>
										<?php endforeach; ?>
										<div class="images-rotation" data-images='["<?= implode("\", \"", $images); ?>"]'>
									    	<img style="cursor: pointer;" data-type='news' data-id="<?= $product->id ?>" src="/app/images/<?= $img ?>" alt="" class="btn-view-product img-responsive">
									  	</div>
										<?php $display = "none"; ?>
									</div>
								<?php endforeach; ?>
							</div>
							<h5><a href="javascript:void(0)"><?= __($product->name) ?></a></h5>
							<div class="simpleCart_shelfItem">
								<p><i class="item_price">Gs.<?= $product->price ?></i></p>
							</div>
							<div class="row">
							  <?php $product_selected = 'product-selected' ?>
					          <?php foreach ($product->styles as $style): ?>
						          <div class="col-sm-2">
				            			<a class='img-stylex <?= $product_selected ?>' data-product-id="<?= $product->id ?>" data-id="<?= $style->id ?>" href="javascript:void(0)"><img src="/app/images/<?= $style->img_url ?>" class'img-responsive' style='border-radius: 30px; height: 30px; width: 30px;' alt="<?= $style->name ?>"></a>
						          </div>
						          <?php $product_selected = '' ?>
					          <?php endforeach; ?>
				       		</div>
						</div>
					</div>
				</li>
				<?php endforeach; ?>
			</ul>
			<script type="text/javascript">
				$(window).load(function() {
					$("#flexiselDemo2").flexisel({
						visibleItems:4,
						animationSpeed: 1000,
						autoPlay: true,
						autoPlaySpeed: 3000,    		
						pauseOnHover: true,
						enableResponsiveBreakpoints: true,
						responsiveBreakpoints: { 
							portrait: { 
								changePoint:480,
								visibleItems: 1
							}, 
							landscape: { 
								changePoint:640,
								visibleItems:2
							},
							tablet: { 
								changePoint:768,
								visibleItems: 3
							}
						}
					});

					$('.images-rotation').imagesRotation();

					$('.btn-view-product').click(function() {
						var id = $(this).data('id');
						$.each (news_products, function(i, product) {
							if (id == product.id) {
								product_selected = product;
							}
						});

						var style_id;

						$(".section-images-stylesx[data-product-id='" + product_selected.id + "']").each(function(index, value) {
						    if ($(this).css("display") == "block") {
								style_id = $(this).data('id');
						    }
						});

						$.each (product_selected.styles, function(i, style) {
							if (style_id == style.id) {
								style_selected = style;
							}
						});

						stylesLoad();
						styleChanged(style_selected);

						$('#lbl-title').text(product_selected.name);
						$('#article_amount').text("1");
						$('#quick-view-popup').modal('show');
					});

					//seleccion del estilo
					$('div').on("click", "a.img-stylex", function() {

						var style_id = $(this).data('id');
						var product_id = $(this).data('product-id');

						$(".section-images-stylesx[data-product-id='" + product_id +"']").each(function(index, value) {
						    var id = $(this).data('id');

						    if (id == style_id) {
						    	$("a.img-stylex[data-id='" + id +"']").addClass('product-selected');
								$(this).css('display', '');
						    } else {
						    	$("a.img-stylex[data-id='" + id +"']").removeClass('product-selected');
								$(this).css('display', 'none');
						    }
						});
					});
				});
			</script>
			<script type="text/javascript" src="js/jquery.flexisel.js"></script>
		</div>
	</div>
	<div class="modal video-modal fade" id="myModal6" tabindex="-1" role="dialog" aria-labelledby="myModal6">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
				</div>
				<section>
					<div class="modal-body">
						<div class="col-md-5 modal_body_left">
							<img src="/app/images/39.jpg" alt=" " class="img-responsive" />
						</div>
						<div class="col-md-7 modal_body_right">
							<h4>a good look women's Long Skirt</h4>
							<p>Ut enim ad minim veniam, quis nostrud 
								exercitation ullamco laboris nisi ut aliquip ex ea 
								commodo consequat.Duis aute irure dolor in 
								reprehenderit in voluptate velit esse cillum dolore 
								eu fugiat nulla pariatur. Excepteur sint occaecat 
								cupidatat non proident, sunt in culpa qui officia 
								deserunt mollit anim id est laborum.</p>
							<div class="rating">
								<div class="rating-left">
									<img src="/app/images/star-.png" alt=" " class="img-responsive" />
								</div>
								<div class="rating-left">
									<img src="/app/images/star-.png" alt=" " class="img-responsive" />
								</div>
								<div class="rating-left">
									<img src="/app/images/star-.png" alt=" " class="img-responsive" />
								</div>
								<div class="rating-left">
									<img src="/app/images/star.png" alt=" " class="img-responsive" />
								</div>
								<div class="rating-left">
									<img src="/app/images/star.png" alt=" " class="img-responsive" />
								</div>
								<div class="clearfix"> </div>
							</div>
							<div class="modal_body_right_cart simpleCart_shelfItem">
								<p><span>Gs.320</span> <i class="item_price">Gs.250</i></p>
								<p><a class="item_add" href="#">Add to cart</a></p>
							</div>
							<h5>Color</h5>
							<div class="color-quality">
								<ul>
									<li><a href="#"><span></span>Red</a></li>
									<li><a href="#" class="brown"><span></span>Yellow</a></li>
									<li><a href="#" class="purple"><span></span>Purple</a></li>
									<li><a href="#" class="gray"><span></span>Violet</a></li>
								</ul>
							</div>
						</div>
						<div class="clearfix"> </div>
					</div>
				</section>
			</div>
		</div>
	</div>
<!-- //checkout -->

<!-- popup confirmacion para vaciar carrito -->
<div class="modal fade" id="confirm-remove-article-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><?= __('Confirmar') ?></h4>
            </div>
            <div class="modal-body">
                <p><?= __('Se eliminará el articulo seleccionado del carrito.') ?></p>
                <p><?= __('Esta Seguro de eliminar el articulo ?') ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Cancelar') ?></button>
                <a id="btn-confirm-remove-article" class="btn btn-danger btn-ok"><?= __('Aceptar') ?></a>
            </div>
        </div>
    </div>
</div>
<!-- // -->

<!-- quick-view-popup -->
<div data-backdrop="true" data-keyboard="true" id="quick-view-popup" class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="dialog">
        <div class="modal-content">
            <div class="modal-body">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <br>
                  <br>
                  <div class="row">
                      <div class="section-assets-options col-md-1">
                      </div>
                      <div class="section-main-assets col-md-5">
                      </div>
                      <div class="simpleCart_shelfItem col-md-6">
                      	  <h4 style="font-size: 20px; color: #222; font-weight: bold; font-family: Arial,Helvetica,sans-serif; text-transform:capitalize;" id='lbl-title' id="gridSystemModalLabel"></h4>
                          <p>Gs.<i id="lbl-price" class="item_price"></i></p>

                          <h4><?= __('Estilo') ?></h4>
                          <div class="row">
                              <div class="section-styles">
                              </div>
                          </div>
                          <h4><?= __('Talle') ?></h4>
                          <div class="row">
                              <div class="section-size col-md-12">
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-5">
                                  <h4><?= __('Cantidad') ?></h4>
                                  <div class="quantity"> 
										<div class="quantity-select">                           
											<div class="entry value-minus">&nbsp;</div>
											<div id ="article_amount" class="item_Quantity entry value"><span>1</span></div>
											<div class="entry value-plus active">&nbsp;</div>
										</div>
									</div>
                              </div>
                          	  <div style="margin-top: 20px" class="col-md-12">
									<p><a id="btn-add-to-cart" class="item_add" href="javascript:void(0)"><?= __('Agregar al carrito') ?></a></p>
							  </div>
                          </div>
                      </div>
                  </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	var shopping_cart;
	var customer;
	var news_products;
	var product_selected;
	var style_selected;
	var main_asset;
	var article_selected;

	var update = function(id, increment) {
		$.ajax({
			url: "<?= $this->Url->build(['controller' => 'shopping-carts', 'action' => 'update']) ?>",
			type: 'POST',
			dataType: "json",
			data : JSON.stringify({
				id : id,
				increment: increment,
			}),
			success: function(data) {
				if (data.shopping_cart) {
					var total = 0;
					var amount = 0;
					shopping_cart = data.shopping_cart;

					$.each (shopping_cart, function(i, article) {
						amount += parseInt(article.amount);
						total += parseInt(article.amount) * parseFloat(article.price);
						var p = parseFloat(article.price) * parseInt(article.amount);
						$("ul#summary-section span[data-id='" + article.id +"']").html("");
						$("ul#summary-section span[data-id='" + article.id +"']").append("Gs." + p);
					});
					$("#shopping-cart-quantity").text(amount);
					$("#quantity-articles").text(amount);
					$("#shopping-cart-total").text('Gs.' + parseFloat(total));
					$('#lbl-total').text('Gs.' + parseFloat(total));
				}
			},
			error: function (e) {
				console.log(e);
				//generateNoty('error', 'Error al cargar los articulos.');
			}
		});
	}

	//carga la imagen de la izquierda por defecto el primer Style
	var assetsOptionsLoad = function(style) {
	$('.section-assets-options').html('');
		$.each (style.assets, function(i, asset) {
			var img = "<a class='asset-option' data-id='"+asset.id+"' href='javascript:void(0)'><img src='/app/images/" + asset.url + "' class'img-responsive' style='width: 50px;' alt='Image'></a>";
			$('.section-assets-options').append(img).append('<br>').append('<br>');
		});
	}

	//cargar imagen principal
	var mainAssetLoad = function(asset) {
	$('.section-main-assets').html('');
		var img_main_asset = "<img style='max-width:100%; height: auto; display: block;' src='/app/images/" + asset.url + "' class='img-responsive' alt='Image'>"
		$('.section-main-assets').append(img_main_asset);
	}

	//carga de precio
	var articlePriceLoad = function(article) {
		$('#lbl-price').text(article.price);
	}

	//carga de estilos
	var stylesLoad = function() {
		$('.section-styles').html('');
		var id = style_selected.id;
		$.each (product_selected.styles, function (i, style) {
			var sel = (id == style.id) ? 'product-selected' : ''
			var img = "<div class='col-sm-1'><a class='" + sel + " img-style' data-id='" + style.id + "' href='javascript:void(0)'><img src='/app/images/" + style.img_url + "' class'img-responsive' style='border-radius: 30px; height: 30px; width: 30px;' alt='Image'></a></div>";
			$('.section-styles').append(img);
		});
	}

	//carga de talles
	var articleSizeLoad = function(style) {
		$('.section-size').html('');

		$.each (style.articles, function(i, article) {
			var btnSelection = 'btn-sizes';
			if (i == 0) {
				btnSelection = 'btn-sizes-selected';
			}
			var size = "<a data-id='" + article.id + "' style='margin-right: 4px;' type='button' class='btn-size " + btnSelection + "'>" + article.size + "</a>";
			$('.section-size').append(size);
		});
	}

	//funcion para cambiar el estilo
	var styleChanged = function (style) {
		assetsOptionsLoad(style);

		main_asset  = style.assets[0];
		mainAssetLoad(main_asset);

		article_selected = style.articles[0];
		articlePriceLoad(article_selected);

		articleSizeLoad(style);
	}

	//cambiar el talle y seleccionar articulo
	var sizeChanged = function(id) {
		$.each (style_selected.articles, function (i, article) {
			if (article.id == id) {
				article_selected = article;
			}
		});
	}

	$(document).ready(function() {

		customer = <?= json_encode($customer) ?>;
		shopping_cart = <?= json_encode($shopping_cart) ?>;
		news_products = <?= json_encode($news_products); ?>;
		var total = 0;
		var amount = 0;
		if (shopping_cart) {

			$.each(shopping_cart, function (i, article) {
				total += parseFloat(article.price) * parseInt(article.amount);
				amount += parseInt(article.amount);
			});
		}

		$("#shopping-cart-quantity").text(amount);
		$("#quantity-articles").text(amount);
		$("#shopping-cart-total").text('Gs.' + parseFloat(total));

		$("#btn-buy").click(function() {
			if (customer) {
				window.location = "/app/confirmar-compra";
			} else {
				confirm_purchase = true;
				$('#title-account-popup').text("Si deseas mantener un historial de compras registrate en Cootchy Coo o si ya posees una ingresa a la misma.");
				$('#tab-no-login').show();
				$('#tab-no-login').trigger('click');
				$('#myModal88').modal('show');
			}
		});
		
		//seleccion del estilo
		$('div').on("click", "a.img-stylex", function() {

			var style_id = $(this).data('id');

			$('.section-images-styles').each(function(index, value) {
			    var id = $(this).data('id');

			    if (id == style_id) {
			    	$("a.img-stylex[data-id='" + id +"']").addClass('product-selected');
					$(this).css('display', '');
			    } else {
			    	$("a.img-stylex[data-id='" + id +"']").removeClass('product-selected');
					$(this).css('display', 'none');
			    }
			});
		});

		//seleccion de las imagnes del costado si se hace click aparece como imagen principal
	    $('div').on("click", "a.asset-option", function() {
			var id = $(this).data('id');
			var asset;
			$.each (style_selected.assets, function(i, asst) {
				if (asst.id == id) {
					asset = asst;
				}
			});
			mainAssetLoad(asset);
		});

		//seleccion del estilo
	    $('div').on("click", "a.img-style", function() {
			var id = $(this).data('id');

			$.each (product_selected.styles, function(i, style) {
				if (style.id == id) {
				    $("a.img-style[data-id='" + style.id +"']").addClass('product-selected');
					style_selected = style;
					styleChanged(style_selected);
				} else {
					$("a.img-style[data-id='" + style.id +"']").removeClass('product-selected');
				}
			});
	    });

	    //cambia el estilo de la seleccion del boton de talles
	    $('div').on("click", "a.btn-size", function() {
			var id = $(this).data('id');
			$('.section-size a').each (function(index) {
				$(this).removeClass("btn-sizes-selected").addClass("btn-sizes");
				if (id == $(this).data('id')) {
					$(this).addClass("btn-sizes-selected");
					sizeChanged(id);
				}
			});
	    });

	    //quantity
		$('.value-plus').on('click', function(){
			var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) + 1;
			divUpd.text(newVal);
		});

		$('.value-minus').on('click', function(){
			var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) - 1;
			if (newVal >= 1) divUpd.text(newVal);
		});

		//agrega un articulo al carrito
		$('#btn-add-to-cart').click(function() {

			var amount = parseInt($("#article_amount").text());
			var id = article_selected.id;

			addToCart(id, amount);
		});
	});
</script>
