<div class="row">
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-11">
                <legend><?= __('Sucursal') ?></legend>
            </div>
            <div class="col-md-1">
                <div class="pull-right">
                    <a class="btn btn-default" title="Ir a la lista de sucursales." href="<?=$this->Url->build(["action" => "index"])?>" role="button">
                        <span class="glyphicon icon-undo2"  aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <?= $this->Form->create($store) ?>
                <fieldset>
                    <?php
                        echo $this->Form->input('name', ['label' => __('Nombre')]);
                        echo $this->Form->input('address', ['label' => __('Domicilio')]);
                        echo $this->Form->input('phone', ['label' => __('Teléfono')]);
                        echo $this->Form->input('enable', ['label' => __('Habilitado')]);
                    ?>
                </fieldset>
                <?= $this->Form->button(__('Guardar')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="row">
            <div class="col-md-11">
                <legend><?= __('Imagen Principal') ?></legend>
            </div>
            <div class="col-md-1">
                <div class="pull-right">
                    <a class="btn btn-default" title="<?= __('Nueva Imagen') ?>" href="#" role="button" id="btn-add-asset" >
                        <span class="glyphicon icon-plus" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div id="main-image" class="row">
                    <?php if ($store->url): ?>
                        <div class='col-md-4'>
                            <img style="max-width: 200px;" src="/app/images/<?= $store->url ?>"></img>
                            <a class='btn-delete-asset' data-id="<?= $store->id ?>" href='javascript:void(0)'><?= __('Quitar') ?></a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="row">
            <div class="col-md-11">
                <legend><?= __('Galeria') ?></legend>
            </div>
            <div class="col-md-1">
                <div class="pull-right">
                    <a class="btn btn-default" title="<?= __('Nueva Imagen') ?>" href="#" role="button" id="btn-add-gallery" >
                        <span class="glyphicon icon-plus" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div id="gallery" class="row">
                    <?php foreach ($store->stores_assets as $sa): ?>
                        <div class='col-md-4'>
                            <img style="max-width: 200px;" src="/app/images/<?= $sa->url ?>"></img>
                            <a class='btn-delete-gallery' data-id="<?= $sa->id ?>" href='javascript:void(0)'><?= __('Quitar') ?></a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>

</div>

<div id="styles-popup" class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?= __('Añadir Imagen') ?></h4>
      </div>
      <div class="modal-body">
        <?= $this->Form->create(null, ['url' => ['controller' => 'stores' , 'action' => 'addImage'], 'enctype' => "multipart/form-data" ]) ?>
            <fieldset>
                <?php
                    echo $this->Form->input('store_id', ['type' => 'hidden', 'value' => $store->id]);
                    echo $this->Form->file('file', ['required' => true, 'type'=>'file']);
                ?>
            </fieldset>
            <br>

            <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Cancelar') ?></button>
            <?= $this->Form->button(__('Agregar')) ?>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>

<div id="assets-popup" class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?=  __('Añadir Imagen') ?></h4>
      </div>
      <div class="modal-body">
           <?= $this->Form->create(null, ['url' => ['controller' => 'stores' , 'action' => 'addGallery'], 'enctype' => "multipart/form-data" ]) ?>
            <fieldset>
                <?php
                    echo $this->Form->file('files[]', ['required' => true, 'multiple'=>'multiple']);
                    echo $this->Form->input('store_id', ['type' => 'hidden', 'value' => $store->id]);
                ?>
            </fieldset>
            <br>

            <button type="button" class="btn btn-default" data-dismiss="modal"><?=  __('Cancelar') ?></button>
            <?= $this->Form->button(__('Agregar')) ?>
            <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

    var store = <?= json_encode($store); ?>;

    $('a#btn-add-asset').click(function() {
        $('#styles-popup.modal-actions').modal('show');
    });
    
    $('a#btn-add-gallery').click(function() {
        $('#assets-popup.modal-actions').modal('show');
    });

    $(document).on("click", "a.btn-delete-asset", function(e) {

        var id  = $(this).data('id');
        console.log(id);
        var text = 'Esta Seguro que desea eliminar la imagen ? ';
        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/app/stores/deleteImage/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id})))
                .find('#replacer').submit();
            }
        });
    });
    
    $(document).on("click", "a.btn-delete-gallery", function(e) {

        var id  = $(this).data('id');
        console.log(id);
        var text = 'Esta Seguro que desea eliminar la imagen ? ';
        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/app/stores/deleteGallery/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id})))
                .find('#replacer').submit();
            }
        });
    });
</script>