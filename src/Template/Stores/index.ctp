<style type="text/css">

    #table-stores {
        width: 100% !important;
    }

    #table-stores td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 5px 0px 5px !important;
        vertical-align: middle;
    }

    .modal-actions a.btn {
        width: 100%;
        margin-bottom: 5px;
    }

    tr.selected {
        background-color: #8eea99;
        color: #333 !important;
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }

</style>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-stores" >
            <thead>
                <tr>
                    <th><?= __('#') ?></th>
                    <th><?= __('Nombre') ?></th>
                    <th><?= __('Número') ?></th>
                    <th><?= __('Creado') ?></th>
                    <th><?= __('Modificado') ?></th>
                    <th><?= __('Dirección') ?></th>
                    <th><?= __('Teléfono') ?></th>
                    <th><?= __('Habilitado') ?></th>
                    <th><?= __('Imagen') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($stores as $store): ?>
                <tr data-id="<?= $store->id ?>">
                    <td><?= $store->id ?></td>
                    <td><?= h($store->name) ?></td>
                    <td><?= $store->number ?></td>
                    <td><?= h($store->created->format('Y-m-d H:i:s')) ?></td>
                    <td><?= h($store->modified->format('Y-m-d H:i:s')) ?></td>
                    <td><?= h($store->address) ?></td>
                    <td><?= h($store->phone) ?></td>
                    <?php if ($store->enable): ?>
                        <td>
                            <span class="icon-checkmark2 green" aria-hidden="true"></span>
                            <span style="display:none;" >1</span>
                        </td>
                    <?php else: ?>
                        <td>
                            <span class="icon-cancel-circle red" aria-hidden="true"></span>
                            <span style="display:none;" >0</span>
                        </td>
                    <?php endif; ?>
                    <?php if ($store->url): ?>
                        <td>
                            <span class="icon-checkmark2 green" aria-hidden="true"></span>
                            <span style="display:none;" >1</span>
                        </td>
                    <?php else: ?>
                        <td>
                            <span class="icon-cancel-circle red" aria-hidden="true"></span>
                            <span style="display:none;" >0</span>
                        </td>
                    <?php endif; ?>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?= __('Acciones') ?></h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <?php if ($user['admin']): ?>
                        <a class="btn btn-default" id="btn-edit" href="#" role="button">
                            <span class="glyphicon glyphicon-edit"  aria-hidden="true"></span>
                            <?= __('Editar') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </a>
                    <?php endif; ?>
                    <a class="btn btn-warning " id="btn-view" href="#" role="button">
                        <span class="glyphicon glyphicon-eye-open"  aria-hidden="true"></span>
                        <?= __('Ver articulos') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>
                </center>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

   var table_stores = null;

   $(document).ready(function () {

        $('#table-stores').removeClass('display');
		
		table_stores = $('#table-stores').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cangando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguinte",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "Todas"]],
            /*"dom": '<"toolbar">frtip'*/
		});
		
		$('#table-stores_filter').closest('div').before($('#btns-tools').contents());

    });

    $('#table-stores tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_stores.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            $('.modal-actions').modal('show');
        }
    });

    $('a#btn-view').click(function() {
       var action = '/app/stores/view/' + table_stores.$('tr.selected').data('id');
       window.open(action, '_self');
    });

    $('a#btn-edit').click(function() {
       var action = '/app/stores/edit/' + table_stores.$('tr.selected').data('id');
       window.open(action, '_self');
    });

</script>
