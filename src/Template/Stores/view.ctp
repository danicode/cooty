<style type="text/css">

    #table-articles {
        width: 100% !important;
    }

    #table-articles td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 5px 0px 5px !important;
        vertical-align: middle;
    }

    .modal-actions a.btn {
        width: 100%;
        margin-bottom: 5px;
    }

     tr.selected {
        background-color: #8eea99;
        color: #333 !important;
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }

</style>

<div id="btns-tools">
    <div class="pull-right btns-tools margin-bottom-5">
        <button type="button" class="btn btn-default btn-export" title="Exportar en formato excel el contenido de la tabla" >
            <span class="glyphicon icon-file-excel"  aria-hidden="true" ></span>
        </button>
        <a class="btn btn-default" title="<?= __('Volver a la lista de sucursales') ?>" href="<?=$this->Url->build(["action" => "index"])?>" role="button">
            <span class="glyphicon icon-undo2" aria-hidden="true"></span>
        </a>
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        <legend class="sub-title"><?= h($store->name) ?></legend>
    
        <table class="vertical-table">
            <tr>
                <th scope="row"><?= __('Address') ?></th>
                <td><?= h($store->address) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('ID') ?></th>
                <td><?= $store->id ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Numero') ?></th>
                <td><?= $store->number ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Creado') ?></th>
                <td><?= date('d/m/Y H:i', strtotime($store->created->format('Y-m-d H:i:s'))) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modificado') ?></th>
                <td><?= date('d/m/Y H:i', strtotime($store->modified->format('Y-m-d H:i:s'))) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Habilitado') ?></th>
                <td><?= $store->enable ? __('Ok') : __('No'); ?></td>
            </tr>
        </table>
    </div>
    <div class="col-md-10">
         <legend class="sub-title"><?= __('Articulos') ?></legend>
         <table class="table table-bordered table-hover" id="table-articles" >
            <thead>
                <tr>
                    <th><?= __('#') ?></th>
                    <th><?= __('Cantidad') ?></th>
                    <th><?= __('Código') ?></th>
                    <th><?= __('Descripción') ?></th>
                    <th><?= __('Precio') ?></th>
                    <th><?= __('Última sincro') ?></th>
                    <th><?= __('Habilitado') ?></th>
                    <th><?= __('Modificado') ?></th>
                    <th><?= __('Visible') ?></th>
                </tr>
            </thead>
            <tbody>
            <?php $i = 1; ?>
            <?php foreach ($store->stores_has_articles as $sha):  ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $sha->amount ?></td>
                    <td><?= h($sha->article->code) ?></td>
                    <td><?= h($sha->article->description) ?></td>
                    <td>Gs.<?= $sha->article->price ?></td>
                    <td><?= $sha->last_sync ?></td>
                    <td><?= $sha->article->enable ?></td>
                    <td><?= date('d/m/Y H:i', strtotime($sha->modified->format('Y-m-d H:i:s'))) ?></td>
                    <?php if ($sha->visible): ?>
                        <td>
                            <span class="icon-checkmark2 green" aria-hidden="true"></span>
                            <span style="display:none;" >1</span>
                        </td>
                    <?php else: ?>
                        <td>
                            <span class="icon-cancel-circle red" aria-hidden="true"></span>
                            <span style="display:none;" >0</span>
                        </td>
                    <?php endif; ?>
                </tr>
                <?php $i++ ?>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">

   var table_articles = null;

   $(document).ready(function () {

        $('#table-articles').removeClass('display');

		table_articles = $('#table-articles').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "columnDefs": [
                 {
                    "targets": [ 6 ],
                    "visible": false,
                    "searchable": true
                },
                {
                    "targets": [ 8 ],
                    "visible": false,
                    "searchable": true
                },
            ],
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cangando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguinte",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "Todas"]],
            /*"dom": '<"toolbar">frtip'*/
		});

		$('#table-articles_filter').closest('div').before($('#btns-tools').contents());

    });

    $('#table-articles tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_articles.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            $('.modal-actions').modal('show');
        }
    });

    $('.btn-export-pdf').click(function(){
       var action = '/app/articles/viewpdf/';
       window.open(action, '_black');
    });

    $(".btn-export").click(function() {
        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-articles').tableExport({tableName: 'Articles', type:'excel', escape:'false'});
            }
        });
    });

</script>
