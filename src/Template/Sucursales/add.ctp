<div class="row">
     <div class="col-md-2">
        <div class="pull-right">
             <a class="btn btn-default" title="Ir a la lista de Sucursales." href="<?=$this->Url->build(["action" => "index"])?>" role="button">
                <span class="glyphicon icon-undo2" aria-hidden="true"></span>
            </a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        <?= $this->Form->create($sucursal) ?>
        <fieldset>
            <?php
                echo $this->Form->input('name', ['label' => __('Nombre')]);
                echo $this->Form->input('address', ['label' => __('Domicilio')]);
                echo $this->Form->input('phone', ['label' => __('Teléfono')]);
                echo $this->Form->input('open_hours', ['label' => __('Horario')]);
                echo $this->Form->input('enable', ['label' => __('Habilitado')]);
                echo $this->Form->input('map', ['label' => __('Mapa')]);
            ?>
        </fieldset>
        <?= $this->Form->button(__('Aceptar')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
