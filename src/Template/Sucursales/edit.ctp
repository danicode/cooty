<div class="row">
     <div class="col-md-2">
        <div class="pull-right">
             <a class="btn btn-default" title="Ir a la lista de Sucursales." href="<?=$this->Url->build(["action" => "index"])?>" role="button">
                <span class="glyphicon icon-undo2" aria-hidden="true"></span>
            </a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        <?= $this->Form->create($sucursal) ?>
        <fieldset>
            <?php
                echo $this->Form->input('name', ['label' => __('Nombre')]);
                echo $this->Form->input('address', ['label' => __('Domicilio')]);
                echo $this->Form->input('phone', ['label' => __('Teléfono')]);
                echo $this->Form->input('open_hours', ['label' => __('Horario')]);
                echo $this->Form->input('enable', ['label' => __('Habilitado')]);
                echo $this->Form->input('map', ['label' => __('Mapa')]);
            ?>
        </fieldset>
        <?= $this->Form->button(__('Guardar')) ?>
    <?= $this->Form->end() ?>
    </div>

    <div class="col-md-3">
        <div class="row">
            <div class="col-md-11">
                <legend><?= __('Imagen') ?></legend>
            </div>
            <div class="col-md-1">
                <div class="pull-right">
                    <a class="btn btn-default" title="<?= __('Imagen') ?>" href="#" role="button" id="btn-add-asset" >
                        <span class="glyphicon icon-plus" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div id="main-image" class="row">
                    <?php if ($sucursal->url): ?>
                        <div class='col-md-4'>
                            <img style="max-width: 200px;" src="/app/images/<?= $sucursal->url ?>"></img>
                            <a class='btn-delete-asset' data-id="<?= $sucursal->id ?>" href='javascript:void(0)'><?= __('Quitar') ?></a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="styles-popup" class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?= __('Añadir Imagen') ?></h4>
      </div>
      <div class="modal-body">
        <?= $this->Form->create(null, ['url' => ['controller' => 'sucursales' , 'action' => 'addImage'], 'enctype' => "multipart/form-data" ]) ?>
            <fieldset>
                <?php
                    echo $this->Form->input('sucursal_id', ['type' => 'hidden', 'value' => $sucursal->id]);
                    echo $this->Form->file('file', ['required' => true, 'type'=>'file']);
                ?>
            </fieldset>
            <br>

            <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Cancelar') ?></button>
            <?= $this->Form->button(__('Aceptar')) ?>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

    var sucursal = <?= json_encode($sucursal); ?>;

    $('a#btn-add-asset').click(function() {
        $('#styles-popup.modal-actions').modal('show');
    });

    $(document).on("click", "a.btn-delete-asset", function(e) {

        var id  = $(this).data('id');
        var text = 'Esta Seguro que desea eliminar la imagen ? ';
        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/app/sucursales/deleteImage/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id})))
                .find('#replacer').submit();
            }
        });
    });

</script>
