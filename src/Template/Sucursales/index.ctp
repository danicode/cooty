<style type="text/css">

    #table-sucursales {
        width: 100% !important;
    }

    #table-sucursales td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 5px 0px 5px !important;
        vertical-align: middle;
    }

    .modal-actions a.btn {
        width: 100%;
        margin-bottom: 5px;
    }

    tr.selected {
        background-color: #8eea99;
        color: #333 !important;
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }

</style>

<div id="btns-tools">
    <div class="pull-right btns-tools margin-bottom-5">
        <a class="btn btn-default" title="Nueva Sucursal" href="<?=$this->Url->build(["controller" => "sucursales", "action" => "add"])?>" role="button">
            <span class="glyphicon icon-plus"  aria-hidden="true"></span>
        </a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-sucursales" >
            <thead>
                <tr>
                    <th><?= __('#') ?></th>
                    <th><?= __('Nombre') ?></th>
                    <th><?= __('Dirección') ?></th>
                    <th><?= __('Teléfono') ?></th>
                    <th><?= __('Horario') ?></th>
                    <th><?= __('Habilitado') ?></th>
                    <th><?= __('Imagen') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($sucursales as $sucursal): ?>
                <tr data-id="<?= $sucursal->id ?>">
                    <td><?= $sucursal->id ?></td>
                    <td><?= h($sucursal->name) ?></td>
                    <td><?= h($sucursal->address) ?></td>
                    <td><?= h($sucursal->phone) ?></td>
                    <td><?= h($sucursal->open_hours) ?></td>
                    <?php if ($sucursal->enable): ?>
                        <td>
                            <span class="icon-checkmark2 green" aria-hidden="true"></span>
                            <span style="display:none;" >1</span>
                        </td>
                    <?php else: ?>
                        <td>
                            <span class="icon-cancel-circle red" aria-hidden="true"></span>
                            <span style="display:none;" >0</span>
                        </td>
                    <?php endif; ?>
                    <?php if ($sucursal->url): ?>
                        <td><img style="width: 113px; height: auto; position: relative;display: inline-block;" src="/app/images/<?= $sucursal->url ?>" alt="" class="img-responsive"/></td>
                    <?php else: ?>
                        <td>Sin Imagen disponible</td>
                    <?php endif; ?>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?= __('Acciones') ?></h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <?php if ($user['admin']): ?>
                        <a class="btn btn-default" id="btn-edit" href="#" role="button">
                            <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                            <?= __('Editar') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </a>
                    <?php endif; ?>
                    <a class="btn btn-danger " id="btn-delete" href="#" role="button">
                        <span class="glyphicon icon-bin" aria-hidden="true"></span>
                        <?= __('Eliminar') ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>
                </center>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

   var table_sucursales = null;

   $(document).ready(function () {

        $('#table-sucursales').removeClass('display');
		
		table_sucursales = $('#table-sucursales').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cangando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "Todas"]],
            /*"dom": '<"toolbar">frtip'*/
		});
		
		$('#table-sucursales_filter').closest('div').before($('#btns-tools').contents());

    });

    $('#table-sucursales tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_sucursales.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            $('.modal-actions').modal('show');
        }
    });

    $('a#btn-edit').click(function() {
       var action = '/app/sucursales/edit/' + table_sucursales.$('tr.selected').data('id');
       window.open(action, '_self');
    });

    $('a#btn-delete').click(function() {

        var text = 'Esta Seguro que desea eliminar la Sucursal ?';
        var controller  = 'sucursales';
        var id  = table_sucursales.$('tr.selected').data('id');

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/app/'+controller+'/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"})))
                .find('#replacer').submit();
            }
        });
    });

</script>
