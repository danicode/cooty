<style type="text/css">

    #table-syncslogs {
        width: 100% !important;
    }

    #table-syncslogs td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 5px 0px 5px !important;
        vertical-align: middle;
    }

    .modal-actions a.btn {
        width: 100%;
        margin-bottom: 5px;
    }

    tr.selected {
        background-color: #8eea99;
        color: #333 !important;
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }

</style>

<div id="btns-tools">
    <div class="pull-right btns-tools margin-bottom-5">
        <button type="button" class="btn btn-default btn-export" title="<?= __('Exportar en formato excel el contenido de la tabla') ?>" >
            <span class="glyphicon icon-file-excel" aria-hidden="true" ></span>
        </button>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-syncslogs" >
        <thead>
            <tr>
                <th><?= __('#') ?></th>
                <th><?= __('Código') ?></th>
                <th><?= __('Descripción') ?></th>
                <th><?= __('Sucursal') ?></th>
                <th><?= __('Creado') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($syncsLogs as $syncsLog): ?>
            <tr>
                <td><?= $syncsLog->id ?></td>
                <td><?= h($syncsLog->article_code) ?></td>
                <td><?= h($syncsLog->description) ?></td>
                <td><?= $syncsLog->has('store') ? $this->Html->link($syncsLog->store->name, ['controller' => 'Stores', 'action' => 'view', $syncsLog->store->id]) : '' ?></td>
                <td><?= date('d/m/Y H:i', strtotime($syncsLog->created->format('Y-m-d H:i:s'))) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">

   var table_articles = null;

   $(document).ready(function () {

        $('#table-syncslogs').removeClass('display');

		table_users = $('#table-syncslogs').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "columnDefs": [
                {
                    "targets": [ 4 ],
                    "visible": true,
                    "searchable": true,
                    "type": "datetime-custom"
                }
            ],
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cangando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguinte",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "Todas"]],
            /*"dom": '<"toolbar">frtip'*/
		});

		$('#table-syncslogs_filter').closest('div').before($('#btns-tools').contents());

    });

    $('#table-syncslogs tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_users.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            $('.modal-actions').modal('show');
        }
    });

    $('.btn-export-pdf').click(function() {
       var action = '/app/syncslogs/viewpdf/';
       window.open(action, '_black');
    });

    $(".btn-export").click(function() {
        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-syncslogs').tableExport({tableName: 'Logs', type:'excel', escape:'false'});
            }
        });
    });

</script>
