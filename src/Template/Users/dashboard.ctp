<style type="text/css">

    .huge {
      font-size: 40px;
    }
    .panel-green {
      border-color: #5cb85c;
    }
    .panel-green > .panel-heading {
      border-color: #5cb85c;
      color: white;
      background-color: #5cb85c;
    }
    .panel-green > a {
      color: #5cb85c;
    }
    .panel-green > a:hover {
      color: #3d8b3d;
    }
    .panel-red {
      border-color: #d9534f;
    }
    .panel-red > .panel-heading {
      border-color: #d9534f;
      color: white;
      background-color: #d9534f;
    }
    .panel-red > a {
      color: #d9534f;
    }
    .panel-red > a:hover {
      color: #b52b27;
    }
    .panel-yellow {
      border-color: #f0ad4e;
    }
    .panel-yellow > .panel-heading {
      border-color: #f0ad4e;
      color: white;
      background-color: #f0ad4e;
    }
    .panel-yellow > a {
      color: #f0ad4e;
    }
    .panel-yellow > a:hover {
      color: #df8a13;
    }

</style>

<p><?= __('BIENVENIDO') ?> <span class="label label-success"><?= h($user['username']) ?></span></p>

<div class="row">
    <?php if ($user['admin']): ?>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-home fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?= $stores->count(); ?></div>
                            <div><?= __('Sucursales') ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-tags fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?= $articles->count(); ?></div>
                            <div><?= __('¡Articulos Nuevos!') ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-shopping-cart fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?= $orders->count(); ?></div>
                            <div><?= __('¡Nuevos Pedidos!') ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-user fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?= $users->count(); ?></div>
                            <div><?= __('Usuarios del Sistema') ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php else: ?>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-bookmark fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?= $orders_assigned->count() ?></div>
                            <div><?= __('Pedidos Asignados') ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-check-circle-o fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?= $orders_completed->count() ?></div>
                            <div><?= __('Pedidos Completados') ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-truck fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?= $orders_dispatched->count() ?></div>
                            <div><?= __('Pedidos Despachados') ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-ban fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?= $orders_cancelled->count() ?></div>
                            <div><?= __('Pedidos Cancelados') ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>