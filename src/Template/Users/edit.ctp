<style type="text/css">

    .my-hidden {
        display: none;
    }

</style>

<div class="row">
     <div class="col-md-2">
        <div class="pull-right">
             <a class="btn btn-default" title="Ir a la lista de Usuarios" href="<?=$this->Url->build(["action" => "index"])?>" role="button">
                <span class="glyphicon icon-undo2"  aria-hidden="true"></span>
            </a>
            <a class="btn btn-default" title="Agregar nuevo Usuario" href="<?=$this->Url->build(["action" => "add"])?>" role="button">
                <span class="glyphicon icon-plus"  aria-hidden="true"></span>
            </a>
              <?= $this->Html->link(__("<span class='glyphicon icon-bin' title='Eliminar Usuario' aria-hidden='true'></span>"), 
                '#',
                [
                    'class' => 'btn btn-default btn-delete', 
                    'role' => 'button',
                    'data-controller' => 'Users',
                    'data-id' => $user->id,
                    'data-text' =>  __('Esta Seguro que desea eliminar el Usuario <strong>{0}</strong>?', $user->username),
                    'escape' => false
                ]) ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-2">

        <?= $this->Form->create($user,  ['class' => 'form-load']) ?>
        <fieldset>
            <?php
                echo $this->Form->input('username', ['label' => __('Nombre'), 'required' => true]);
                echo $this->Form->input('password', ['label' => __('Clave'), 'value' => '', 'required' => false]);
                echo $this->Form->input('enable', ['label' => __('Habilitar')]);
                echo $this->Form->input('admin', ['label' => __('Administrador')]);
                $storex = array();
                foreach ($stores as $store) {
                    $storex[$store->id] = $store->name;
                }
                $default = array();
                foreach ($user->users_has_stores as $uhs) {
                    array_push($default, $uhs->store_id);
                }

                echo $this->Form->input(
                    'stores', 
                    [
                        'label' => __('Sucursales'),
                        'type' => 'select',
                        'class' => 'selectpicker',
                        'multiple' => 'checkbox',
                        'options' => $storex, 
                        'empty' => false,
                        'default' => $default,
                    ]
                );
            ?>
        </fieldset>
        <?= $this->Form->button(__('Guardar')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<script type="text/javascript">

    var user;

    $(document).ready(function () {
        user = <?= json_encode($user); ?>;

        if (user.admin) {
            $('.form-group.multicheckbox').addClass('my-hidden');
        } else {
            $('.form-group.multicheckbox').removeClass('my-hidden');
        }
    });

    $(document).on("change", "#admin", function(e) {
        var self = $(this);
        if (self.is(":checked")) {
            $('.form-group.multicheckbox').addClass('my-hidden');
        } else {
            $('.form-group.multicheckbox').removeClass('my-hidden');
        }
    });

</script>
