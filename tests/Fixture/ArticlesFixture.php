<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArticlesFixture
 *
 */
class ArticlesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'full_code' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'code_1' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'code_2' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'code_3' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'code_4' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'full_desc' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'desc_1' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'desc_2' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'desc_3' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'desc_4' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'enable' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'price' => ['type' => 'decimal', 'length' => 7, 'precision' => 2, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'amount' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'last_sync' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'store_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_articles_stores1_idx' => ['type' => 'index', 'columns' => ['store_id'], 'length' => []],
            'full_code' => ['type' => 'index', 'columns' => ['full_code'], 'length' => []],
            'code_1' => ['type' => 'index', 'columns' => ['code_1'], 'length' => []],
            'code_2' => ['type' => 'index', 'columns' => ['code_2'], 'length' => []],
            'code_3' => ['type' => 'index', 'columns' => ['code_3'], 'length' => []],
            'code_4' => ['type' => 'index', 'columns' => ['code_4'], 'length' => []],
            'desc_1' => ['type' => 'index', 'columns' => ['desc_1'], 'length' => []],
            'desc_2' => ['type' => 'index', 'columns' => ['desc_2'], 'length' => []],
            'desc_3' => ['type' => 'index', 'columns' => ['desc_3'], 'length' => []],
            'desc_4' => ['type' => 'index', 'columns' => ['desc_4'], 'length' => []],
            'full_desc' => ['type' => 'index', 'columns' => ['full_desc'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_articles_stores1' => ['type' => 'foreign', 'columns' => ['store_id'], 'references' => ['stores', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'full_code' => 'Lorem ipsum dolor sit amet',
            'code_1' => 'Lorem ipsum dolor sit amet',
            'code_2' => 'Lorem ipsum dolor sit amet',
            'code_3' => 'Lorem ipsum dolor sit amet',
            'code_4' => 'Lorem ipsum dolor sit amet',
            'full_desc' => 'Lorem ipsum dolor sit amet',
            'desc_1' => 'Lorem ipsum dolor sit amet',
            'desc_2' => 'Lorem ipsum dolor sit amet',
            'desc_3' => 'Lorem ipsum dolor sit amet',
            'desc_4' => 'Lorem ipsum dolor sit amet',
            'created' => '2017-05-12 21:02:45',
            'enable' => 1,
            'modified' => '2017-05-12 21:02:45',
            'price' => 1.5,
            'amount' => 1,
            'last_sync' => 1,
            'store_id' => 1
        ],
    ];
}
