<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\StoreSyncComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\StoreSyncComponent Test Case
 */
class StoreSyncComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Controller\Component\StoreSyncComponent
     */
    public $StoreSync;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->StoreSync = new StoreSyncComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StoreSync);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
