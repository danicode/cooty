<?php
namespace App\Test\TestCase\Controller;

use App\Controller\StylesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\StylesController Test Case
 */
class StylesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.styles',
        'app.products',
        'app.articles',
        'app.favorites',
        'app.orders_has_articles',
        'app.stores_has_articles',
        'app.stores',
        'app.orders',
        'app.syncs_logs',
        'app.users_has_stores',
        'app.categories',
        'app.seasons',
        'app.assets'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
