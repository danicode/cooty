<?php
namespace App\Test\TestCase\Controller;

use App\Controller\SyncsLogsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\SyncsLogsController Test Case
 */
class SyncsLogsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.syncs_logs',
        'app.stores',
        'app.articles',
        'app.assets',
        'app.favorites',
        'app.orders_has_articles',
        'app.orders',
        'app.users_has_stores'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
