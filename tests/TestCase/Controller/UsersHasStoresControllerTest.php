<?php
namespace App\Test\TestCase\Controller;

use App\Controller\UsersHasStoresController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\UsersHasStoresController Test Case
 */
class UsersHasStoresControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users_has_stores',
        'app.users',
        'app.stores',
        'app.stores_has_articles',
        'app.articles',
        'app.favorites',
        'app.orders_has_articles',
        'app.orders',
        'app.customers',
        'app.deliveries',
        'app.orders_status',
        'app.styles',
        'app.products',
        'app.categories',
        'app.categories_assets',
        'app.groups',
        'app.seasons',
        'app.assets',
        'app.syncs_logs'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
