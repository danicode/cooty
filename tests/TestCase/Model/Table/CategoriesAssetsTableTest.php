<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CategoriesAssetsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CategoriesAssetsTable Test Case
 */
class CategoriesAssetsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CategoriesAssetsTable
     */
    public $CategoriesAssets;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.categories_assets',
        'app.categories',
        'app.products',
        'app.articles',
        'app.favorites',
        'app.orders_has_articles',
        'app.stores_has_articles',
        'app.stores',
        'app.orders',
        'app.syncs_logs',
        'app.users_has_stores',
        'app.styles',
        'app.assets',
        'app.seasons'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CategoriesAssets') ? [] : ['className' => 'App\Model\Table\CategoriesAssetsTable'];
        $this->CategoriesAssets = TableRegistry::get('CategoriesAssets', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CategoriesAssets);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
