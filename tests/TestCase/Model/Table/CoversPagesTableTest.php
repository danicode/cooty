<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CoversPagesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CoversPagesTable Test Case
 */
class CoversPagesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CoversPagesTable
     */
    public $CoversPages;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.covers_pages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CoversPages') ? [] : ['className' => 'App\Model\Table\CoversPagesTable'];
        $this->CoversPages = TableRegistry::get('CoversPages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CoversPages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
