<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FavoritesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FavoritesTable Test Case
 */
class FavoritesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FavoritesTable
     */
    public $Favorites;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.favorites',
        'app.customers',
        'app.orders',
        'app.deliveries',
        'app.stores',
        'app.stores_has_articles',
        'app.articles',
        'app.orders_has_articles',
        'app.styles',
        'app.products',
        'app.categories',
        'app.categories_assets',
        'app.groups',
        'app.seasons',
        'app.assets',
        'app.syncs_logs',
        'app.users_has_stores',
        'app.users',
        'app.orders_status'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Favorites') ? [] : ['className' => 'App\Model\Table\FavoritesTable'];
        $this->Favorites = TableRegistry::get('Favorites', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Favorites);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
