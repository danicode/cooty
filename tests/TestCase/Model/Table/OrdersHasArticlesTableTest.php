<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrdersHasArticlesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrdersHasArticlesTable Test Case
 */
class OrdersHasArticlesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OrdersHasArticlesTable
     */
    public $OrdersHasArticles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.orders_has_articles',
        'app.orders',
        'app.customers',
        'app.favorites',
        'app.deliveries',
        'app.stores',
        'app.stores_has_articles',
        'app.articles',
        'app.styles',
        'app.products',
        'app.categories',
        'app.categories_assets',
        'app.groups',
        'app.seasons',
        'app.assets',
        'app.syncs_logs',
        'app.users_has_stores',
        'app.orders_status'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OrdersHasArticles') ? [] : ['className' => 'App\Model\Table\OrdersHasArticlesTable'];
        $this->OrdersHasArticles = TableRegistry::get('OrdersHasArticles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrdersHasArticles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
