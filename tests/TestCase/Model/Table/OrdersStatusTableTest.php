<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrdersStatusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrdersStatusTable Test Case
 */
class OrdersStatusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OrdersStatusTable
     */
    public $OrdersStatus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.orders_status',
        'app.orders',
        'app.customers',
        'app.favorites',
        'app.deliveries',
        'app.stores',
        'app.stores_has_articles',
        'app.articles',
        'app.orders_has_articles',
        'app.styles',
        'app.products',
        'app.categories',
        'app.categories_assets',
        'app.groups',
        'app.seasons',
        'app.assets',
        'app.syncs_logs',
        'app.users_has_stores',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OrdersStatus') ? [] : ['className' => 'App\Model\Table\OrdersStatusTable'];
        $this->OrdersStatus = TableRegistry::get('OrdersStatus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrdersStatus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
