<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductsHasColorsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductsHasColorsTable Test Case
 */
class ProductsHasColorsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductsHasColorsTable
     */
    public $ProductsHasColors;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.products_has_colors',
        'app.products',
        'app.articles',
        'app.stores',
        'app.orders',
        'app.syncs_logs',
        'app.users_has_stores',
        'app.assets',
        'app.favorites',
        'app.orders_has_articles',
        'app.categories',
        'app.seasons',
        'app.products_has_colors',
        'app.colors'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProductsHasColors') ? [] : ['className' => 'App\Model\Table\ProductsHasColorsTable'];
        $this->ProductsHasColors = TableRegistry::get('ProductsHasColors', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductsHasColors);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
