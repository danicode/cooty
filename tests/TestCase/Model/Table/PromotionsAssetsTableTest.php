<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PromotionsAssetsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PromotionsAssetsTable Test Case
 */
class PromotionsAssetsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PromotionsAssetsTable
     */
    public $PromotionsAssets;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.promotions_assets',
        'app.promotions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PromotionsAssets') ? [] : ['className' => 'App\Model\Table\PromotionsAssetsTable'];
        $this->PromotionsAssets = TableRegistry::get('PromotionsAssets', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PromotionsAssets);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
