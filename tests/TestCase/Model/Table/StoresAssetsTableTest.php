<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StoresAssetsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StoresAssetsTable Test Case
 */
class StoresAssetsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StoresAssetsTable
     */
    public $StoresAssets;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.stores_assets',
        'app.stores',
        'app.stores_has_articles',
        'app.articles',
        'app.favorites',
        'app.customers',
        'app.orders',
        'app.deliveries',
        'app.orders_has_articles',
        'app.orders_status',
        'app.users',
        'app.users_has_stores',
        'app.styles',
        'app.products',
        'app.categories',
        'app.categories_assets',
        'app.groups',
        'app.seasons',
        'app.assets',
        'app.syncs_logs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('StoresAssets') ? [] : ['className' => 'App\Model\Table\StoresAssetsTable'];
        $this->StoresAssets = TableRegistry::get('StoresAssets', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StoresAssets);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
