<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StoresHasArticlesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StoresHasArticlesTable Test Case
 */
class StoresHasArticlesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StoresHasArticlesTable
     */
    public $StoresHasArticles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.stores_has_articles',
        'app.stores',
        'app.articles',
        'app.assets',
        'app.favorites',
        'app.orders_has_articles',
        'app.orders',
        'app.syncs_logs',
        'app.users_has_stores'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('StoresHasArticles') ? [] : ['className' => 'App\Model\Table\StoresHasArticlesTable'];
        $this->StoresHasArticles = TableRegistry::get('StoresHasArticles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StoresHasArticles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
