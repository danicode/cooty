<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StylesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StylesTable Test Case
 */
class StylesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StylesTable
     */
    public $Styles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.styles',
        'app.products',
        'app.articles',
        'app.favorites',
        'app.orders_has_articles',
        'app.stores_has_articles',
        'app.stores',
        'app.orders',
        'app.syncs_logs',
        'app.users_has_stores',
        'app.categories',
        'app.seasons',
        'app.assets'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Styles') ? [] : ['className' => 'App\Model\Table\StylesTable'];
        $this->Styles = TableRegistry::get('Styles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Styles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
