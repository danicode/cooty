<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SyncsLogsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SyncsLogsTable Test Case
 */
class SyncsLogsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SyncsLogsTable
     */
    public $SyncsLogs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.syncs_logs',
        'app.stores',
        'app.articles',
        'app.assets',
        'app.favorites',
        'app.orders_has_articles',
        'app.orders',
        'app.users_has_stores'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SyncsLogs') ? [] : ['className' => 'App\Model\Table\SyncsLogsTable'];
        $this->SyncsLogs = TableRegistry::get('SyncsLogs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SyncsLogs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
