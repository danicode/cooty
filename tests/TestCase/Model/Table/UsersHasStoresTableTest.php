<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersHasStoresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersHasStoresTable Test Case
 */
class UsersHasStoresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersHasStoresTable
     */
    public $UsersHasStores;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users_has_stores',
        'app.users',
        'app.stores',
        'app.stores_has_articles',
        'app.articles',
        'app.favorites',
        'app.orders_has_articles',
        'app.orders',
        'app.customers',
        'app.deliveries',
        'app.orders_status',
        'app.styles',
        'app.products',
        'app.categories',
        'app.categories_assets',
        'app.groups',
        'app.seasons',
        'app.assets',
        'app.syncs_logs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UsersHasStores') ? [] : ['className' => 'App\Model\Table\UsersHasStoresTable'];
        $this->UsersHasStores = TableRegistry::get('UsersHasStores', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UsersHasStores);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
